![Static Badge](https://img.shields.io/badge/made_with-Python-blue?color=blue)
[![pipeline status](https://gitlab.hrz.tu-chemnitz.de/s0872522--tu-dresden.de/tomeda/badges/master/pipeline.svg)](https://gitlab.hrz.tu-chemnitz.de/s0872522--tu-dresden.de/tomeda/-/commits/master)
[![coverage report](https://gitlab.hrz.tu-chemnitz.de/s0872522--tu-dresden.de/tomeda/badges/master/coverage.svg)](https://gitlab.hrz.tu-chemnitz.de/s0872522--tu-dresden.de/tomeda/-/commits/master)
[![Latest Release](https://gitlab.hrz.tu-chemnitz.de/s0872522--tu-dresden.de/tomeda/-/badges/release.svg)](https://gitlab.hrz.tu-chemnitz.de/s0872522--tu-dresden.de/tomeda/-/releases)
![Gitlab pipeline status (self-managed)](https://img.shields.io/gitlab/pipeline-status/s0872522--tu-dresden.de%252Ftomeda?gitlab_url=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de&branch=master&link=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Fs0872522--tu-dresden.de%2Ftomeda)
![Gitlab code coverage (self-managed, specific job)](https://img.shields.io/gitlab/pipeline-coverage/s0872522--tu-dresden.de%2Ftomeda?gitlab_url=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2F)
![GitLab (self-managed)](https://img.shields.io/gitlab/license/s0872522--tu-dresden.de%2Ftomeda?gitlab_url=https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de)
[![PyPI version](https://badge.fury.io/py/tomeda.svg)](https://badge.fury.io/py/tomeda)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

# ToMeDa
**To**ol for **Me**tadata **Da**ta Management is a tool to create, collect and upload metadata to a dataverse instance.
It is aimd to aid computational engineers with creating metadata for a dataset easier and faster.
- It is based on Pydantic to define the metadata schema
- Enables the engineer to participate in the metadata schmema creation
- Derives a schema documentation and entity diagrom from the schema
- Outputs the metadata in JSON
- Optionally has Dataverse integration (Dataverse schema derivation and metadata conversion)


See the [poster](./media/Poster_ToMeDa_Sander_2023.pdf) for more information.

---
**NOTE**

The current state is of the state of reasearch software and is not ready for production use.
It is a generalisation of an internal tool which is not yet fully completed.
We are actively working on it and will release a stable version soon. For the moment only Linux is supported.

For a demonstration use the script in `scripts/tomeda_all.sh` after installing the package via `poetry install`.
The resulting files will reside in `scripts/user/`, `scripts/admin/` and `example/example_1/`.

A more detailed example is in development and will be released in the next weeks.

---

# Workflows
## Administrator / DataOfficer
This role has different responsibility than the user.
The Dataofficer is responsible for the metadata schema, the dataverse instance and the
mapping between them.
1. Create a schema together with the User (exmaple in `data/dataset_metadata_schema.py`)
2. Dump the schema documentation (see `scripts/tomeda_DOFF_00_dump_documentation`)
3. Dump Collector Template to configure the collector (see `scripts/tomeda_DOFF_01_dump_template`, user-filled template in example in `example/example_1/metadata_config.nt`)
4. Dump other information (see `scripts/tomeda_DOFF_[02-05]...`) for dataverse transformation (e.g. dataverse keys, dataverse tsv)
## User
The user applies the schema to the dataset and uploads it to the dataverse instance. This is done in different steps
1. Extract metadata from the different locations specified in the Collector Template (see `scripts/tomeda_USER_01_extract_metadata`)
2. Map metadata to dataverse (see `scripts/tomeda_USER_02_map_metadata_to_dataverse`). This is only needed when using
   Dataverse as a backend. Dataverse cannot natively accept the metadata schema keys because they are to deep,
   therefore a flattening and dedublication takes place.
3. Upload metadata to dataverse (see `scripts/tomeda_USER_03_upload_to_dataverse`) [In Progress]

# Installation
## Requirements
* **[Python 3.10+](https://www.python.org/downloads/):**
The application was developed using Python 3.10. Lower Python versions may not work.
* **Supported operating systems:**
Basically the application can be run on any system that supports Python.
However, the `gather` files may contain OS specific commands.
  * Linux
  * Windows
  * MacOS
* **[Graphviz](https://graphviz.org/) (optional)**:
is used to visualize data structures, if `graph` extra installed.
For more information see [PyGraphviz](https://pygraphviz.github.io/documentation/stable/install.html)
and [Graphviz](https://graphviz.org/download/).
  * Linux:
    * Debian: `sudo apt-get install graphviz libgraphviz-dev`
    * RedHat: `sudo dnf install graphviz graphviz-devel`
  * Windows:
    * [Microsoft Visual C++ Build Tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/):
    `winget install Microsoft.VisualStudio.2022.BuildTools` or `choco install visualstudio2022buildtools`
    * `winget install Graphviz.Graphviz` or `choco install Graphviz`
  * MacOS (untested): `brew install graphviz`

# Setup Development Environment

## Requirements
* Requirements from [Installation](#installation)
* **[Poetry](https://python-poetry.org/)**:
is used to manage dependencies and virtual environments. pip is not recommended, as version incompatibilities may occur.
Use the official installer or pipx instead.
  * Linux: `curl -sSL https://install.python-poetry.org | python3 -` or `pipx install poetry`
  * Windows: `(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | python -` or `pipx install poetry`
  * MacOS: `curl -sSL https://install.python-poetry.org | python3 -` or `pipx install poetry`

## Install
Install with linter dependencies
```shell
poetry install
```
Install with linter, test and docs dependencies
```shell
poetry install --with test,docs
```
Install with linter, test, docs and graph dependencies
```shell
poetry install --with test,docs -E graph
```

## Activate Virtual Environment
Poetry automatically creates a virtual environment for the project.
```shell
poetry shell
```
Or manually activate the virtual environment
```shell
source $(poetry env info --path)/bin/activate
```
Execute commands in virtual environment e.g.
```shell
poetry run python -m tomeda
```

## Pre-Commit
Pre-Commit is used to run linters and formatters before committing.
```shell
pre-commit install
```

# Usage
Source ToMeDa
```shell
source tomeda.env
```

Execute Scripts in the following order
```shell
tomeda_DOFF_01_dump_template
tomeda_DOFF_02_dump_info_struct
tomeda_DOFF_03_extract_TSV_keys
tomeda_DOFF_04_derive_dataverse_key
tomeda_DOFF_05_derive_dataverse_tsv

tomeda_USER_01_extract_metadata
tomeda_USER_02_map_metadata_to_dataverse
tomeda_USER_03_upload_to_dataverse
```

# Readme

* [Readme for Users](docs/mkdocs/for_user.md)
* [Readme for Administrators](docs/mkdocs/for_admin.md)

For `mkdocs` use
```shell
mkdocs serve
````

# Open Issues
## ToDo:
- [ ] Integrate Uploader script into ToMeDa
### Known Limitations
- The current version does allow of the upload of the custom Metadata Schema to the Dataverse instance,
  but it is not possible to use it because of name conflicts with the SOLR schema. (working on a fix)
