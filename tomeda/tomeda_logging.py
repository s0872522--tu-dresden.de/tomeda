"""
This module provides extended logging functionality, including the
`TraceLogger` class and the `TomedaLogging` class for flexible logging
configuration.

`TraceLogger` enhances Python's standard `logging.Logger` class by
introducing a new logging level, `TRACE`, which is lower than `DEBUG`.
It includes a new `trace` method for logging at this level.

`TomedaLogging`, on the other hand, provides a convenient interface for
configuring both the root logger and any custom loggers needed by
individual modules. It manages the format of log messages, the default
log level, and the routing of logs to various outputs, such as the
console or a file. For a logger that does not require a unique
configuration, simply invoking `logging.getLogger(__name__)` will apply
the default settings as set by root logger.

In essence, this module extends Python's logging capabilities, making it
easier to manage and adapt logs according to specific needs.

"""
import logging
import sys
from collections.abc import Callable
from logging import (
    FileHandler,
    Formatter,
    Handler,
    Logger,
    StreamHandler,
)
from typing import (
    Any,
    TextIO,
    cast,
)

logger = logging.getLogger(__name__)


class TraceLogger(Logger):
    """
    Extended Logger class that introduces a new logging level `TRACE`.

    The `TRACE` level is situated below the `DEBUG` level and is
    represented by the numeric value 5. A corresponding method `trace`
    is provided to log messages at this level.
    """

    TRACE = 5
    logging.addLevelName(TRACE, "TRACE")

    def trace(
        self,
        msg: Any,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Log a message with a severity `TRACE` on this logger.

        Parameters
        ----------
        msg : Any
            The message to be logged. It can be any Python structure
            that has a `str` method.
        args : Any
            Variable argument list that gets formatted with `msg`.
        kwargs : Any
            Arbitrary keyword arguments that are interpreted according e.g.
            to the `exc_info`, `stack_info`, `stacklevel` or `extra` parameters
            of the `logging.Logger.log` method.

        """
        if self.isEnabledFor(self.TRACE):
            self._log(
                self.TRACE,
                msg,
                args,
                **kwargs,
            )


class TomedaLogging:
    """
    This class configures logging for the application, primarily
    focusing on configuring the root logger, but also supports
    configuring module-specific loggers.

    The TomedaLogging class supports configuring different loggers with
    different levels, formatting styles, and handlers for console and
    file output. If a logger does not need to be specifically
    configured, it will inherit the configuration from the root logger,
    which can be simply retrieved using 'logging.getLogger(__name__)'.

    The class doesn't require the logger configuration to be present in
    the same module where the logger is used. This allows for a
    centralized logger configuration.

    Example
    -------
    To configure the root logger and then retrieve a module-specific
    logger:

    ```python
    TomedaLogging()
    logger = logging.getLogger(__name__)
    ```

    To configure a logger with a custom name, specify a filename for the
    log file, set the log level for file logging to WARNING, and disable
    console logging:

    ```python
    TomedaLogging(
        "my_special_logger",
        file_name="my_special_logger.log",
        file_level=logging.WARNING,
        console_level=None
    )
    logger = logging.getLogger("my_special_logger")
    ```

    """

    _MAX_FILE_HANDLER_ATTEMPTS = 3

    _CONSOLE_HANDLER_FORMATTER = "%(name)s - %(levelname)s - %(message)s"
    _FILE_HANDLER_FORMATTER = (
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )

    def __init__(
        self,
        name: str | None = None,
        console_level: int | None = TraceLogger.TRACE,
        file_level: int | None = logging.DEBUG,
        file_name: str | None = "tomeda.log",
        console_formatter: (
            Formatter | str | None
        ) = _CONSOLE_HANDLER_FORMATTER,
        file_formatter: (Formatter | str | None) = _FILE_HANDLER_FORMATTER,
    ) -> None:
        self._console_level: int | None = console_level
        self._file_level: int | None = file_level
        self._console_handler: StreamHandler[TextIO] | None = StreamHandler()
        self._file_handler: FileHandler | None = None

        if logging.getLoggerClass() != TraceLogger:
            logging.setLoggerClass(TraceLogger)

        self._logger: Logger = logging.getLogger(name)
        self._logger.setLevel(logging.INFO)

        self._configure_console_handler(
            level=console_level,
            formatter=console_formatter,
        )
        self._configure_file_handler(
            level=file_level,
            formatter=file_formatter,
            file_name=file_name,
        )
        logger.info(
            "Logger '%s' initialized with level '%s'.",
            self._logger.name,
            logging.getLevelName(self._logger.getEffectiveLevel()),
        )

    def set_console_level(self, level: int | None) -> None:
        """
        Set the log level for the console handler.

        If the level is set to None, the console handler will be
        removed.

        Parameters
        ----------
        level : int | None
            The log level. If it's None, the console handler will be
            removed.

        """
        formatter = (
            self._console_handler.formatter if self._console_handler else None
        )
        self._configure_console_handler(level, formatter)

    def set_file_level(self, level: int | None) -> None:
        """
        Set the log level for the file handler.

        If the level is set to None, the file handler will be removed.

        Parameters
        ----------
        level : int | None
            The log level. If it's None, the file handler will be
            removed.

        """
        formatter = self._file_handler.formatter if self._file_handler else None
        filename = (
            self._file_handler.baseFilename if self._file_handler else None
        )
        self._configure_file_handler(level, formatter, filename)

    def _configure_console_handler(
        self,
        level: int | None,
        formatter: Formatter | str | None,
    ) -> None:
        """
        Private method to configure the console handler.

        This method removes the previous console handler, and sets up a
        new one if the level is not None.

        Parameters
        ----------
        level : int | None, optional
            The log level for the console handler. If it's None, the
            console handler will be removed.
        formatter : str | Formatter | None, optional
            The formatter for console log messages. It can be a
            string (format), a Formatter instance, or None.

        """
        self._console_level = level
        if self._console_handler is not None:
            self._logger.removeHandler(self._console_handler)
        if level is None:
            return

        self._console_handler = cast(
            StreamHandler | None,  # type: ignore[type-arg]
            self._set_handler(
                self._console_handler,
                level,
                formatter,
                lambda: StreamHandler(sys.stdout),
            ),
        )

    def _configure_file_handler(
        self,
        level: int | None,
        formatter: Formatter | str | None,
        file_name: str | None,
    ) -> None:
        """
        Private method to configure the file handler.

        This method removes the previous file handler, and sets up a new
        one. If the log level or file name are None or empty, the file
        handler will be removed.

        Parameters
        ----------
        level : int | None, optional
            The log level for the file handler. If it's None, the file
            handler will be removed.
        formatter : str | Formatter | None, optional
            The formatter for file log messages. It can be a string
            (format), a Formatter instance, or None.
        file_name : str | None, optional
            The name of the log file. If it's None or empty, the file
            handler will be removed.

        """
        self._file_level = level
        if self._file_handler is not None:
            self._logger.removeHandler(self._file_handler)
        if (level is None) or (file_name is None) or (not file_name.strip()):
            return

        handler = None
        for attempt in range(self._MAX_FILE_HANDLER_ATTEMPTS):
            try:
                handler = cast(
                    FileHandler,
                    self._set_handler(
                        self._file_handler,
                        level,
                        formatter,
                        lambda: FileHandler(
                            str(file_name), mode="a", encoding="utf8"
                        ),
                    ),
                )
                break
            except FileNotFoundError:
                if attempt == self._MAX_FILE_HANDLER_ATTEMPTS - 1:
                    logger.warning(
                        "Failed to open log file '%s'. "
                        "File Logging is disabled for '%s'-Logger.",
                        file_name,
                        self._logger.name,
                    )
                    break
            except PermissionError:
                logger.warning(
                    "No permission to write to log file '%s'. "
                    "File Logging is disabled for '%s'-Logger.",
                    file_name,
                    self._logger.name,
                )
                break
            except IsADirectoryError:
                logger.warning(
                    "Log file '%s' is a directory. "
                    "File Logging is disabled for '%s'-Logger.",
                    file_name,
                    self._logger.name,
                )
                break

        self._file_handler = handler

    def _set_handler(
        self,
        old_handler: Handler | None,
        level: int | None,
        formatter: Formatter | str | None,
        create_handler: Callable[[], logging.Handler],
    ) -> logging.Handler | None:
        """
        Private method to configure a handler.

        This method removes the previous handler, and sets up a new one
        if the level is not None.

        Parameters
        ----------
        old_handler : Handler | None
            The previous handler that will be removed.
        level : int | None
            The log level for the new handler. If it's None, no new
            handler will be set up.
        formatter : str | Formatter | None
            The formatter for the new handler's log messages. It can be
            a string (format), a Formatter instance, or None.
        create_handler : Callable
            A function to create a new handler.

        Returns
        -------
        Handler | None
            The new handler, or None if the level is None.

        """
        if old_handler is not None:
            self._logger.removeHandler(old_handler)
        if level is None:
            return None

        handler = create_handler()
        handler.setLevel(level)
        handler.setFormatter(self._create_formatter(formatter))

        self._update_log_level(level)

        self._logger.addHandler(handler)

        return handler

    def _update_root_log_level(self, level: int) -> None:
        """
        Private method to set the log level for the root logger.

        This method determines the lowest log level among all loggers
        and sets it as the level of the root logger.

        Parameters
        ----------
        level : int
            The initial level e.g. from new or modified logger that will
            be compared with the levels of all loggers.

        """
        loggers = [
            logger_
            for logger_ in logging.getLogger().manager.loggerDict.values()
            if isinstance(logger_, Logger)
        ]
        lowest_level = min(
            (logger_.getEffectiveLevel() for logger_ in loggers),
            default=level,
        )

        logging.getLogger().setLevel(lowest_level)

    def _update_log_level(self, level: int) -> None:
        """
        Private method to set the log level for the logger instance.

        This method determines the lowest log level among all handlers
        of this logger and sets it as the level of the logger.

        Parameters
        ----------
        level : int
            The initial level e.g. from new handler that will be
            compared with the levels of all handlers.

        """
        handler_levels = [
            handler.level
            for handler in self._logger.handlers
            if isinstance(handler, Handler)
        ]
        lowest_level = min(handler_levels, default=level)
        self._logger.setLevel(lowest_level)
        self._update_root_log_level(lowest_level)

    def _create_formatter(
        self, formatter: str | Formatter | None
    ) -> Formatter | None:
        """
        Private method to create a formatter.

        This method creates a Formatter instance if the input formatter
        is a string.

        Parameters
        ----------
        formatter : str | Formatter | None
            The input formatter. It can be a string (format), a
            Formatter instance, or None.

        Returns
        -------
        Formatter | None
            The created Formatter instance if the input formatter is a
            string. If the input formatter is a Formatter instance, it
            will be returned as it is. If the input formatter is None,
            None will be returned.

        """
        if isinstance(formatter, str):
            formatter = Formatter(formatter)
        return formatter
