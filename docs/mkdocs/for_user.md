# User-Documentation

The user has the following responsibilities:

1. Fill the prefilled metadata config file
2. Execute the gather script to create the metadata json
3. Get the API key from the dataverse instance
4. Execute the upload_to_dataverse script to upload the data to dataverse.

## 1. Fill the prefilled metadata config file

Example of a prefilled metadata config file:

e.g.
```yaml
contact:
    -
        name: @literal Max Mustermann
        givenName: @echo $USER
        familyName:
        address:
        affiliation:
            name:
    -
        name: @literal John Smith
        givenName: @@echo $USER
        familyName:
        address:
        affiliation:
            name:


```

## 2. Execute the gather script to create the metadata json
- ToDo
## 3. Get the API key from the dataverse instance
See [here](https://guides.dataverse.org/en/latest/api/auth.html).
## 4. Execute the upload_to_dataverse script to upload the data to dataverse.
- ToDo