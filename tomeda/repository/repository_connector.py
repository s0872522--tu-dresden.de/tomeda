"""
This module defines the `TomedaRepositoryConnector` base class.

The `TomedaRepositoryConnector` is an abstract base class that is used to
establish a connection to a specific repository and perform various operations
like fetching metadata, uploading data, etc. It provides the basic structure and
methods for the connection and leaves the specific implementation to its
subclasses.

Each repository that needs to be connected will have a specific subclass
of `TomedaRepositoryConnector`, and those subclasses will be placed in their
own separate modules.

Classes
-------
TomedaRepositoryConnector : ABC
    An abstract base class that provides a common interface for all
    repository connectors. Defines a set of methods that subclasses
    should implement.

"""
import importlib
import json
import logging
from abc import ABC, abstractmethod
from types import ModuleType
from typing import (
    Any,
    TypeVar,
    cast,
)
from urllib.parse import urlparse

from tomeda.exception import (
    TomedaFactoryImportError,
    TomedaFactoryInstantiationError,
    TomedaFactoryInvalidClassError,
    TomedaFactoryTypeError,
    TomedaFactoryValueError,
)
from tomeda.tomeda_logging import TraceLogger

from .constants import REPOSITORY_DATA, JSONType
from .http_handler import TomedaHttpHandler
from .models import (
    RepositoryDataCollection,
    RepositoryDataCollectionContent,
    RepositoryDataDataset,
    RepositoryDataMetadataBlock,
    RepositoryDataMetadataBlockFieldDefinition,
    RepositoryDataServer,
    RepositoryDataUser,
)
from .repository_execption import (
    TomedaHttpConnectionError,
    TomedaHttpUnauthorizedError,
    TomedaRepositoryAuthenticationError,
    TomedaRepositoryBadResponseError,
    TomedaRepositoryConnectionError,
    TomedaRepositoryTypeError,
    TomedaRepositoryValueError,
)
from .validator import TomedaRepositoryValidator

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


class TomedaRepositoryConnector(ABC):
    """
    Abstract base class for connecting to a specific repository.

    This class provides the basic structure and methods for interacting with a
    repository, including setting up the connection, fetching metadata,
    uploading data, etc. The specific details of how these operations are
    performed should be implemented in the subclasses.

    Parameters
    ----------
    url : str
        The base URL of the repository to connect to.
    api_token : str
        The API token to use for authenticating with the repository.

    Attributes
    ----------
    repository_type : str
        The type of the repository e.g. 'dataverse', 'coscine', etc.
    base_url : str
        The base URL of the repository.
    api_token : str
        The API token used for authentication.
    base_url_api : str
        The base URL of the repository's API.
    USER_AGENT : str
        The user agent string to use when making requests to the repository.
    API_VERSION : str
        The version of the API to use.

    """

    API_VERSION = ""
    USER_AGENT = "ToMeDa"

    base_url: str = ""
    base_url_api: str = ""

    def __init__(
        self,
        repository_type: str,
        validator: TomedaRepositoryValidator,
        url: str,
        api_token: str,
    ):
        self.repository_type = repository_type
        self.validator = validator
        self.base_url = url
        self.base_url_api = self._set_base_url_api()
        self.validator.base_url = self.base_url
        self._validate_url(self.base_url)
        self.api_token = api_token
        self.http = TomedaHttpHandler()

    @abstractmethod
    def _set_base_url_api(self) -> str:
        """
        Constructs the base API URL for a repository.

        This is an abstract method that must be overridden in subclasses.

        The base API URL is typically formed by appending '/api/' and
        the API version to the base URL of the repository. The method
        also validates the constructed URL before returning it.

        Returns
        -------
        str
            The base API URL for the repository.

        Raises
        ------
        TomedaRepositoryTypeError
            If the base URL is not a string.
        TomedaRepositoryValueError
            If the constructed URL does not refer to the repository or if the
            URL is not a valid Unicode string.
        """
        pass

    def _validate_url(self, url: str) -> bool:
        """
        Validates if a string is a well-formed URL. The URL must be a string,
        must refer to the repository, must be a valid Unicode string, must
        use a supported protocol (HTTP or HTTPS), and must have a domain name.

        If the URL does not have a protocol, it will be extended with the HTTPS
        protocol, and the base URL, if necessary, will be extended with the
        HTTPS protocol as well.

        Parameters
        ----------
        url : str
            The URL to validate.

        Returns
        -------
        bool
            True if the url is valid, False otherwise.

        Raises
        ------
        TomedaRepositoryTypeError
            If the provided URL is not a string.
        TomedaRepositoryValueError
            If the URL does not refer to the repository, is not a valid
            Unicode string, uses a not supported protocol, or does not
            have a domain name.
        """
        if not isinstance(url, str):
            logger.trace("Passing URL to request JSON is not a string.")
            raise TomedaRepositoryTypeError(
                self.repository_type,
                self.base_url,
                "Passing URL is not a string.",
            )
        if not url.startswith(self.base_url):
            logger.trace("The URL does not refer to the repository: %s.", url)
            raise TomedaRepositoryValueError(
                self.repository_type,
                self.base_url,
                "The URL does not refer to the repository: %s.",
                url,
            )

        try:
            parsed_url = urlparse(url)
        except UnicodeEncodeError as error:
            logger.trace(
                "The URL is not a valid Unicode string: %s", str(error)
            )
            raise TomedaRepositoryValueError(
                self.repository_type,
                self.base_url,
                "The URL is not a valid Unicode string: %s",
                str(error),
            ) from error

        # If the URL does not have a protocol, it is extended with HTTPS
        # protocol and if necessary the base URL extended with HTTPS protocol.
        if not parsed_url.scheme:
            logger.info(
                "The URL does not have a protocol. URL is extended with HTTPS "
                "protocol. New URL: %s",
                url,
            )
            old_url = url
            url = f"https://{url}"
            if old_url == self.base_url:
                self.base_url = url
                self.base_url_api = self._set_base_url_api()
            parsed_url = urlparse(url)

        if parsed_url.scheme not in ["http", "https"]:
            logger.trace(
                "URL has a not supported protocol: %s", parsed_url.scheme
            )
            raise TomedaRepositoryValueError(
                self.repository_type,
                self.base_url,
                "URL has a not supported protocol: %s",
                parsed_url.scheme,
            )
        if not parsed_url.netloc:
            logger.trace("URL does not have a domain name.")
            raise TomedaRepositoryValueError(
                self.repository_type,
                self.base_url,
                "URL does not have a domain name.",
            )

        return True

    def _get_http_headers(
        self, content_type: str | None = None, auth: bool = True
    ) -> dict[str, str]:
        """
        Returns HTTP headers for a request. The headers include a User-Agent
        and optionally include authentication headers and a content type.
        Content-Type is also added automatically in some cases e.g. JSON.

        Parameters
        ----------
        content_type : str | None, optional
            The content type for the request, by default None
        auth : bool, optional
            Whether to include authentication headers in the returned headers,
            by default True

        Returns
        -------
        dict[str, str]
            The HTTP headers to be used in a request.
        """
        headers = {"User-Agent": self.USER_AGENT}
        if auth:
            headers |= self._get_auth_headers()
        if content_type:
            headers["Content-Type"] = content_type
        return headers

    @abstractmethod
    def _get_auth_headers(self) -> dict[str, str]:
        """
        Returns authentication headers for a request.

        This is a base method that should be overridden by subclasses to provide
        the necessary authentication headers for their specific use case.

        Returns
        -------
        dict[str, str]
            An empty dictionary in the base implementation. Subclasses should
            return a dictionary with authentication headers.
        """
        pass

    def _get_json_from_url(self, url: str, context: str) -> JSONType:
        """
        Sends a GET request to the specified URL and returns the response in
        JSON format.

        Parameters
        ----------
        url : str
            The URL to send the GET request to.
        context : str
            The context from which the request is being made. This is
            used for error messages. If not a string or empty, it will
            be set to 'unknown'.

        Returns
        -------
        JSONType
            The JSON response from the server parsed into a Python data type
            (usually a dict or list).

        Raises
        ------
        TomedaRepositoryValueError
            If the URL does not refer to the repository or is not a
            valid Unicode string.
        TomedaRepositoryTypeError
            If the provided URL is not a string.
        TomedaRepositoryBadResponseError
            If the response from the server is not a valid JSON.
        TomedaRepositoryAuthenticationError
            If the request to the server is unauthorized.
        TomedaRepositoryConnectionError
            If the connection to the server fails.
        """
        if not isinstance(context, str) or not context:
            context = "unknown"

        self._validate_url(url)

        try:
            http_response = self.http.get(url, header=self._get_http_headers())
            return cast(JSONType, json.loads(http_response.text))
        except json.JSONDecodeError as error:
            logger.trace(
                f"The repository's response to '{context}' does not have a "
                "valid JSON format."
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                self.base_url,
                "The repository's response to '%s' does not have a valid "
                "JSON format.",
                context,
            ) from error
        except TomedaHttpUnauthorizedError as error:
            logger.trace(
                f"Request to the repository to '{context}' is unauthorized."
            )
            raise TomedaRepositoryAuthenticationError(
                self.repository_type,
                self.base_url,
                "Request to the repository to '%s' is unauthorized.",
                context,
            ) from error
        except TomedaHttpConnectionError as error:
            logger.trace(f"Connection to the repository to '{context}' failed.")
            raise TomedaRepositoryConnectionError(
                self.repository_type,
                self.base_url,
                "Connection to the repository to '%s' failed.",
                context,
            ) from error

    @abstractmethod
    def _fetch_api_data(self, url: str, context: str) -> JSONType:
        pass

    @abstractmethod
    def get_server_info(self) -> RepositoryDataServer:
        pass

    @abstractmethod
    def get_user_info(self) -> RepositoryDataUser:
        pass

    @abstractmethod
    def get_metadata_blocks(self) -> list[RepositoryDataMetadataBlock]:
        pass

    @abstractmethod
    def get_metadata_block_definition(
        self, block_id: int | str
    ) -> list[RepositoryDataMetadataBlockFieldDefinition]:
        pass

    @abstractmethod
    def get_collection(self, collection_id: str) -> RepositoryDataCollection:
        pass

    @abstractmethod
    def get_collection_contents(
        self, collection_id: str
    ) -> list[RepositoryDataCollectionContent]:
        pass

    @abstractmethod
    def get_collection_metadata_blocks(
        self, collection_id: str
    ) -> list[RepositoryDataMetadataBlock]:
        pass

    @abstractmethod
    def get_dataset(
        self, dataset_id: int, dataset_name: str
    ) -> RepositoryDataDataset:
        pass


class TomedaRepositoryFactory:
    """
    A factory class to dynamically create instances of
    TomedaRepositoryConnector.

    The factory uses the provided repository name to determine which specific
    repository connector to instantiate. It ensures that the specified module
    and class names are valid, imports the required module, and then
    instantiates the specified class, passing any additional arguments
    and keyword arguments to the class constructor.

    The factory ensures that the specified module and class names are valid,
    imports the required module, and then instantiates the specified class,
    passing any additional arguments and keyword arguments to the class
    constructor.

    This factory was chosen as the design pattern to facilitate the dynamic
    importation of classes. This dynamic import capability enables the system
    to seamlessly integrate with plugins, as specific repository connectors
    can be determined, imported, and instantiated at runtime based on the
    provided repository name.

    Methods
    -------
    __new__(cls, repository_name, *args, **kwargs) -> TomedaRepositoryConnector:
        Creates and returns an instance of a TomedaRepositoryConnector subclass.
    """

    TBaseClass = TypeVar("TBaseClass")
    TActualClass = TypeVar("TActualClass")

    def __new__(  # type: ignore[misc]
        cls,
        repository_name: str,
        *args: Any,
        **kwargs: Any,
    ) -> TomedaRepositoryConnector:
        """
        Create and return an instance of a TomedaRepositoryConnector subclass.

        The specific subclass is determined based on the provided repository
        name. Any additional arguments or keyword arguments are passed
        to the class constructor.

        Parameters
        ----------
        repository_name : str
            The name of the repository to determine which connector to
            instantiate.
        *args : tuple[Any, ...]
            Positional arguments to be passed to the class constructor.
        **kwargs : dict[str, Any]
            Keyword arguments to be passed to the class constructor.

        Returns
        -------
        TomedaRepositoryConnector
            An instance of the specified TomedaRepositoryConnector subclass.

        Raises
        ------
        TomedaFactoryTypeError
            If the provided names (repository, module, class) are not strings.
        TomedaRepositoryInvalidRepositoryNameError
            If the provided repository name is not supported.
        TomedaFactoryImportError
            If there's an error importing the specified module or class.
        TomedaFactoryInvalidClassError
            If the imported class is not a subclass of
            TomedaRepositoryConnector.
        TomedaFactoryInstantiationError
            If there's an error instantiating the specified class.
        """
        cls._validate_repository_name(repository_name)
        repository_data = cls._get_repository_data(repository_name)

        validator = cls._create_instance_from_data(
            TomedaRepositoryValidator,
            repository_data["validator"]["module"],
            repository_data["validator"]["class"],
            repository_name,
            (repository_name, "url"),
            {},
        )

        args = (repository_name, validator) + args
        # TODO: Check type of base_class
        return cls._create_instance_from_data(
            TomedaRepositoryConnector,  # type: ignore[type-abstract]
            repository_data["connector"]["module"],
            repository_data["connector"]["class"],
            repository_name,
            args,
            kwargs,
        )

    @classmethod
    def _create_instance_from_data(  # type: ignore[misc]
        cls,
        base_class: type[TBaseClass],
        module_name: str,
        class_name: str,
        repository_name: str,
        args: tuple[Any, ...],
        kwargs: dict[str, Any],
    ) -> TBaseClass:
        """
        Creates and returns an instance of the specified class.

        This method imports the specified module, retrieves the specified class,
        validates it, and then instantiates it using the provided arguments.

        Parameters
        ----------
        base_class : type[TBaseClass]
            The base class that the class to be instantiated should
            inherit from.
        module_name : str
            The name of the module from which the class will be imported.
        class_name : str
            The name of the class to be instantiated.
        repository_name : str
            The name of the repository for which the instance is being created.
        args : tuple[Any, ...]
            Positional arguments to be passed to the class constructor.
        kwargs : dict[str, Any]
            Keyword arguments to be passed to the class constructor.

        Returns
        -------
        TBaseClass
            An instance of the specified class.

        Raises
        ------
        TomedaFactoryTypeError, TomedaFactoryImportError,
        TomedaFactoryInvalidClassError, TomedaFactoryInstantiationError
            Refer to methods `_validate_module_name`, `_validate_class_name`,
            `_import_module`, `_import_class`, `_validate_class`, and
            `_instantiate_class` for details.
        """
        cls._validate_module_name(module_name)
        cls._validate_class_name(class_name)
        module: ModuleType = cls._import_module(module_name, repository_name)
        # TODO: Check type of base_class
        actual_class: type[base_class]  # type: ignore[valid-type]
        actual_class = cls._import_class(
            module,
            class_name,
            base_class,
            repository_name,
        )
        cls._validate_class(
            class_name,
            actual_class,
            base_class,
            repository_name,
        )
        instance = cls._instantiate_class(
            actual_class,
            class_name,
            repository_name,
            args,
            kwargs,
        )
        return instance

    @staticmethod
    def _instantiate_class(  # type: ignore[misc]
        actual_class: type[TActualClass],
        class_name: str,
        repository_name: str,
        args: tuple[Any, ...],
        kwargs: dict[str, Any],
    ) -> TActualClass:
        """
        Creates an instance of the specified repository class.

        Parameters
        ----------
        actual_class : type[TActualClass]
            The class to be instantiated.
        class_name : str
            The name of the class, used for error reporting.
        repository_name : str
            The name of the repository for which the class is being
            instantiated.
        args : tuple
            Positional arguments to be passed to the class constructor.
        kwargs : dict
            Keyword arguments to be passed to the class constructor.

        Returns
        -------
        TBaseClass
            An instance of the specified class.

        Raises
        ------
        TomedaFactoryInstantiationError
            If there's an error instantiating the specified class.
        """

        try:
            instance = actual_class(*args, **kwargs)
        except Exception as error:
            logger.trace(
                "Error instantiating class %s for repository %s: %s",
                class_name,
                repository_name,
                error,
            )
            raise TomedaFactoryInstantiationError(
                f"Error instantiating class {class_name} for repository "
                f"{repository_name}: {error}"
            ) from error
        return instance

    @staticmethod
    def _validate_class(
        class_name: str,
        actual_class: type[TActualClass],
        base_class: type[TBaseClass],
        repository_name: str,
    ) -> None:
        """
        Validates if the provided class is a subclass of the specified
        base class.

        Parameters
        ----------
        class_name : str
            The name of the class to validate.
        actual_class : type[TActualClass]
            The actual class to be validated.
        base_class : type[TBaseClass]
            The base class that the repository_class should inherit from.
        repository_name : str
            The name of the repository for which the class is being validated.

        Raises
        ------
        TomedaFactoryInvalidClassError
            If the provided class is not a subclass of the specified base class.
        """
        if not issubclass(actual_class, base_class):
            logger.error(
                "Class '%s' for repository '%s' is not a subclass of '%s'",
                class_name,
                repository_name,
                base_class.__name__,
            )
            raise TomedaFactoryInvalidClassError(
                f"Class '{class_name}' for repository '{repository_name}' is"
                f" not a subclass of '{base_class.__name__}'"
            )

    @staticmethod
    def _import_class(
        module: ModuleType,
        class_name: str,
        base_class: type[TBaseClass],
        repository_name: str,
    ) -> type[TBaseClass]:
        """
        Retrieves a class from the specified module.

        Parameters
        ----------
        module : ModuleType
            The module from which to retrieve the class.
        class_name : str
            The name of the class to retrieve.
        base_class : type[TBaseClass]
            The base class that the retrieved class should inherit from.
        repository_name : str
            The name of the repository for which the class is being imported.

        Returns
        -------
        type[TBaseClass]
            The retrieved class.

        Raises
        ------
        TomedaFactoryImportError
            If there's an error retrieving the class from the module or
            if the retrieved class does not inherit from the specified
            base class.
        """
        try:
            # TODO: Check type of base_class
            actual_class: type[base_class]  # type: ignore[valid-type]
            actual_class = getattr(module, class_name)
        except Exception as error:
            logger.trace(
                "Failed to retrieve class '%s' from module '%s' for "
                "repository '%s'. Error: %s",
                class_name,
                module.__name__,
                repository_name,
                error,
            )
            raise TomedaFactoryImportError(
                f"Failed to retrieve class '{class_name}' from module "
                f"'{module.__name__}' for repository '{repository_name}'. "
                f"Error: {error}"
            ) from error
        return actual_class

    @staticmethod
    def _import_module(module_name: str, repository_name: str) -> ModuleType:
        """
        Imports the specified module using importlib.

        Parameters
        ----------
        module_name : str
            The name of the module to import.
        repository_name : str
            The name of the repository for which the module is being imported.

        Returns
        -------
        ModuleType
            The imported module.

        Raises
        ------
        TomedaFactoryImportError
            If there's an error importing the specified module.
        """
        try:
            # `importlib` is not cached because the module is imported
            # only once (or very rarely).
            module: ModuleType = importlib.import_module(module_name)
        except Exception as error:
            logger.trace(
                "Cannot import module %s for repository %s: %s",
                module_name,
                repository_name,
                error,
            )
            raise TomedaFactoryImportError(
                f"Cannot import module {module_name} for repository "
                f"{repository_name}: {error}"
            ) from error
        return module

    @staticmethod
    def _validate_class_name(class_name: str) -> None:
        """
        Validates if the provided class name is a string.

        Parameters
        ----------
        class_name : str
            The name of the class to validate.

        Raises
        ------
        TomedaFactoryTypeError
            If the provided class_name is not a string.
        """
        if not isinstance(class_name, str) or not class_name:
            logger.trace("Class name is not a string or empty: %s", class_name)
            raise TomedaFactoryTypeError(
                f"Class name is not a string: {class_name}"
            )

    @staticmethod
    def _validate_module_name(module_name: str) -> None:
        """
        Validates if the provided module name is a string.

        Parameters
        ----------
        module_name : str
            The name of the module to validate.

        Raises
        ------
        TomedaFactoryTypeError
            If the provided module_name is not a string.
        """
        if not isinstance(module_name, str) or not module_name:
            logger.trace(
                "Module name is not a string or empty: %s", module_name
            )
            raise TomedaFactoryTypeError(
                f"Module name is not a string: {module_name}"
            )

    @staticmethod
    def _validate_repository_name(repository_name: str) -> None:
        """
        Validates if the provided repository name is a string and exists
        in the REPOSITORIES.

        Parameters
        ----------
        repository_name : str
            The name of the repository to validate.

        Raises
        ------
        TomedaFactoryTypeError
            If the provided repository_name is not a string.
        TomedaRepositoryInvalidRepositoryNameError
            If the provided repository_name is not found in REPOSITORIES.
        """
        if not isinstance(repository_name, str) or not repository_name:
            logger.trace(
                "Repository name is not a string or empty: %s", repository_name
            )
            raise TomedaFactoryTypeError(
                f"Repository name is not a string: {repository_name}"
            )
        if repository_name not in REPOSITORY_DATA:
            logger.trace(
                "Repository name is not supported: %s", repository_name
            )
            raise TomedaFactoryValueError(
                f"Repository name is not supported: {repository_name}"
            )

    @staticmethod
    def _get_repository_data(
        repository_name: str,
    ) -> dict[str, dict[str, str]]:
        """
        Retrieves the configuration data for the specified repository name.

        This method ensures that the data structure for the given
        repository name adheres to the expected format.

        Parameters
        ----------
        repository_name : str
            The name of the repository for which data is being retrieved.

        Returns
        -------
        dict[str, dict[str, str]]
            The configuration data for the specified repository.

        Raises
        ------
        TomedaFactoryValueError
            If the data for the given repository name does not match the
            expected format.
        """
        repo_data = REPOSITORY_DATA.get(repository_name)

        if not repo_data:
            logger.trace("No data found for repository: %s", repository_name)
            raise TomedaFactoryValueError(
                f"No data found for repository: {repository_name}"
            )

        for component in ["connector", "validator"]:
            if component not in repo_data:
                logger.trace(
                    "'%s' component missing for repository: %s",
                    component,
                    repository_name,
                )
                raise TomedaFactoryValueError(
                    f"'{component}' component missing for repository:"
                    f" {repository_name}"
                )

            component_data = repo_data[component]
            if not isinstance(component_data, dict):
                logger.trace(
                    "Invalid data format for component '%s' in repository: %s",
                    component,
                    repository_name,
                )
                raise TomedaFactoryValueError(
                    f"Invalid data format for component '{component}' in"
                    f" repository: {repository_name}"
                )

            for attribute in ["module", "class"]:
                if attribute not in component_data or not isinstance(
                    component_data[attribute], str
                ):
                    logger.trace(
                        "Invalid or missing '%s' for component '%s' in"
                        " repository: %s",
                        attribute,
                        component,
                        repository_name,
                    )
                    raise TomedaFactoryValueError(
                        f"Invalid or missing '{attribute}' for component"
                        f" '{component}' in repository: {repository_name}"
                    )

        return repo_data
