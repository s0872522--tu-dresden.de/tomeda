from dataclasses import dataclass


def bool_to_str_upper(value: bool) -> str:
    """Convert a boolean value to its uppercase string representation."""
    return str(value).upper()


@dataclass(kw_only=True, frozen=True)
class MetadataBlockLine:
    """Represents a metadata block line."""

    prefix: str
    name: str
    dataverse_alias: str
    display_name: str


@dataclass(frozen=True)
class MetadataBlock:
    """Represents a metadata block with a header and body."""

    body: MetadataBlockLine
    header: MetadataBlockLine = MetadataBlockLine(
        prefix="#metadataBlock",
        name="name",
        dataverse_alias="dataverseAlias",
        display_name="displayName",
    )


@dataclass(kw_only=True, frozen=True)
class DatasetFieldLine:
    """Represents a dataset field line with multiple attributes."""

    prefix: str
    name: str
    title: str
    description: str
    watermark: str
    field_type: str
    display_order: int | str
    display_format: str
    advanced_search_field: bool | str
    allow_controlled_vocabulary: bool | str
    allow_multiples: bool | str
    facetable: bool | str
    display_on_create: bool | str
    required: bool | str
    parent: str
    metadata_block_id: str
    term_uri: str

    def __post_init__(self):
        self._convert_bools_to_uppercase_strings()

    def _convert_bools_to_uppercase_strings(self):
        """
        Convert bools to uppercase strings
        """
        bool_fields = [
            "advanced_search_field",
            "allow_controlled_vocabulary",
            "allow_multiples",
            "facetable",
            "display_on_create",
            "required",
        ]

        for field_name in bool_fields:
            field_value = getattr(self, field_name)
            if isinstance(field_value, bool):
                """Allows setting values on frozen dataclasses"""
                object.__setattr__(
                    self, field_name, bool_to_str_upper(field_value)
                )


@dataclass(frozen=True)
class DatasetField:
    """Represents a dataset field block with a header and body."""

    body: list[DatasetFieldLine]
    header: DatasetFieldLine = DatasetFieldLine(
        prefix="#datasetField",
        name="name",
        title="title",
        description="description",
        watermark="watermark",
        field_type="fieldType",
        display_order="displayOrder",
        display_format="displayFormat",
        advanced_search_field="advancedSearchField",
        allow_controlled_vocabulary="allowControlledVocabulary",
        allow_multiples="allowmultiples",
        facetable="facetable",
        display_on_create="displayoncreate",
        required="required",
        parent="parent",
        metadata_block_id="metadatablock_id",
        term_uri="termURI",
    )


@dataclass(kw_only=True, frozen=True)
class ControlledVocabularyLine:
    """Represents a controlled vocabulary line."""

    prefix: str
    related_dataset_field: str
    value: str
    identifier: str
    display_order: int | str


@dataclass(frozen=True)
class ControlledVocabulary:
    """Represents the controlled vocabulary block with a header and body."""

    body: list[ControlledVocabularyLine]
    header: ControlledVocabularyLine = ControlledVocabularyLine(
        prefix="#controlledVocabulary",
        related_dataset_field="DatasetField",
        value="Value",
        identifier="identifier",
        display_order="displayOrder",
    )


@dataclass(kw_only=True, frozen=True)
class CustomMetadataBlock:
    """Represents a custom metadata block with associated fields."""

    metadata_block: MetadataBlock
    dataset_field: DatasetField
    controlled_vocabulary: ControlledVocabulary
