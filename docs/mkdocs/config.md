# Configuration

Die Konfiguration ermöglicht es welche Funktionen der Software verwendet werden, welche Daten verarbeitet werden sollen.

Für möglichst hohe Flexibilität gibt es mehrere Möglichkeiten die Konfiguration zu setzen. Diese Varianten können auch
kombiniert werden und haben eine absteigende Priorität.

* **CLI-Argumente** um schnell und einfach Einstellungen für einen einzelnen Lauf von ToMeDa anzupassen.
* **Umgebungsvariablen** für systemweite Konfiguration, Shell-Skripte und Shell-Sessions.
* **.env-Datei** für persistente und projektspezifische Konfiguration zentral zu verwalten, ohne die Systemumgebung zu
  beeinflussen.

## Config Möglichkeiten

### CLI-Argumente

Die CLI-Argumente sind die einfachste Möglichkeit die Konfiguration zu setzen. Sie werden als Parameter an das Skript
übergeben und überschreiben alle anderen Konfigurationsmöglichkeiten. Einige Beispiele:

```shell
tomedo --help

tomeda \
  --create-dataset-table \
  --schema-module dataset_schema.py \
  --schema-class Dataset \
  --output metadata_output_info_table
```

Flags (Argumente ohne Wert) schalten Parameter ein oder aus. Standardmäßig sind sie ausgeschaltet. Um sie einzuschalten,
muss der Flag angegeben werden z.B. `--create-dataset-table`. Um sie explizit auszuschalten oder andere Config zu
überschreiben muss der Flag mit einem `no-`-Präfix angegeben werden z.B. `--no-create-dataset-table`.

Optonen (Argumente mit Wert) setzen einen Wert für einen Parameter. Um einen Wert zu setzen, muss der Option der Wert
mit einem `=`-Zeichen angehängt werden z.B. `--schema-module dataset_schema.py`. Um eine Option explizit auszuschalten
oder andere Config zu überschreiben muss der Option der Wert mit einem `=`-Zeichen angehängt werden
z.B. `--schema-module=`.
