from pathlib import Path
from unittest import mock
from unittest.mock import MagicMock

import pytest

from tomeda.file_exception import (
    TomedaFileCreateError,
    TomedaFileDirectoryCreateError,
    TomedaFileEncodingError,
    TomedaFileExistsError,
    TomedaFileNotReadableError,
    TomedaFileNotWritableError,
    TomedaFilePermissionError,
    TomedaFileValueError,
)
from tomeda.file_handler import TomedaFileHandler


def setup_temp_file(tmp_path, permissions, content="Hello, world!"):
    temp_file = tmp_path / "temp.txt"
    bytes_content = content.encode("utf8")
    with temp_file.open("wb") as f:
        f.write(bytes_content)
    temp_file.chmod(permissions)
    return temp_file


def test_is_readable_readonly(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o400)
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_readable()

    assert result == True


def test_is_readable_writeonly(tmp_path):
    # Writeonly is interpreted differently by file systems and operating
    # systems. It is not trivially possible to check the
    # file system. It is also not enough to check only operating systems
    # or to check only via the permissions of the directory.
    # Therefore, the pathlib method must be mocked for the corresponding tests.

    temp_file = setup_temp_file(tmp_path, 0o200)
    file_handle = TomedaFileHandler(temp_file)

    with mock.patch("os.access", return_value=False):
        result = file_handle.is_readable()

    assert result == False


def test_is_readable_all_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_readable()

    assert result == True


def test_is_readable_no_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o000)
    file_handle = TomedaFileHandler(temp_file)

    with mock.patch("os.access", return_value=False):
        result = file_handle.is_readable()

    assert result == False


def test_is_readable_user_no_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o060)
    file_handle = TomedaFileHandler(temp_file)

    with mock.patch("os.access", return_value=False):
        result = file_handle.is_readable()

    assert result == False


def test_is_readable_no_exist(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_readable()

    assert result == False


def test_is_writeable_readonly(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o400)
    file_handle = TomedaFileHandler(temp_file)

    # Problem with overlayFS in Docker container
    with mock.patch("os.access", return_value=False):
        result = file_handle.is_writable()

    assert result == False


def test_is_writeable_writeonly(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o200)
    file_handle = TomedaFileHandler(temp_file)

    with mock.patch("os.access", return_value=False):
        result = file_handle.is_writable()

    assert result == False


def test_is_writeable_all_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_writable()

    assert result == True


def test_is_writeable_no_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o000)
    file_handle = TomedaFileHandler(temp_file)

    with mock.patch("os.access", return_value=False):
        result = file_handle.is_writable()

    assert result == False


def test_is_writeable_user_no_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o060)
    file_handle = TomedaFileHandler(temp_file)

    with mock.patch("os.access", return_value=False):
        result = file_handle.is_writable()

    assert result == False


def test_is_writeable_no_exist(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_writable()

    assert result == False


def test_is_existing_no_exist(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_existing()

    assert result == False


def test_is_existing_exist_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o600)
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_existing()

    assert result == True


def test_is_existing_exist_no_permission(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o060)
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle.is_existing()

    assert result == True


def test__check_overwrite_existing_file_overwrite_false(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o600)
    file_handle = TomedaFileHandler(temp_file, overwrite=False)

    with pytest.raises(TomedaFileExistsError):
        file_handle._check_overwrite()


def test__check_overwrite_existing_file_overwrite_true(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o600)
    file_handle = TomedaFileHandler(temp_file, overwrite=True)

    file_handle._check_overwrite()

    # Should not raise an error, so we don't have to check anything.


def test__check_overwrite_non_existing_file_overwrite_false(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file, overwrite=False)

    file_handle._check_overwrite()

    # Should not raise an error, so we don't have to check anything.


def test__check_overwrite_non_existing_file_overwrite_true(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file, overwrite=True)

    file_handle._check_overwrite()

    # Should not raise an error, so we don't have to check anything.


def test__check_writable_raises_error_when_not_writable_and_existing(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    with (
        mock.patch.object(file_handle, "is_writable", return_value=False),
        mock.patch.object(file_handle, "is_existing", return_value=True),
    ):
        with pytest.raises(TomedaFileNotWritableError):
            file_handle._check_writable()


def test__check_writable_no_error_when_not_writable_and_not_existing(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    with (
        mock.patch.object(file_handle, "is_writable", return_value=False),
        mock.patch.object(file_handle, "is_existing", return_value=False),
    ):
        file_handle._check_writable()  # Should not raise


def test__check_writable_no_error_when_writable_and_existing(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    with (
        mock.patch.object(file_handle, "is_writable", return_value=True),
        mock.patch.object(file_handle, "is_existing", return_value=True),
    ):
        file_handle._check_writable()  # Should not raise


def test__check_writable_no_error_when_writable_and_not_existing(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    with (
        mock.patch.object(file_handle, "is_writable", return_value=True),
        mock.patch.object(file_handle, "is_existing", return_value=False),
    ):
        file_handle._check_writable()  # Should not raise


def test_create_parent_dir_when_already_exists(tmp_path):
    parent_directory = tmp_path / "parent_directory"
    parent_directory.mkdir()
    file_path = parent_directory / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    # No exception should be raised in this case
    file_handle.create_parent_dir()


def test_create_parent_dir_when_not_exists(tmp_path):
    # Arrange
    parent_directory = tmp_path / "parent_directory"
    file_path = parent_directory / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    # Act
    file_handle.create_parent_dir()

    # Assert
    assert parent_directory.exists()


def test_create_parent_dir_when_not_exists_nested(tmp_path):
    # Arrange
    parent_directory = tmp_path / "parent_directory" / "nested"
    file_path = parent_directory / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    # Act
    file_handle.create_parent_dir()

    # Assert
    assert parent_directory.exists()


def test_create_parent_dir_permission_error(tmp_path):
    # Arrange
    parent_directory = tmp_path / "parent_directory"
    file_path = parent_directory / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    # Act
    with mock.patch.object(Path, "mkdir", side_effect=PermissionError):
        with pytest.raises(TomedaFilePermissionError):
            file_handle.create_parent_dir()


def test_create_parent_dir_io_error(tmp_path):
    # Arrange
    parent_directory = tmp_path / "parent_directory"
    file_path = parent_directory / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    # Act
    with mock.patch.object(Path, "mkdir", side_effect=IOError):
        with pytest.raises(TomedaFileDirectoryCreateError):
            file_handle.create_parent_dir()


def test__convert_line_to_text_none(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle._convert_line_to_text(None)

    assert result == "\n"


def test__convert_line_to_text_single_string(tmp_path):
    # Arrange
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle._convert_line_to_text("Hello, world!")
    assert result == "Hello, world!\n"

    result = file_handle._convert_line_to_text("Hello, world!\n")
    assert result == "Hello, world!\n"

    result = file_handle._convert_line_to_text("Hello, world!\r\n")
    assert result == "Hello, world!\n"

    result = file_handle._convert_line_to_text("Hello, world!\r")
    assert result == "Hello, world!\n"

    result = file_handle._convert_line_to_text("Hello, world!\n\r")
    assert result == "Hello, world!\n"


def test__convert_line_to_text_list_of_strings(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    result = file_handle._convert_line_to_text(["Hello,", "world!"])
    assert result == "Hello,\nworld!\n"

    result = file_handle._convert_line_to_text(["Hello,", "", "world!"])
    assert result == "Hello,\n\nworld!\n"

    result = file_handle._convert_line_to_text(["Hello,", "    ", "world!"])
    assert result == "Hello,\n    \nworld!\n"


def test__convert_line_to_text_invalid_type(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    with pytest.raises(TomedaFileValueError):
        file_handle._convert_line_to_text(42)  # Non-string, non-list type


def test__convert_line_to_text_list_contains_non_string(tmp_path):
    temp_file = tmp_path / "temp.txt"
    file_handle = TomedaFileHandler(temp_file)

    with pytest.raises(TomedaFileValueError):
        # List contains non-string type
        file_handle._convert_line_to_text([42])

    with pytest.raises(TomedaFileValueError):
        # List contains non-string type and string type
        file_handle._convert_line_to_text(["Hello,", 42, "world!"])


def test_create_file_when_file_does_not_exist(tmp_path):
    temp_file = tmp_path / "new_file.txt"
    file_handle = TomedaFileHandler(temp_file)

    file_handle.create_file()

    assert temp_file.exists(), "File should be created"


def test_create_file_when_file_already_exists(tmp_path):
    temp_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(temp_file)

    file_handle.create_file()

    assert temp_file.exists(), "File should still exist"


def test_create_file_when_permission_error(tmp_path, mocker):
    temp_file = tmp_path / "no_permission.txt"
    file_handle = TomedaFileHandler(temp_file)

    mocker.patch.object(Path, "touch", side_effect=PermissionError)

    with pytest.raises(TomedaFilePermissionError):
        file_handle.create_file()


def test_create_file_when_io_error(tmp_path, mocker):
    temp_file = tmp_path / "io_error.txt"
    file_handle = TomedaFileHandler(temp_file)

    mocker.patch.object(Path, "touch", side_effect=IOError)

    with pytest.raises(TomedaFileCreateError):
        file_handle.create_file()


def test_read_with_raw_true(tmp_path):
    test_file = setup_temp_file(
        tmp_path,
        0o777,
        " Test line 1 \n \n Test line 2 \r\n Test line 3 \r Test line 4"
        " \n\r Test line 5 ",
    )
    file_handle = TomedaFileHandler(test_file)

    result = file_handle.read(raw=True, strip=True)
    assert result == [
        " Test line 1 \n \n Test line 2 \r\n Test line 3 \r Test line 4"
        " \n\r Test line 5 ",
    ]

    result = file_handle.read(raw=True, strip=False)
    assert result == [
        " Test line 1 \n \n Test line 2 \r\n Test line 3 \r Test line 4"
        " \n\r Test line 5 ",
    ]


def test_read_with_raw_false_and_strip_true(tmp_path):
    test_file = setup_temp_file(
        tmp_path,
        0o777,
        " Test line 1 \n \n Test line 2 \r\n Test line 3 \r Test line 4"
        " \n\r Test line 5 ",
    )
    file_handle = TomedaFileHandler(test_file)

    result = file_handle.read(raw=False, strip=True)
    assert result == [
        "Test line 1",
        "Test line 2",
        "Test line 3",
        "Test line 4",
        "Test line 5",
    ]


def test_read_with_raw_false_and_strip_false(tmp_path):
    test_file = setup_temp_file(
        tmp_path,
        0o777,
        " Test line 1 \n \n Test line 2 \r\n Test line 3 \r Test line 4"
        " \n\r Test line 5 ",
    )
    file_handle = TomedaFileHandler(test_file)

    result = file_handle.read(raw=False, strip=False)
    assert result == [
        " Test line 1 ",
        " ",
        " Test line 2 ",
        " Test line 3 ",
        " Test line 4 ",
        "",
        " Test line 5 ",
    ]


def test_read_with_not_readable_file(tmp_path):
    test_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(test_file)

    with mock.patch.object(
        TomedaFileHandler, "is_readable", return_value=False
    ):
        with pytest.raises(TomedaFileNotReadableError):
            file_handle.read()


def test_read_with_permission_error(tmp_path):
    test_file = setup_temp_file(tmp_path, 0o777, " Test line 1 \n Test line 2 ")
    file_handle = TomedaFileHandler(test_file)

    with mock.patch.object(Path, "open", side_effect=PermissionError):
        with pytest.raises(TomedaFilePermissionError):
            file_handle.read()


def test_read_with_encoding_error(tmp_path):
    test_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(test_file)

    with mock.patch.object(
        Path,
        "open",
        side_effect=UnicodeDecodeError("utf-8", b"", 0, 1, "invalid utf-8"),
    ):
        with pytest.raises(TomedaFileEncodingError):
            file_handle.read()


def test_read_with_io_error(tmp_path):
    test_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(test_file)

    with mock.patch.object(Path, "open", side_effect=IOError):
        with pytest.raises(TomedaFileNotReadableError):
            file_handle.read()


@pytest.mark.parametrize(
    "error, exception, attempt",
    [
        (ValueError(), TomedaFileEncodingError, 0),
        (
            PermissionError(),
            TomedaFileNotWritableError,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 1,
        ),
        (
            PermissionError(),
            TomedaFileNotWritableError,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS,
        ),
        (
            IOError(),
            TomedaFileNotWritableError,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 1,
        ),
        (
            IOError(),
            TomedaFileNotWritableError,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS,
        ),
        (
            Exception(),
            TomedaFileNotWritableError,
            0,
        ),
    ],
)
def test_handle_write_error_raises(tmp_path, error, exception, attempt):
    test_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(test_file)

    with pytest.raises(exception):
        file_handle._handle_write_error(error, attempt)


@pytest.mark.parametrize(
    "error, attempt",
    [
        (PermissionError(), -1),
        (PermissionError(), 0),
        (PermissionError(), TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2),
        (IOError(), -1),
        (IOError(), 0),
        (IOError(), TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2),
    ],
)
def test_handle_write_error_retries(tmp_path, error, attempt):
    test_file = setup_temp_file(tmp_path, 0o777)
    file_handle = TomedaFileHandler(test_file)

    assert file_handle._handle_write_error(error, attempt)


def test_write_with_dryrun(tmp_path):
    file_path = tmp_path / "file.txt"
    file_handle = TomedaFileHandler(file_path, dryrun=True)

    with mock.patch.object(
        file_handle, "_handle_write_error", new_callable=MagicMock
    ):
        file_handle.write("Hello, World!")

    assert not file_path.exists()


def test_write_content_as_string(tmp_path):
    file_path = tmp_path / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    with mock.patch.object(
        file_handle, "_handle_write_error", new_callable=MagicMock
    ):
        file_handle.write("Hello, World!")
        file_handle._handle_write_error.assert_not_called()

    assert file_path.read_bytes() == b"Hello, World!\n"


def test_write_content_as_list(tmp_path):
    file_path = tmp_path / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    with mock.patch.object(
        file_handle, "_handle_write_error", new_callable=MagicMock
    ):
        file_handle.write(["Hello,", "", " ", "World!"])
        file_handle._handle_write_error.assert_not_called()

    assert file_path.read_bytes() == b"Hello,\n\n \nWorld!\n"


def test_write_content_as_invalid_list(tmp_path):
    file_path = tmp_path / "file.txt"
    file_handle = TomedaFileHandler(file_path)

    with mock.patch.object(
        file_handle, "_handle_write_error", new_callable=MagicMock
    ):
        with pytest.raises(TomedaFileValueError):
            file_handle.write(["Hello,", "", " ", 5, "World!"])


@pytest.mark.parametrize(
    "error, exec_attempt, expected_attempts, exception",
    [
        (IOError(), 1, 1, None),
        (
            IOError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            None,
        ),
        (
            IOError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            TomedaFileNotWritableError,
        ),
        (
            IOError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS + 2,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            TomedaFileNotWritableError,
        ),
        (
            PermissionError(),
            2,
            2,
            None,
        ),
        (
            PermissionError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            None,
        ),
        (
            PermissionError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            TomedaFileNotWritableError,
        ),
        (
            PermissionError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS + 2,
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            TomedaFileNotWritableError,
        ),
        (
            ValueError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            1,
            TomedaFileEncodingError,
        ),
        (
            ValueError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS,
            1,
            TomedaFileEncodingError,
        ),
        (
            ValueError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS + 2,
            1,
            TomedaFileEncodingError,
        ),
        (
            Exception(),
            2,
            1,
            TomedaFileNotWritableError,
        ),
        (
            Exception(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS - 2,
            1,
            TomedaFileNotWritableError,
        ),
        (
            Exception(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS,
            1,
            TomedaFileNotWritableError,
        ),
        (
            ArithmeticError(),
            TomedaFileHandler.MAX_WRITE_ATTEMPTS + 2,
            1,
            TomedaFileNotWritableError,
        ),
    ],
)
def test_write_with_some_attempts(
    tmp_path, error, exec_attempt, expected_attempts, exception
):
    file_path = tmp_path / "file.txt"
    file_handle = TomedaFileHandler(file_path)
    TomedaFileHandler.MAX_WRITE_ATTEMPTS = 3

    with mock.patch.object(
        Path, "write_text", side_effect=[error] * (exec_attempt - 1) + [None]
    ) as mocked_write_text:
        if exception is not None:
            with pytest.raises(exception):
                file_handle.write("Test content")
        else:
            file_handle.write("Test content")

        assert mocked_write_text.call_count == expected_attempts
