from dataclasses import dataclass
from typing import List, Union

from tomeda.repository import JSONType


@dataclass
class RepositoryDataServer:
    """
    Represents server-related data for a generic repository in the
    Tomeda system.

    This class captures detailed information regarding the server, its
    build, version, and the API version used for making requests.

    Attributes
    ----------
    server : str
        A message or identifier related to the repository server.
    build : str
        The build information of the repository server.
    version : str
        The version number of the repository server.
    api_version : str
        The version of the API used to make the requests.
    """

    server: str
    build: str
    version: str
    api_version: str


@dataclass
class RepositoryDataUser:  # pylint: disable=too-many-instance-attributes
    id_: int
    username: str
    firstname: str
    lastname: str
    display_name: str
    email: str
    superuser: bool
    deactivated: bool
    create_date: str
    raw_data: JSONType


@dataclass
class RepositoryDataCollection:
    """
    Represents metadata for a generic repository in the Tomeda system.

    This class provides a structured representation for the metadata
    associated with a repository, regardless of its specific type. The
    design is meant to be generic to accommodate diverse repository
    types and is intended to align with the base class
    `TomedaRepositoryConnector`.

    Attributes
    ----------
    id : int
        A unique identifier for the repository.
    id_alias : str
        An alias or shorthand for the repository, often used for display
        or quick reference.
    name : str
        The full name or title of the repository.
    creation_date : str
        The date and time when the repository was created. This is a
        string representation and might follow the ISO 8601 format
        (e.g., '2023-07-26T11:38:51Z').
    contact : list[str]
        A list of contact email addresses associated with the repository.
    category : str
        The category or type of the repository
        (e.g., 'RESEARCHERS', 'DATA', etc.).
    raw_data : JSONType
        The original response data received from the server, stored as a
        dictionary.
    """

    id: int
    id_alias: str
    name: str
    creation_date: str | None
    contact: list[str]
    category: str | None
    raw_data: JSONType


@dataclass
# pylint: disable=too-many-instance-attributes
class RepositoryDataCollectionContent:
    id_: int
    id_alias: str
    persistent_id: str
    type_: str
    publication_date: str
    publisher: str
    authority: str
    persistent_url: str | None
    raw_data: JSONType


@dataclass
# pylint: disable=too-many-instance-attributes
class RepositoryDataMetadataBlockFieldDefinition:
    name: str
    display_name: str
    title: str
    type_: str
    description: str
    format_: str
    is_controlled_vocabulary: bool
    multiple: bool
    watermark: str
    raw_data: JSONType


@dataclass
class RepositoryDataMetadataBlockFieldValue:
    # typing type requires forward reference
    value: str | List[Union[str, "RepositoryDataMetadataBlockFieldValue"]]
    title: str
    type_: str
    multiple: bool
    raw_data: JSONType


@dataclass
class RepositoryDataMetadataBlock:
    id_: int | None
    name: str
    display_name: str
    field_definition: list[RepositoryDataMetadataBlockFieldDefinition] | None
    field_value: list[RepositoryDataMetadataBlockFieldValue] | None
    raw_data: JSONType


@dataclass
class RepositoryDataDatasetVersion:
    id_: int
    version: str
    version_state: str
    metadata_block: dict[str, RepositoryDataMetadataBlock]
    files: list[str]
    create_date: str
    release_date: str
    publication_date: str
    citation_date: str
    last_update_date: str
    raw_data: JSONType


@dataclass
class RepositoryDataDataset:
    id_: int
    id_alias: str
    persistent_id: str
    latest_version: RepositoryDataDatasetVersion
    publisher: str
    authority: str
    persistent_url: str
    publication_date: str
    raw_data: JSONType
