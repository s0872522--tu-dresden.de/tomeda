"""
This script is used to extract keys from Dataverse TSV files. It is used in the
following way:

"""

import csv
import logging
import os
from pathlib import Path

from .file_handler import TomedaFileHandler
from .params import TomedaParameter
from .tomeda_logging import TraceLogger

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


def main(param: TomedaParameter) -> None:
    """
    Main function that extracts keys from Dataverse TSV files.
    """
    folder_path = Path(param.tsv_dir[0])
    output_path = Path(param.output)

    if not folder_path.is_dir():
        logging.critical(f"Folder {folder_path} does not exist.")
        return

    if not output_path.is_dir() or not os.access(output_path, os.W_OK):
        logging.critical(
            f"Output folder {output_path} does not exist "
            f"or is not writable."
        )
        return

    for tsv_file in folder_path.glob("*.tsv"):
        keys = extract_keys(tsv_file)
        key_file = output_path / tsv_file.with_suffix(".keys").name
        write_keys(keys, key_file, param.force_overwrite)


def extract_keys(tsv_file: Path) -> list:
    """
    Extracts keys from a given TSV file.

    Parameters
    ----------
    tsv_file : Path
        The input TSV file path.

    Returns
    -------
    list
        A list of keys.
    """

    tsv_file_handle = TomedaFileHandler(tsv_file)
    file_data = tsv_file_handle.read(strip=False)
    file_data = file_data[2:]
    reader = csv.DictReader(file_data, delimiter="\t")

    keys = []
    for row in reader:
        first_column_value = row["#datasetField"]
        if first_column_value.startswith("#controlledVocabulary"):
            break
        keys.append(row["name"])

    return keys


def write_keys(keys: list, file: Path, overwrite: bool) -> None:
    """
    Writes keys to a given file.

    Parameters
    ----------
    keys : list
        The list of keys to write.
    file : Path
        The output file path.
    overwrite : bool
        Whether to overwrite the output file if it exists.
    """
    key_file_handle = TomedaFileHandler(file, overwrite=overwrite)
    key_file_handle.write(keys)
