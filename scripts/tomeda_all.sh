#!/bin/bash

set -euo pipefail

echo "Sourcing DOFF_00"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_DOFF_00_dump_documentation"
echo "Sourcing DOFF_01"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_DOFF_01_dump_template"
echo "Sourcing DOFF_02"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_DOFF_02_dump_info_struct"
echo "Sourcing DOFF_03"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_DOFF_03_extract_TSV_keys"
echo "Sourcing DOFF_04"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_DOFF_04_derive_dataverse_key"
echo "Sourcing DOFF_05"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_DOFF_05_derive_dataverse_tsv"
echo "Sourcing USER_01"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_USER_01_extract_metadata"
echo "Sourcing USER_02"
source "$(dirname "${BASH_SOURCE[0]}")/tomeda_USER_02_map_metadata_to_dataverse"
