"""
The module serves as a central interface for configuration management in
ToMeDa. It provides robust support for command line argument processing,
handling of environment variables and dotenv files.

The main component of this module is the `TomedaParameter`
dataclass, which stores values from various sources with prioritization.
These values are fundamentally validated through properties within the
dataclass.

The `get_parameters` method allows for the creation of an instance of
`TomedaParameterValues`, populated with the appropriate values.

This module facilitates effective configuration management by streamlining
the integration of command line arguments, environment variables, and
configuration files, ensuring a coherent and prioritized collection of
configuration parameters.
"""

import argparse
import logging
import os
from dataclasses import dataclass, field
from pathlib import Path

from dotenv import dotenv_values

from .default_exception import (
    TomedaNotAPathError,
    TomedaUnexpectedTypeError,
    TomedaValueError,
)
from .tomeda_logging import TraceLogger

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


@dataclass
# pylint: disable=too-many-instance-attributes,too-many-public-methods
class TomedaParameter:
    """
    A data class to store and manage parameters for the Tomeda application.

    This class uses properties to enforce type checking and validation
    for each parameter. It also includes a method to validate
    interdependencies between different parameters.

    Attributes
    ----------
    _gatherer_file : Path | None
        Path to the gatherer file.
    _schema_module : str | None
        Python module to import the schema from.
    _schema_class : str | None
        Python class name to import the schema from.
    _read : bool
        Flag indicating whether to read a filled-in gatherer file.
    _collector_root : Path | None
        Root directory (or file) of the Tomeda collector process.

    _output : Path | None
        Path to output the metadata as JSON.
    _force_overwrite : bool
        Flag to overwrite the existing configuration file if it exists.
    _dry_run : bool
        Flag for a dry run, executing the script without making actual changes.

    _create_gatherer : bool
        Flag to create an empty gatherer file.
    _create_schema_documentation : bool
        Flag to create the schema documentation.
    _create_dataset_table : bool
        Flag to create the dataset table.
    _check_recommendations : bool
        Flag to check if the recommendations are met.
    _extract_repository_metadata : bool
        Flag to extract metadata from the data repository.
    _derive_repository_keys : bool
        Flag to derive keys from the data repository.
    _derive_repository_tsv_from_mapping : bool
        Flag to derive TSV files from the mapping table.
    _create_repository_compatible_metadata : bool
        Flag to create repository compatible metadata.
    _tsv_dir : list[Path]
        Directories containing TSV files.
    _matched_entries : Path | None
        Path to the matched entries file.
    _schema_info_table : Path | None
        Path to the schema info table.
    _new_keys : list[Path]
        List of paths to the new keys file.

    _dataset_metadata : Path | None
        Path to the dataset metadata file.
    _mapping_table : Path | None
        Mapping table, serving as an input.

    _upload : bool
        Flag to upload the metadata JSON to the data repository server.
    _upload_file : Path | None
        Path to the upload file.
    _server_url : str | None
        URL of the data repository server.
    _target_collection : str | None
        Name of the target collection in the data repository.
    _query_fields : list[str]
        Fields used to check if the dataset already exists.
    _api_token : str | None
        API token for authentication at the data repository server.

    Methods
    -------
    validate_interdependent_attributes()
        Validates the interdependencies between different parameters.

    Example
    -------
    >>> params = TomedaParameter()
    >>> params.dry_run = '1'
    >>> params.validate_interdependent_attributes()
    """

    _gatherer_file: Path | None = field(default=None, init=False)
    _schema_module: str | None = field(default=None, init=False)
    _schema_class: str | None = field(default=None, init=False)
    _read: bool = field(default=False, init=False)
    _collector_root: Path | None = field(default=None, init=False)

    _output: Path | None = field(default=None, init=False)
    _force_overwrite: bool = field(default=False, init=False)
    _dry_run: bool = field(default=False, init=False)

    _create_gatherer: bool = field(default=False, init=False)
    _create_schema_documentation: bool = field(default=False, init=False)
    _create_dataset_table: bool = field(default=False, init=False)
    _check_recommendations: bool = field(default=False, init=False)
    _extract_repository_metadata: bool = field(default=False, init=False)
    _derive_repository_keys: bool = field(default=False, init=False)
    _derive_repository_tsv_from_mapping: bool = field(default=False, init=False)
    _create_repository_compatible_metadata: bool = field(
        default=False, init=False
    )
    _tsv_dir: list[Path] = field(default_factory=list[Path], init=False)
    _matched_entries: Path | None = field(default=None, init=False)
    _schema_info_table: Path | None = field(default=None, init=False)
    _new_keys: list[Path] = field(default_factory=list[Path], init=False)

    _dataset_metadata: Path | None = field(default=None, init=False)
    _mapping_table: Path | None = field(default=None, init=False)

    _upload: bool = field(default=False, init=False)
    _upload_file: Path | None = field(default=None, init=False)
    _server_url: str | None = field(default=None, init=False)
    _target_collection: str | None = field(default=None, init=False)
    _query_fields: list[str] = field(default_factory=list[str], init=False)
    _api_token: str | None = field(default=None, init=False)

    @property
    def gatherer_file(self) -> Path | None:
        """Path to the gatherer file."""
        return self._gatherer_file

    @gatherer_file.setter
    def gatherer_file(self, value: str | Path | None) -> None:
        """
        Sets the gatherer file path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the gatherer file path. If the value is a string,
            it will be converted to a Path object. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._gatherer_file = self._convert_to_path(value, self._gatherer_file)

    @property
    def schema_module(self) -> str | None:
        """Python module to import the schema from."""
        return self._schema_module

    @schema_module.setter
    def schema_module(self, value: str | None) -> None:
        """
        Sets the schema module.

        Parameters
        ----------
        value : str or None
            The new value of the schema module. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string and not None.
        """
        self._schema_module = self._convert_to_string(
            value, self._schema_module
        )

    @property
    def schema_class(self) -> str | None:
        """Python class name to import the schema from."""
        return self._schema_class

    @schema_class.setter
    def schema_class(self, value: str | None) -> None:
        """
        Sets the schema class.

        Parameters
        ----------
        value : str or None
            The new value of the schema class. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string and not None.
        """
        self._schema_class = self._convert_to_string(value, self._schema_class)

    @property
    def read(self) -> bool:
        """Flag indicating whether to read a filled-in gatherer file."""
        return self._read

    @read.setter
    def read(self, value: bool | str | None) -> None:
        """
        Sets the read flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the read flag. If the value is a string,
            it will be converted to a boolean based on common string
            representations of true/false values. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._read = self._convert_to_bool(value, self._read)

    @property
    def collector_root(self) -> Path | None:
        """Root directory (or file) of the Tomeda collector process."""
        return self._collector_root

    @collector_root.setter
    def collector_root(self, value: str | Path | None) -> None:
        """
        Sets the collector root path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the collector root path. If the value is a string,
            it will be converted to a Path object. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._collector_root = self._convert_to_path(
            value, self._collector_root
        )

    @property
    def output(self) -> Path | None:
        """Path to output the metadata as JSON."""
        return self._output

    @output.setter
    def output(self, value: str | Path | None) -> None:
        """
        Sets the output path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the output path. If the value is a string,
            it will be converted to a Path object. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._output = self._convert_to_path(value, self._output)

    @property
    def force_overwrite(self) -> bool:
        """Flag to overwrite the existing configuration file if it exists."""
        return self._force_overwrite

    @force_overwrite.setter
    def force_overwrite(self, value: bool | str | None) -> None:
        """
        Sets the force overwrite flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the force overwrite flag. If the value is a string,
            it will be converted to a boolean based on common string
            representations of true/false values. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._force_overwrite = self._convert_to_bool(
            value, self._force_overwrite
        )

    @property
    def dry_run(self) -> bool:
        """Flag for dry run."""
        return self._dry_run

    @dry_run.setter
    def dry_run(self, value: str | bool | None) -> None:
        """
        Sets the dry run flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the dry run flag. If the value is a string,
            it will be converted to a boolean based on common string
            representations of true/false values. If None is passed,
            the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._dry_run = self._convert_to_bool(value, self._dry_run)

    @property
    def create_gatherer(self) -> bool:
        """Flag to create an empty gatherer file."""
        return self._create_gatherer

    @create_gatherer.setter
    def create_gatherer(self, value: bool | str | None) -> None:
        """
        Sets the creating gatherer flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the creating gatherer flag. If the value is
            a string, it will be converted to a boolean based on common
            string representations of true/false values. If None is passed,
            the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._create_gatherer = self._convert_to_bool(
            value, self._create_gatherer
        )

    @property
    def create_schema_documentation(self) -> bool:
        """Flag to create the schema documentation."""
        return self._create_schema_documentation

    @create_schema_documentation.setter
    def create_schema_documentation(self, value: bool | str | None) -> None:
        """
        Sets the creating schema documentation flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the creating schema documentation flag. If
            the value is a string, it will be converted to a boolean
            based on common string representations of true/false values.
            If None is passed, the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._create_schema_documentation = self._convert_to_bool(
            value, self._create_schema_documentation
        )

    @property
    def create_dataset_table(self) -> bool:
        """Flag to create the dataset table."""
        return self._create_dataset_table

    @create_dataset_table.setter
    def create_dataset_table(self, value: bool | str | None) -> None:
        """
        Sets the creating dataset table flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the creating dataset table flag. If the value
            is a string, it will be converted to a boolean based on
            common string representations of true/false values. If None
            is passed, the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._create_dataset_table = self._convert_to_bool(
            value, self._create_dataset_table
        )

    @property
    def check_recommendations(self) -> bool:
        """Flag to check if the recommendations are met."""
        return self._check_recommendations

    @check_recommendations.setter
    def check_recommendations(self, value: bool | str | None) -> None:
        """
        Sets the check recommendations flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the check recommendations flag. If the
            value is a string, it will be converted to a boolean based
            on common string representations of true/false values. If
            None is passed, the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._check_recommendations = self._convert_to_bool(
            value, self._check_recommendations
        )

    @property
    def extract_repository_metadata(self) -> bool:
        """Flag to extract metadata from the data repository."""
        return self._extract_repository_metadata

    @extract_repository_metadata.setter
    def extract_repository_metadata(self, value: bool | str | None) -> None:
        """
        Sets the extract repository metadata flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the extract repository metadata flag. If
            the value is a string, it will be converted to a boolean
            based on common string representations of true/false values.
            If None is passed, the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._extract_repository_metadata = self._convert_to_bool(
            value, self._extract_repository_metadata
        )

    @property
    def derive_repository_keys(self) -> bool:
        """Flag to derive keys from the data repository."""
        return self._derive_repository_keys

    @derive_repository_keys.setter
    def derive_repository_keys(self, value: bool | str | None) -> None:
        """
        Sets the derive repository keys flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the derive repository keys flag. If the
            value is a string, it will be converted to a boolean based
            on common string representations of true/false values. If
            None is passed, the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._derive_repository_keys = self._convert_to_bool(
            value, self._derive_repository_keys
        )

    @property
    def derive_repository_tsv_from_mapping(self) -> bool:
        """Flag to derive TSV files from the mapping table."""
        return self._derive_repository_tsv_from_mapping

    @derive_repository_tsv_from_mapping.setter
    def derive_repository_tsv_from_mapping(
        self, value: bool | str | None
    ) -> None:
        """
        Sets the derive repository TSV from mapping flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the derive repository TSV from mapping flag.
            If the value is a string, it will be converted to a boolean
            based on common string representations of true/false values.
            If None is passed, the current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._derive_repository_tsv_from_mapping = self._convert_to_bool(
            value, self._derive_repository_tsv_from_mapping
        )

    @property
    def create_repository_compatible_metadata(self) -> bool:
        """Flag to create repository compatible metadata."""
        return self._create_repository_compatible_metadata

    @create_repository_compatible_metadata.setter
    def create_repository_compatible_metadata(
        self, value: bool | str | None
    ) -> None:
        """
        Sets the creating repository compatible metadata flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the creating repository compatible metadata
            flag. If the value is a string, it will be converted to a
            boolean based on common string representations of true/false
            values. If None is passed, the current value continues to
            be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._create_repository_compatible_metadata = self._convert_to_bool(
            value, self._create_repository_compatible_metadata
        )

    @property
    def tsv_dir(self) -> list[Path]:
        """Directories containing TSV files."""
        return self._tsv_dir

    @tsv_dir.setter
    def tsv_dir(
        self, value: str | list[str | Path | None] | Path | None
    ) -> None:
        """
        Sets the TSV directory paths.

        Parameters
        ----------
        value : str | list[str | Path] | Path | None
            The new value of the TSV directory paths. If the value is a string,
            it will be converted to a Path object. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._tsv_dir = self._convert_to_list_path(value, self._tsv_dir)

    @property
    def matched_entries(self) -> Path | None:
        """Path to the matched entries file."""
        return self._matched_entries

    @matched_entries.setter
    def matched_entries(self, value: str | Path | None) -> None:
        """
        Sets the matched entries path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the 'matched entries' path. If the value is
            a string, it will be converted to a Path object. If None is
            passed, the current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._matched_entries = self._convert_to_path(
            value, self._matched_entries
        )

    @property
    def schema_info_table(self) -> Path | None:
        """Path to the schema info table."""
        return self._schema_info_table

    @schema_info_table.setter
    def schema_info_table(self, value: str | Path | None) -> None:
        """
        Sets the schema info table path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the schema info table path. If the value is
            a string, it will be converted to a Path object. If None is
            passed, the current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._schema_info_table = self._convert_to_path(
            value, self._schema_info_table
        )

    @property
    def new_keys(self) -> list[Path]:
        """Path to the new keys file."""
        return self._new_keys

    @new_keys.setter
    def new_keys(
        self, value: str | list[str | Path | None] | Path | None
    ) -> None:
        """
        Sets the new keys path.

        Parameters
        ----------
        value : str | list[str | Path | None] | Path | None
            The new value of the 'new keys' path. If the value is a string,
            it will be converted to a Path object. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._new_keys = self._convert_to_list_path(value, self._new_keys)

    @property
    def dataset_metadata(self) -> Path | None:
        """Path to the dataset metadata file."""
        return self._dataset_metadata

    @dataset_metadata.setter
    def dataset_metadata(self, value: str | Path | None) -> None:
        """
        Sets the dataset metadata path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the dataset metadata path. If the value is
            a string, it will be converted to a Path object. If None is
            passed, the current value continues to be used.

        Raises
        ------
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        """
        self._dataset_metadata = self._convert_to_path(
            value, self._dataset_metadata
        )

    @property
    def mapping_table(self) -> Path | None:
        """Mapping table, serving as an input."""
        return self._mapping_table

    @mapping_table.setter
    def mapping_table(self, value: str | Path | None) -> None:
        """
        Sets the mapping table path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the mapping table path. If the value is a string,
            it will be converted to a Path object. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._mapping_table = self._convert_to_path(value, self._mapping_table)

    @property
    def upload(self) -> bool:
        """Flag to upload the metadata JSON to the data repository server."""
        return self._upload

    @upload.setter
    def upload(self, value: bool | str | None) -> None:
        """
        Sets the upload flag.

        Parameters
        ----------
        value : bool, str or None
            The new value of the upload flag. If the value is a string,
            it will be converted to a boolean based on common string
            representations of true/false values. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        self._upload = self._convert_to_bool(value, self._upload)

    @property
    def upload_file(self) -> Path | None:
        """Path to the upload file."""
        return self._upload_file

    @upload_file.setter
    def upload_file(self, value: str | Path | None) -> None:
        """
        Sets the upload file path.

        Parameters
        ----------
        value : str, Path or None
            The new value of the upload file path. If the value is a string,
            it will be converted to a Path object. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        self._upload_file = self._convert_to_path(value, self._upload_file)

    @property
    def server_url(self) -> str | None:
        """URL of the data repository server."""
        return self._server_url

    @server_url.setter
    def server_url(self, value: str | None) -> None:
        """
        Sets the server URL.

        Parameters
        ----------
        value : str or None
            The new value of the server URL. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string or None.
        """
        self._server_url = self._convert_to_string(value, self._server_url)

    @property
    def target_collection(self) -> str | None:
        """Name of the target collection in the data repository."""
        return self._target_collection

    @target_collection.setter
    def target_collection(self, value: str | None) -> None:
        """
        Sets the target collection name.

        Parameters
        ----------
        value : str or None
            The new value of the target collection name. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string or None.
        """
        self._target_collection = self._convert_to_string(
            value, self._target_collection
        )

    @property
    def query_fields(self) -> list[str]:
        """Fields used to check if the dataset already exists."""
        return self._query_fields

    @query_fields.setter
    def query_fields(self, value: str | list[str | None] | None) -> None:
        """
        Sets the query fields.

        Parameters
        ----------
        value : str | list[str | None] | None
            The new value of the query fields. If the value is a string,
            it will be split into a list of strings. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted.
        TomedaUnexpectedTypeError
            If the provided value is not of the expected type.
        """
        self._query_fields = self._convert_to_list_str(
            value, self._query_fields
        )

    @property
    def api_token(self) -> str | None:
        """API token for authentication at the data repository server."""
        return self._api_token

    @api_token.setter
    def api_token(self, value: str | None) -> None:
        """
        Sets the API token.

        Parameters
        ----------
        value : str or None
            The new value of the API token. If None is passed, the
            current value continues to be used.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string or None.
        """
        self._api_token = self._convert_to_string(value, self._api_token)

    def validate_interdependent_attributes(self) -> None:
        """
        Validates the interdependencies between different parameters.

        Raises
        ------
        TomedaValueError
            If the interdependencies are not met.
        """
        # DOFF_00
        if self.create_schema_documentation and (
            self.schema_module is None
            or self.schema_class is None
            or self.output is None
        ):
            raise TomedaValueError(
                f"schema_module: {self.schema_module}, schema_class: "
                f"{self.schema_class}, output: {self.output}",
                "For creating schema documentation, schema_module, "
                "schema_class, and output must be set.",
            )

        # DOFF_01
        if self.create_gatherer and (
            self.output is None
            or self.schema_module is None
            or self.schema_class is None
        ):
            raise TomedaValueError(
                f"schema_module: {self.schema_module}, schema_class: "
                f"{self.schema_class}, output: {self.output}",
                "For creating a gatherer file, gatherer_file, "
                "schema_module, and schema_class must be set.",
            )

        # DOFF_02
        if self.create_dataset_table and (
            self.schema_module is None
            or self.schema_class is None
            or self.output is None
        ):
            raise TomedaValueError(
                f"schema_module: {self.schema_module}, schema_class: "
                f"{self.schema_class}, output: {self.output}",
                "For creating a dataset table, schema_module, "
                "schema_class, and output must be set.",
            )

        # DOFF_03
        if self.extract_repository_metadata and (
            self.tsv_dir is None or self.output is None
        ):
            raise TomedaValueError(
                f"tsv_dir: {self.tsv_dir}, output: {self.output}",
                "For extracting repository metadata, tsv_dir and output "
                "must be set.",
            )

        # DOFF_04
        if self.derive_repository_keys and (
            self.matched_entries is None or self.schema_info_table is None
        ):
            raise TomedaValueError(
                f"matched_entries: {self.matched_entries}, "
                f"schema_info_table: {self.schema_info_table}",
                "For deriving repository keys, matched_entries and "
                "schema_info_table must be set.",
            )

        # DOFF_05
        if self.derive_repository_tsv_from_mapping and (
            self.new_keys is None or self.schema_info_table is None
        ):
            raise TomedaValueError(
                f"new_keys: {self.new_keys}, "
                f"schema_info_table: {self.schema_info_table}",
                "For deriving repository TSV from mapping, new_keys and "
                "schema_info_table must be set.",
            )

        # USER_01
        if self.read and (  # pylint: disable=too-many-boolean-expressions
            self.gatherer_file is None
            or self.collector_root is None
            or self.schema_module is None
            or self.schema_class is None
            or self.output is None
        ):
            raise TomedaValueError(
                f"gatherer_file: {self.gatherer_file}, "
                f"collector_root: {self.collector_root}, "
                f"schema_module: {self.schema_module}, "
                f"schema_class: {self.schema_class}, "
                f"output: {self.output}",
                "For reading a gatherer file, gatherer_file, collector_root, "
                "schema_module, schema_class, and output must be set.",
            )

        # USER_02
        if self.create_repository_compatible_metadata and (
            self.mapping_table is None
            or self.tsv_dir is None
            or self.dataset_metadata is None
            or self.output is None
        ):
            raise TomedaValueError(
                f"mapping_table: {self.mapping_table}, "
                f"tsv_dir: {self.tsv_dir}, "
                f"dataset_metadata: {self.dataset_metadata}, "
                f"output: {self.output}",
                "For creating repository compatible metadata, "
                "mapping_table, tsv_dir, dataset_metadata, and output "
                "must be set.",
            )

        # USER_03
        if self.upload and (
            self.server_url is None
            or self.target_collection is None
            or self.api_token is None
        ):
            raise TomedaValueError(
                f"server_url: {self.server_url}, "
                f"target_collection: {self.target_collection}, "
                f"api_token: {self.api_token}",
                "For uploading, server_url, target_collection, "
                "and api_token must be set. (Optional: query_fields)",
            )

    @staticmethod
    def _convert_to_bool(
        value: str | bool | None, current_value: bool | None
    ) -> bool:
        """
        Converts a given value to a boolean.

        Parameters
        ----------
        value : bool, str or None
            The value to be converted to boolean. Accepts boolean, string
            representations of boolean, or None.
        current_value : bool | None
            The current value of the boolean attribute, returned if the
            input is None.

        Returns
        -------
        bool
            The boolean value derived from the input, or the current value
            if the input is None.

        Raises
        ------
        TomedaValueError
            If the provided value cannot be converted to a boolean.
        TomedaUnexpectedTypeError
            If the provided value is not a boolean, string, or None.
        """
        if isinstance(value, bool):
            return value
        if value is None:
            return current_value or False
        if isinstance(value, str):
            if value.lower().strip() in ["true", "yes", "on", "1"]:
                return True
            if value.lower().strip() in ["false", "no", "off", "0", ""]:
                return False
            logger.error(
                "Invalid string value: '%s'. Must be a boolean or "
                "a string representing a boolean ('true', 'false', 'yes', "
                "'no', '1', '0').",
                value,
            )
            raise TomedaValueError(
                value, f"Cannot convert string to bool: '{value}'"
            )
        logger.error("Unexpected type for bool conversion: %s", type(value))
        raise TomedaUnexpectedTypeError(
            str(value), f"Expected bool or str, got {type(value)}"
        )

    @staticmethod
    def _convert_to_string(
        value: str | None, current_value: str | None
    ) -> str | None:
        """
        Converts or validates a given value to a string, or returns the
        current value if the input is None.

        Parameters
        ----------
        value : str | None
            The value to be validated or converted to a string.
        current_value : str | None
            The current value of the string attribute, returned if the
            input is None.

        Returns
        -------
        str | None
            The validated string, or the current value if the input is None.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string or None.
        """
        if value is None:
            return current_value
        if isinstance(value, str):
            return value.strip()
        logger.error("Unexpected type for string conversion: %s", type(value))
        raise TomedaUnexpectedTypeError(
            str(value), f"Expected str, got {type(value)}"
        )

    @staticmethod
    def _convert_to_path(
        value: str | Path | None, current_value: Path | None
    ) -> Path | None:
        """
        Converts a given value to a Path object, or returns the current
        value if the input is None.

        Parameters
        ----------
        value : str | Path | None
            The value to be converted to a Path object. Can be a string
            representing a file path, a Path object,
            or None.
        current_value : Path | None
            The current value of the Path attribute, returned if the input
            is None.

        Returns
        -------
        Path
            The Path object derived from the input, or the current value
            if the input is None.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not a string, Path object, or None.
        TomedaNotAPathError
            If the provided string or Path does not represent a valid file path.
        """
        if value is None:
            return current_value
        if isinstance(value, Path):
            return value
        if isinstance(value, str):
            if value.strip():
                return Path(value.strip())
            logger.error("Empty string for path conversion.")
            raise TomedaNotAPathError(value, "Empty string")
        logger.error("Unexpected type for path conversion: %s", type(value))
        raise TomedaUnexpectedTypeError(
            str(value), f"Expected str or Path, got {type(value)}"
        )

    @staticmethod
    def _convert_to_list_path(
        value: str | list[str | Path | None] | Path | None,
        current_value: list[Path] | None,
    ) -> list[Path]:
        """
        Converts or validates a given string or list to a list of path
        objects. Strings are split at commas.

        Parameters
        ----------
        value : str | list[str | Path | None] | Path | None
            The string to be split into a list of paths, a list of paths,
            a list of strings, a Path object, or None.
        current_value : list[Path] | None
            The current value of the list attribute, returned if the
            input is None.

        Returns
        -------
        list[Path]
            The list of paths.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not of the expected type.
        """
        if value is None:
            return current_value or []
        if isinstance(value, Path):
            return [value]
        if isinstance(value, list):
            value_list = []
            for item in value:
                if isinstance(item, Path):
                    value_list.append(item)
                    continue
                if isinstance(item, str):
                    if item.strip():
                        value_list.append(Path(item.strip()))
                    continue
                if item is None:
                    continue
                logger.error(
                    "Unexpected type for list conversion: %s", type(item)
                )
                raise TomedaUnexpectedTypeError(
                    str(item), f"Expected str, Path, or None, got {type(item)}"
                )
            return value_list
        if isinstance(value, str):
            value_list = []
            for item in value.split(","):
                if item.strip():
                    value_list.append(Path(item.strip()))
            return value_list
        logger.error("Unexpected type for list conversion: %s", type(value))
        raise TomedaUnexpectedTypeError(
            str(value),
            f"Expected list or single element, got {type(value)}",
        )

    @staticmethod
    def _convert_to_list_str(
        value: str | list[str | None] | None,
        current_value: list[str] | None,
    ) -> list[str]:
        """
        Converts or validates a given string or list to a list of string
        values.

        Parameters
        ----------
        value : str | list[str | None] | None,
            The string to be split into a list of strings, a list of strings,
            or None.
        current_value
            The current value of the list attribute, returned if the
            input is None.

        Returns
        -------
        list[str]
            The list of string values.

        Raises
        ------
        TomedaUnexpectedTypeError
            If the provided value is not of the expected type.
        """
        if value is None:
            return current_value or []
        if isinstance(value, list):
            value_list = []
            for item in value:
                if isinstance(item, str):
                    if item.strip():
                        value_list.append(item.strip())
                    continue
                if item is None:
                    continue
                logger.error(
                    "Unexpected type for list conversion: %s", type(item)
                )
                raise TomedaUnexpectedTypeError(
                    str(item), f"Expected str, Path, or None, got {type(item)}"
                )
            return value_list
        if isinstance(value, str):
            value_list = []
            for item in value.split(","):
                if item.strip():
                    value_list.append(item.strip())
            return value_list
        logger.error("Unexpected type for list conversion: %s", type(value))
        raise TomedaUnexpectedTypeError(
            str(value),
            f"Expected list or single element, got {type(value)}",
        )


def get_parameters() -> TomedaParameter:
    """
    Returns the parameters for the Tomeda application.

    The parameters are read from the environment variables and the command
    line arguments. The values are prioritized in the following order:
    1. Command line arguments
    2. ENV file
    3. OS environment variables
    4. Default values, if available

    Returns
    -------
    TomedaParameter
        The parameters for the Tomeda application.
    """
    logger.info("Loading parameters...")
    params = TomedaParameter()
    os_env = _load_os_env_variables()
    env_file = _load_env_file()
    cli_args = _get_cli_args().__dict__

    params = _update_parameters(params, os_env)
    params = _update_parameters(params, env_file)
    params = _update_parameters(params, cli_args)
    logger.debug("Parameters: %s", params)
    params.validate_interdependent_attributes()
    return params


def _get_public_attributes(class_or_instance: object) -> list[str]:
    """
    Returns the public attributes and properties of a class or instance.

    Parameters
    ----------
    class_or_instance : object
        A class or instance of a class.

    Returns
    -------
    list[str]
        A list of public attributes and properties.
    """
    attributes = []

    for attr in dir(class_or_instance):
        if attr.startswith("_"):
            continue
        # check if it is a method
        if callable(getattr(class_or_instance, attr)):
            continue
        attributes.append(attr)
    return attributes


def _normalize_env_variable_names(
    env_variables: dict[str, str | None] | dict[str, str]
) -> dict[str, str | None]:
    """
    Normalizes the names of the environment variables.

    Parameters
    ----------
    env_variables : dict[str, str | None]
        A dictionary with the environment variables.

    Returns
    -------
    dict[str, str | None]
        A dictionary with the normalized environment variables.
    """
    normalized_env_variables = {}
    for key, value in env_variables.items():
        if key.startswith("TOMEDA_"):
            # Remove the TOMEDA_ prefix with efficient string slicing instead
            # of using the replace() method.
            normalized_key = key[7:].lower()
            normalized_env_variables[normalized_key] = value
    return normalized_env_variables


def _load_env_file() -> dict[str, str | None]:
    """
    Loads the environment variables from the .env file.

    Returns
    -------
    dict[str, str | None]
        A dictionary with the environment variables.
    """
    env_file = Path(".env")
    logger.debug("Loading environment variables from %s", env_file.absolute())
    if env_file.exists():
        try:
            env_vars = dotenv_values(str(env_file))
            logger.trace("Environment variables: %s", env_vars)
            return _normalize_env_variable_names(env_vars)
        except OSError as e:
            logger.warning("Error while reading .env file: %s", e)
        except ValueError as e:
            logger.warning("Error while parsing .env file: %s", e)
        except Exception as e:  # pylint: disable=broad-except
            logger.error("Unexpected error while reading .env file: %s", e)
        return {}
    logger.debug("No .env file found %s. Skipping.", env_file.absolute())
    return {}


def _load_os_env_variables() -> dict[str, str | None]:
    """
    Loads the Tomeda environment variables from the OS environment in
    normalized form.

    Returns
    -------
    dict[str, str | None]
        A dictionary with the environment variables.
    """
    logger.debug("Loading environment variables from OS environment...")
    env_vars = dict(os.environ)
    logger.trace("OS environment variables: %s", env_vars)
    return _normalize_env_variable_names(env_vars)


def _get_cli_args() -> argparse.Namespace:
    """
    Parses the command line arguments.

    Returns
    -------
    argparse.Namespace
        The command line arguments.
    """
    logger.debug("Parsing command line arguments...")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-g",
        "--gatherer",
        dest="gatherer_file",
        type=lambda p: Path(p).absolute(),
        help="Path to the gatherer file.",
    )
    parser.add_argument(
        "--schema-module",
        # dest="schema_module",
        type=str,
        help="The python module with the schema to import from.",
    )
    parser.add_argument(
        "--schema-class",
        # dest="schema_class",
        type=str,
        help="The python class with the schema to import from.",
    )
    parser.add_argument(
        "-r",
        "--read",
        action=argparse.BooleanOptionalAction,
        help="Reads a filled-in gatherer file.",
    )
    parser.add_argument(
        "--collector-root",
        dest="collector_root",
        type=lambda p: Path(p).absolute(),
        help="The root dir (or file) of the tomeda collector process. "
        "It is specified in the Collector File as 'TMD_COLL_ROOT'",
        action="store",
    )

    output_group = parser.add_argument_group("Output Options")
    output_group.add_argument(
        "-o",
        "--output",
        dest="output",
        type=lambda p: Path(p).absolute(),
        help="Path to output the metadata as JSON.",
        action="store",
    )
    output_group.add_argument(
        "-f",
        "--overwrite",
        dest="force_overwrite",
        help="Overwrites the existing configuration file if it exists.",
        action=argparse.BooleanOptionalAction,
    )
    output_group.add_argument(
        "--dry-run",
        help="Runs the script without making any changes.",
        action=argparse.BooleanOptionalAction,
    )

    setup_group = parser.add_argument_group("Setup Options")
    setup_group.add_argument(
        "--create-gatherer",
        action=argparse.BooleanOptionalAction,
        help="Creates an empty gatherer file.",
    )
    setup_group.add_argument(
        "--create-schema-documentation",
        action=argparse.BooleanOptionalAction,
        help="Creates a schema documentation.",
    )
    setup_group.add_argument(
        "--create-dataset-table",
        action=argparse.BooleanOptionalAction,
        help="Creates a dataset table.",
    )
    setup_group.add_argument(
        "--do-not-check-recommendations",
        dest="check_recommendations",
        help="Disables the check for recommendations.",
        action="store_false",
    )
    setup_group.add_argument(
        "--extract-repository-metadata",
        dest="extract_repository_metadata",
        help="Extracts metadata from the repository.",
        action=argparse.BooleanOptionalAction,
    )
    setup_group.add_argument(
        "--derive-repository-keys",
        dest="derive_repository_keys",
        help="Derives keys from the repository.",
        action=argparse.BooleanOptionalAction,
    )
    setup_group.add_argument(
        "--derive-repository-tsv-from-mapping",
        dest="derive_repository_tsv_from_mapping",
        help="Derives TSV files from the mapping table.",
        action=argparse.BooleanOptionalAction,
    )
    setup_group.add_argument(
        "--create-repository-compatible-metadata",
        dest="create_repository_compatible_metadata",
        help="Creates repository compatible metadata.",
        action=argparse.BooleanOptionalAction,
    )
    setup_group.add_argument(
        "--tsv-dir",
        dest="tsv_dir",
        help="Dataverse TSV directory paths.",
        nargs="+",
        action="store",
    )
    setup_group.add_argument(
        "--matched-entries",
        help="Manually matched keys",
        type=lambda p: Path(p).absolute(),
        action="store",
    )
    setup_group.add_argument(
        "--schema-info-table",
        help="Schema information table "
        "(provided e.g. by --create-dataset-table)",
        action="store",
        type=lambda p: Path(p).absolute(),
    )
    setup_group.add_argument(
        "--new-keys",
        type=lambda p: Path(p).absolute(),
        help="Matched Keys",
        nargs=1,
        action="store",
    )

    dataverse_json_group = parser.add_argument_group("Dataverse JSON")
    dataverse_json_group.add_argument(
        "--dataset-metadata",
        dest="dataset_metadata",
        type=lambda p: Path(p).absolute(),
        help="Dataset Metadata. This is an input.",
        action="store",
    )
    dataverse_json_group.add_argument(
        "--mapping-table",
        dest="mapping_table",
        type=lambda p: Path(p).absolute(),
        help="Mapping Table. This is an input.",
        action="store",
    )

    # Upload File
    upload_group = parser.add_argument_group("Upload")
    upload_group.add_argument(
        "-u",
        "--upload",
        help="Flag to upload the metadata JSON to the data repository server.",
        action=argparse.BooleanOptionalAction,
    )
    upload_group.add_argument(
        "--upload-file",
        dest="upload_file",
        type=lambda p: Path(p).absolute(),
        help="Path to the upload file.",
        action="store",
    )
    upload_group.add_argument(
        "--server-url",
        type=str,
        help="The url of the dataverse server",
    )
    upload_group.add_argument(
        "--target-collection",
        type=str,
        help="The name of the target collection in the data repository"
        "server.",
    )
    upload_group.add_argument(
        "--query-fields",
        type=str,
        nargs="+",
        help="The fields that are used to check if the dataset already exists",
    )
    upload_group.add_argument(
        "--api-token",
        type=str,
        help="The api token",
    )
    args = parser.parse_args()
    logger.trace("Command line arguments: %s", args)
    return args


def _update_parameters(
    params: TomedaParameter,
    args: dict[str, str | None],
) -> TomedaParameter:
    """
    Updates the parameters with the values from the args dictionary.

    Parameters
    ----------
    params : TomedaParameter
        The parameters to be updated.
    args : dict[str, str | None]
        The arguments to update the parameters with.

    Returns
    -------
    TomedaParameter
        The updated parameters.
    """
    params_list = _get_public_attributes(params)
    for param in params_list:
        if param in args:
            setattr(params, param, args[param])
    return params
