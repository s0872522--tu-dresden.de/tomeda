from __future__ import annotations

import re
from datetime import date, datetime
from enum import Enum
from pathlib import Path
from typing import Any, List

from pydantic import (
    BaseModel,
    ByteSize,
    EmailStr,
    Field,
)

"""
Implementation of the EngMetat v.0.2 schema.

https://www.izus.uni-stuttgart.de/fokus/engmeta/engmetaMaterials/EngMeta-Documentation-v.0.2-EN.pdf

"""


class StorageComplexType(BaseModel):
    """
    The Storage complex type is used to record information about the storage.
    Its reference can be found:
    https://www.loc.gov/standards/premis/v3/premis-3-0-final.pdf#page=97
    This is a simplified version of the original complex type.
    """

    contentLocation: str = Field(
        title="Content Location",
        description="The content location is a string that identifies the location of the "
        "content of the object. An example of a content location is the 'file path' "
        "of a file.",
    )
    storageMedium: str = Field(
        title="Storage Medium",
        description="The storage medium is a string that identifies the storage medium "
        "of the object. An example of a storage medium is the 'hard disk'",
    )


class FormatComplexType(BaseModel):
    """
    The Format complex type is used to record information about the format of
    the object. Its reference can be found:
    https://www.loc.gov/standards/premis/v3/premis-3-0-final.pdf#page=75
    This is a simplified version of the original complex type.
    """

    formatDesignation: str = Field(
        title="Format Designation",
        description="The format designation is a string that identifies the "
        "format of the object. An example of a format "
        "designation is the 'mp3' type of a file.",
    )
    formatNote: str | None = Field(
        title="Format Note",
        description="Additional information about the format",
    )


class RightsStatementComplexType(BaseModel):
    """
    The Rights Statement complex type is used to record information about the
    rights statement of the object. Its reference can be found:
    https://www.loc.gov/standards/premis/v3/premis-3-0-final.pdf#page=191
    This is a simplified version of the original complex type.
    """

    value: str


class LicenceInformationComplexType(BaseModel):
    """
    The Licence Information complex type is used to record information about
    the licence of the object. Its reference can be found:
    https://www.loc.gov/standards/premis/v3/premis-3-0-final.pdf#page=208
    This is a simplified version of the original complex type.
    """

    value: str


class DTC_ContributorType(Enum):
    """
    The Contributor Type complex type is used to record information about the
    contributor type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#7a-contributortype
    """

    ContactPerson = "ContactPerson"
    DataCollector = "DataCollector"
    DataCurator = "DataCurator"
    DataManager = "DataManager"
    Distributor = "Distributor"
    Editor = "Editor"
    HostingInformation = "HostingInformation"
    Producer = "Producer"
    ProjectLeader = "ProjectLeader"
    ProjectManager = "ProjectManager"
    ProjectMember = "ProjectMember"
    RegistrationAgency = "RegistrationAgency"
    RegistrationAuthority = "RegistrationAuthority"
    RelatedPerson = "RelatedPerson"
    Researcher = "Researcher"
    ResearchGroup = "ResearchGroup"
    RightsHolder = "RightsHolder"
    Sponsor = "Sponsor"
    Supervisor = "Supervisor"
    WorkPackageLeader = "WorkPackageLeader"
    Other = "Other"


class DTC_DateType(Enum):
    """
    The Date Type complex type is used to record information about the
    date type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#8a-datetype
    """

    Accepted = "Accepted"
    Available = "Available"
    Collected = "Collected"
    Created = "Created"
    Issued = "Issued"
    Submitted = "Submitted"
    Updated = "Updated"
    Valid = "Valid"
    Withdrawn = "Withdrawn"
    Other = "Other"


class DTC_DescriptionType(Enum):
    """
    The Description Type complex type is used to record information about the
    description type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#9a-descriptiontype
    """

    Abstract = "Abstract"
    Methods = "Methods"
    SeriesInformation = "SeriesInformation"
    TableOfContents = "TableOfContents"
    TechnicalInfo = "TechnicalInfo"
    Other = "Other"


class DTC_ResourceType(Enum):
    """
    The Resource Type complex type is used to record information about the
    resource type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-mandatory-properties#10-resourcetype
    """

    Audiovisual = "Audiovisual"
    Book = "Book"
    BookChapter = "BookChapter"
    Collection = "Collection"
    ComputationalNotebook = "ComputationalNotebook"
    ConferencePaper = "ConferencePaper"
    ConferenceProceeding = "ConferenceProceeding"
    DataPaper = "DataPaper"
    Dataset = "Dataset"
    Dissertation = "Dissertation"
    Event = "Event"
    Image = "Image"
    InteractiveResource = "InteractiveResource"
    Journal = "Journal"
    JournalArticle = "JournalArticle"
    Model = "Model"
    OutputManagementPlan = "OutputManagementPlan"
    PeerReview = "PeerReview"
    PhysicalObject = "PhysicalObject"
    Preprint = "Preprint"
    Report = "Report"
    Service = "Service"
    Software = "Software"
    Sound = "Sound"
    Text = "Text"
    Workflow = "Workflow"
    Other = "Other"


class DTC_TitleType(Enum):
    """
    The Title Type complex type is used to record information about the
    title type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#11a-titletype
    """

    AlternativeTitle = "AlternativeTitle"
    Subtitle = "Subtitle"
    TranslatedTitle = "TranslatedTitle"
    Other = "Other"


class TitleType(BaseModel):
    value: DTC_TitleType = Field(
        title="Title Type",
        description="Type of the related item title. Use this subproperty to add a"
        "subtitle, translation, or alternate title to the main title."
        "The titleType subproperty is used when more than a"
        "single title is provided. Unless otherwise indicated"
        "by titleType, a title is considered to be the main"
        "title.",
    )


class DTC_RelatedIdentifierType(Enum):
    """
    The RelatedIdentifierType complex type is used to record information about
    the related identifier type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#12a-relatedidentifiertype
    """

    ARK = "ARK"
    arXiv = "arXiv"
    bibcode = "bibcode"
    DOI = "DOI"
    EAN13 = "EAN13"
    EISSN = "EISSN"
    Handle = "Handle"
    IGSN = "IGSN"
    ISBN = "ISBN"
    ISSN = "ISSN"
    ISTC = "ISTC"
    LISSN = "LISSN"
    LSID = "LSID"
    PMID = "PMID"
    PURL = "PURL"
    UPC = "UPC"
    URL = "URL"
    URN = "URN"
    Other = "Other"


class RelatedIdentifierType(BaseModel):
    """
    The RelatedIdentifierType complex type is used to record information about
    the related identifier type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#12a-relatedidentifiertype
    """

    value: str = Field(
        title="RelatedIdentifierType",
        description="Identifiers of related resources."
        "These must be globally unique"
        "identifiers.",
        remark="Free text",
    )


class RelationType(BaseModel):
    """
    The RelationType complex type is used to record information about the
    relation type of the object. Its reference can be found:
    https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#20b-relationtype
    """

    value: str


# -----------------------
# Own Classes Begin Below
# -----------------------


class Language(Enum):
    English = "English"
    German = "German"
    French = "French"
    Spanish = "Spanish"
    Italian = "Italian"
    Russian = "Russian"
    Chinese = "Chinese"
    Japanese = "Japanese"
    Korean = "Korean"
    Arabic = "Arabic"
    Portuguese = "Portuguese"
    Other = "Other"


class ID(BaseModel):
    value: str = Field(
        title="Identifier", description="General identifier of a resource"
    )


class Title(BaseModel):
    value: str = Field(title="Title", description="Title of the resource")
    titleType: None | TitleType = Field(
        title="Title Type",
        description="Type of the title, only specify if not main title",
    )
    lang: None | Language = Field(
        title="Language",
        description="Language of the title",
    )


class Project(BaseModel):
    name: str = Field(title="Name", description="Project name. Free text.")
    level: None | int = Field(
        title="Level",
        description="Project level, can be used to specify subprojects and"
        " subsubprojects. 0 is the highest level.",
    )


class URI(BaseModel):
    value: str = Field(
        title="URI",
        description="Uniform Resource Identifier, a superset of "
        "URL (UniformResource Locator) and URN "
        "(Uniform Resource Name)",
    )


class PID_Type(Enum):
    DOI = "DOI"
    URN = "URN"
    EPIC = "EPIC"
    HANDLE = "HANDLE"
    ORCID = "ORCID"
    GND = "GND"
    OTHER = "OTHER"


class PID(BaseModel):
    """
    Persistent Identifier.
    Identifier of a resource that is persistent and unique.
    """

    id: URI = Field(title="ID", description="Identifier")
    type: PID_Type = Field(
        title="Type",
        description="Type of the identifier (Controlled Vocabulary)",
    )
    scheme: None | URI = Field(
        title="Scheme", description="Scheme of the identifier"
    )


class Date(BaseModel):
    """
    Date of the resource.
    """

    value: date = Field(title="Date", description="Date of the resource")
    dateType: DTC_DateType = Field(
        title="Date Type",
        description="Type of the date",
        based_on="DataCite",
        additional_info="https://support.datacite.org/docs/datacite-metadata-schema-"
        "v44-recommended-and-optional-properties#8a-datetype",
    )


class ChecksumType(BaseModel):
    """
    Checksum of a resource.
    """

    value: str = Field(title="Value", description="Checksum value")
    algorithm: str = Field(title="Algorithm", description="Checksum algorithm")


class FileOrResource(BaseModel):
    """
    File or resource.
    """

    id: None | PID = Field(
        title="ID",
    )
    link: None | list[str] = Field(
        title="Location",
        description="Location/Path of the file or resource",
    )
    checksum: None | list[ChecksumType] = Field(
        title="Checksum",
    )


# ----------------------------
# Discipline Specific Metadata
# ----------------------------
class Point(BaseModel):
    """
    Point in 3D space.
    """

    posX: float = Field(title="Position X", description="Position X of point")
    posY: None | float = Field(
        title="Position Y", description="Position Y of point"
    )
    posZ: None | float = Field(
        title="Position Z", description="Position Z of point"
    )


class TemporalResolution(BaseModel):
    """
    Temporal resolution of the simulation.
    """

    numberOfTimesteps: int = Field(
        title="Time Step Number",
        description="Number of time steps",
    )
    interval: None | float = Field(
        title="Interval",
        description="Interval between time steps",
    )
    unit: None | str = Field(
        title="Unit",
        description="Unit ofInterval",
    )
    timeStep: None | list[float] = Field(
        title="Set of Time Steps",
        description="Specific Time Step(s), use if applicable",
    )


class SpatialResolution(BaseModel):
    """
    Spatial resolution of the simulation. Can be defined by
    - reference to the grid file
    - specification by the number of cells and/or blocks
    - number of grid points in x, y, z direction and/or the interval in x, y, z direction
    - scaling formula
    - a set of points
    """

    file: None | FileOrResource = Field(
        title="GridFile", description="Link/Path to the grid file"
    )
    countCells: None | int = Field(
        title="CellCount", description="Number of cells"
    )
    countBlocks: None | int = Field(
        title="BlockCount", description="Number of blocks"
    )
    countX: None | int = Field(
        title="X Count", description="Number of cells/points in x direction"
    )
    countY: None | int = Field(
        title="Y Count",
        description="Number of cells/points in y direction",
    )
    countZ: None | int = Field(
        title="Z Count", description="Number of cells/points in z direction"
    )
    intervalX: None | float = Field(
        title="X Interval", description="Distance between points X direction"
    )
    intervalY: None | float = Field(
        title="Y Interval", description="Distance between points Y direction"
    )
    intervalZ: None | float = Field(
        title="Z Interval", description="Distance between points Z direction"
    )
    unit: None | str = Field(
        title="DistanceUnit", description="Unit of the spatial resolution"
    )
    scalingFormula: None | str = Field(
        title="scalingFormula", description="Scaling formula"
    )
    point: None | list[Point] = Field(
        title="Set of Points", description="List of points that define the grid"
    )


class Phase(BaseModel):
    """
    Phase of the simulation.
    """

    name: str = Field(title="Name", description="Name of the phase")
    components: list[str] = Field(
        title="Components", description="Components of the phase"
    )


class Variable(BaseModel):
    """
    Variable of the simulation.
    """

    name: str = Field(title="Variable Name")
    symbol: None | str = Field(title="Formula symbol")
    value: None | str = Field(
        title="Value",
        description="Variable value (for controlled variables or parameters)",
    )
    unit: None | str = Field(title="Unit")
    uncertainty: None | float = Field(title="Uncertainty")

    _recommended: list[str] = ["unit", "uncertainty"]


class ForceField(BaseModel):
    """
    Force field of the simulation.
    """

    name: str = Field(
        title="ForceFieldName", description="Name of the force field"
    )
    parameter: None | Variable = Field(
        title="Parameter", description="Parameter of the force field"
    )


class Component(BaseModel):
    """
    Component of the simulation.
    """

    name: str = Field(title="Name", description="Name of the component")
    id: None | ID = Field(title="ID", description="ID of the component")
    smilesCode: None | str = Field(
        title="SMILES Code",
        description="SMILES (Simplified Molecular Input Line Entry Specification) "
        "code of the component",
    )
    IUPAC: None | str = Field(
        title="IUPAC Name",
        description="IUPAC (International Union of Pure and Applied Chemistry) "
        "name of the component",
    )
    quantity: None | str = Field(
        title="Quantity", description="Quantity of the component"
    )
    unit: None | str = Field(title="Unit", description="Unit of the component")
    forceField: None | ForceField = Field(
        title="Force Field",
        description="Force Field of the component",
    )


class BoundaryCondition(BaseModel):
    """
    Boundary condition of the simulation.
    """

    shape: None | str = Field(
        title="Type", description="Tupe of the boundary condition"
    )
    size: None | float = Field(
        title="Size", description="Size of the boundary condition"
    )
    position: None | str = Field(
        title="position", description="Position of the boundary condition"
    )
    component: None | Component = Field(
        title="Component",
        description="(Fix) component of the boundary condition",
    )
    parameter: None | Variable = Field(
        title="Parameter",
        description="(Variable) parameter of the boundary condition",
    )


class Description(BaseModel):
    """
    Description of the simulation.
    """

    content: str = Field(title="Description")
    descriptionType: None | DTC_DescriptionType = Field(
        title="Description Type", description="Type of the description"
    )
    lang: None | Language = Field(title="Language of the Description")


class Method(BaseModel):
    """
    Method of the simulation.
    """

    name: str = Field(title="Method Name", description="Name of the Method")
    description: None | Description = Field(
        title="Description", description="Description of the Method"
    )
    parameters: None | list[Variable] = Field(
        title="Parameters", description="Parameters of the Method"
    )


class PersonOrOrganization(BaseModel):
    """
    Datatype for a Person or Organization.
    """

    name: str = Field(
        title="Name",
        description="Name of the person or organization",
    )
    givenName: None | str = Field(
        title="First Name",
        description="The first name of a Person",
    )
    familyName: None | str = Field(
        title="Surname",
        description="The family name of the person",
    )
    address: None | str = Field(
        title="Address", description="The address of the person of organization"
    )
    affiliation: None | list[PersonOrOrganization] = Field(
        title="Affiliation", description="The affiliation of the person"
    )
    email: None | list[EmailStr] = Field(
        title="Email", description="The email of the person"
    )
    id: None | list[PID] = Field(
        title="ID",
        description="Give ID if applicable",
    )
    role: None | DTC_ContributorType = Field(
        title="Role",
        description="Datacite Contributor Type",
    )


class Publication(BaseModel):
    """
    Publication of the simulation.
    """

    id: None | list[PID] = Field(
        title="ID", description="ID(s) of the publication"
    )
    title: None | Title = Field(
        title="Title", description="Title of the publication"
    )
    author: None | list[PersonOrOrganization] = Field(
        title="Author(s)", description="Author(s) of the publication"
    )
    year: None | str = Field(
        title="Year",
    )
    citation: None | str = Field(
        title="Citation", description="Citation of the publication"
    )


class Software(BaseModel):
    """
    Software used for the simulation.
    """

    name: str = Field(title="Name", description="Name of the software")
    contributor: None | list[PersonOrOrganization] = Field(
        title="Author(s)", description="Author(s) of the software"
    )
    softwareVersion: None | str = Field(
        title="Version",
        description="Version of the software. Prefer 'Major.Minor.Patch'",
    )
    programmingLanguage: None | list[str] = Field(
        title="Programming language(s)",
        description="Programming language(s) used to create the software",
    )
    operatingSystem: None | list[str] = Field(
        title="Operating system(s)",
        description="Operating system(s) on which the software can be executed",
    )
    url: None | URI = Field(
        title="URL", description="Link to the Software Website"
    )
    softwareSourceCode: None | FileOrResource = Field(
        title="Software Source Code",
        description="Link to SourceCode",
    )
    softwareApplication: None | FileOrResource = Field(
        title="Software Application",
        description="Link to executable program",
    )
    codeRepository: None | str = Field(
        title="Repository", description="Link to the code repository"
    )
    licence: None | list[LicenceInformationComplexType] = Field(title="Licence")
    citation: None | str = Field(
        title="Quote", description="Quote of the Software"
    )
    referencePublication: None | Publication = Field(
        title="Reference Publication",
        description="Reference Publication of the used Software",
    )


class Subject(BaseModel):
    """
    Subject of the simulation.
    """

    value: str = Field(title="Subject", description="Subject of the simulation")
    subjectScheme: None | str = Field(
        title="Subject Scheme", description="Subject Scheme of the simulation"
    )
    schemeURI: None | str = Field(
        title="Scheme URI", description="Scheme URI of the simulation"
    )
    valueURI: None | str = Field(
        title="Value URI", description="Value URI of the simulation"
    )
    lang: Language = Field(
        title="Language", description="Language of the simulation"
    )


class CompilerVars(BaseModel):
    """
    Compiler variables.
    """

    output_name: str = Field(
        title="Name of the executable", description="Name of the executable"
    )
    input_object_files: list[str] = Field(
        title="Object files",
        description="Object files used for the compilation",
    )
    libraries: None | list[str] = Field(
        title="Libraries", description="Libraries used for the compilation"
    )
    flags: None | list[str] = Field(
        title="Flags", description="Flags used for the compilation"
    )


class Compiler(BaseModel):
    """
    Compiler used for the simulation.
    """

    name: str = Field(title="CompilerName", description="Name of the compiler")
    flags: None | str = Field(
        title="CompilerFlags",
        description="Used compiler flags. Pass empty string ' ' if no flags were used.",
    )


class Environment(BaseModel):
    """
    Environment of the simulation.
    """

    name: None | str = Field(
        title="SystemName",
        description="Name of the system",
    )
    compiler: None | list[Compiler] = Field(
        title="Used Compiler",
    )
    nodes: None | int = Field(
        title="Number of Nodes",
    )
    ppn: None | int = Field(
        title="Processors per Node",
    )
    cpu: None | list[str] = Field(
        title="CPUModel",
        description="CPU Model used for the simulation",
    )


class Context(BaseModel):
    """
    Context of the simulation.
    """

    referencePublication: None | Publication = Field(
        title="Publication", description="Publication this work is based on"
    )
    relatedResource: None | FileOrResource = Field(
        title="Related Resource", description="Related Resource"
    )
    relatedIdentifierType: None | RelatedIdentifierType = Field(
        title="Related Identifier Type", description="Related Identifier Type"
    )
    relationType: None | RelationType = Field(
        title="Relation Type", description="Relation Type"
    )
    relatedMetadataScheme: None | str = Field(
        title="Related Metadata Scheme", description="Related Metadata Scheme"
    )
    schemeURI: None | str = Field(title="Scheme URI", description="Scheme URI")
    schemeType: None | str = Field(
        title="Scheme Type", description="Scheme Type"
    )


class ProcessingStepType(Enum):
    Generation = "Generation"
    Analysis = "Analysis"
    Postprocessing = "Postprocessing"
    Other = "Other"


class DatasetMode(Enum):
    Simulation = "Simulation"
    Experiment = "Experiment"
    Analysis = "Analysis"
    Prediction = "Prediction"


# class AwardNumber(BaseModel):
#     value: str = Field(title="Award Number", description="Award Number")


class DTC_FunderIdentifierType(Enum):
    CrossrefFunderID = "CrossrefFunderID"
    GRID = "GRID"
    ISNI = "ISNI"
    ROR = "ROR"
    Other = "Other"


class FundingReference(BaseModel):
    funderName: str = Field(
        title="Funder Name",
        description="Name of the funder or institution funding the project",
    )
    funderIdentifier: None | str = Field(
        title="Funder Identifier",
        description="Identifier of the funder or institution funding the project",
    )
    funderIdentifierType: None | DTC_FunderIdentifierType = Field(
        title="Funder Identifier Type",
        description="Identifier type of the funder or institution funding the project",
    )
    awardNumber: None | str = Field(
        title="Award|Grant Number",
        description="Award|Grant number of the funding project",
    )
    awardType: None | str = Field(
        title="Award|Grant Type",
        description="Award|Grant type of the funding project",
    )


class Keyword(BaseModel):
    value: str = Field(title="Keyword", description="Keyword")
    vocabulary: None | str = Field(
        title="Vocabulary", description="Vocabulary of the Keyword used"
    )
    vocabularyURL: None | str = Field(
        title="Vocabulary URL", description="URL of the Vocabulary used"
    )


# --------- END DATATYPES ------------------------------------------------------

# ------------- Domain Specific Metadata ---------------------------------------


class System(BaseModel):
    """
    Observed System of.
    """

    systemID: None | ID = Field(title="ID", description="ID of the system")
    phase: None | list[Phase] = Field(
        title="Phase", description="Phase of the system"
    )
    component: None | list[Component] = Field(
        title="Component(s)", description="(Fixed) Component(s) of the system"
    )
    parameter: None | list[Variable] = Field(
        title="Parameter(s)", description="(Varied) Parameter(s) of the system"
    )
    grid: None | SpatialResolution = Field(
        title="Spatial Resolution",
        description="Spatial Resolution of the simulation",
    )
    temporalResolution: None | TemporalResolution = Field(
        title="Temporal Resolution",
        description="Temporal Resolution of the simulation",
    )
    boundaryCondition: None | list[BoundaryCondition] = Field(
        title="Boundary Condition",
        description="Boundary Condition of the system",
    )


# ----------------- Process Metadata -------------------------------------------


class ProcessingStep(BaseModel):
    """
    A Datatype that describes the processing steps that were taken to create the
    Dataset. It is part of the provenance of the Dataset. The provenance is a
    record that describes the people, institutions, entities and activities
    involved in producing, influencing or delivering a part of the Dataset.
    """

    type: ProcessingStepType = Field(
        title="Type of processing",
    )
    actor: None | list[PersonOrOrganization] = Field(
        title="Actor",
    )
    date: None | Date = Field(
        title="Date",
    )
    method: None | list[Method] = Field(
        title="Method",
    )
    errorMethod: None | list[Method] = Field(title="Error Method")
    input: None | list[FileOrResource] = Field(title="Input file(s)")
    tool: None | list[Software] = Field(title="Software used")
    executionCommand: None | list[str] = Field(title="Command executed")
    environment: None | Environment = Field(title="Computing Environment")
    system: None | System = Field(title="Reference to observed System")
    output: None | list[FileOrResource] = Field(title="Output file(s)")

    _recommended: list[str] = ["method"]


# ------------------ Dataset Base Class ------------------
class Dataset(BaseModel):
    """
    A PyDantic model representing a Dataset.
    """

    # ------------------ Begin Descriptive Metadata -------------------------- #
    contact: list[PersonOrOrganization] = Field(
        title="Contact Person", description="Contact Person of the Dataset"
    )
    creator: list[PersonOrOrganization] = Field(title="Producer/Author")
    contributor: None | list[PersonOrOrganization] = Field(title="Contributor")
    project: None | list[Project] = Field(
        title="Project", description="Project with Attribute 'Level'"
    )
    fundingReference: None | list[FundingReference] = Field(
        title="Funding Information",
        description="Funding Information of the project",
    )
    worked: None | bool = Field(
        title="Indication of Success",
        description="Indicates if the dataset was successful",
    )
    workedNote: None | str = Field(title="Explanation of (missing) Success")
    title: list[Title] = Field(
        title="Title", description="Title of the Dataset"
    )
    description: None | list[Description] = Field(
        title="Description (With attribute Description Type)"
    )
    resourceType: None | DTC_ResourceType = Field(title="Data Type")
    keywords: None | list[Keyword] = Field(title="Keywords")
    subjects: None | Subject = Field(title="Subject")
    dates: None | list[Date] = Field(
        title="Date of Dataset",
        description="Date of Dataset. The type of the Date is specified in the "
        "attribute Date Type of the Date.",
    )
    version: None | str = Field(
        title="Version",
        description="Version of the Dataset. Prefer 'major.minor.patch'",
    )
    mode: None | DatasetMode = Field(
        title="Data Generation Method",
        description="Data Generation Method of the Dataset. Is it a simulation,"
        "experiment, analysis or prediction?",
    )
    measuredVariable: None | list[Variable] = Field(
        title="Measured Variables",
        description="Measured Variables of the Dataset",
    )
    controlledVariable: None | list[Variable] = Field(
        title="Ensemble/Controlled Variables",
        description="Ensemble/Controlled Variables of the Dataset",
    )
    # ------------------ End Descriptive Metadata ---------------------------- #
    # ------------------ Begin Technical Metadata ---------------------------- #
    size: ByteSize = Field(title="Size of Dataset (Byte)")
    storage: StorageComplexType = Field(title="Filename / File Location")
    format: None | FormatComplexType = Field(
        title="File Type", description="Type of Datset. Prefer MIME type."
    )
    identifier: None | PID = Field(
        title="(P)ID", description="(Persistent) Identifier of the Dataset"
    )
    checksum: None | ChecksumType = Field(
        title="Checksum for data integrity",
        description="Checksum for data integrity, ed. MD5",
    )
    rightsStatement: None | RightsStatementComplexType = Field(
        title="Legal information",
        description="Legal information about the Dataset (copyright, access "
        "rights, license)",
    )
    context: None | Context = Field(
        title="Context",
        description="Context of the Dataset. Links to related resources.",
    )
    # ------------------ End Technical Metadata ------------------------------ #
    # ------------------ Begin Process Metadata ------------------------------ #
    provenance: None | list[ProcessingStep] = Field(
        title="Process Metadata",
        description="This is the provenance of the Dataset. It describes the processing steps "
        "that were taken to create the Dataset. This includes steps like "
        "preprocessing, simulation, postprocessing, analysis, etc.",
    )
    # ------------------ End Process Metadata -------------------------------- #

    _recommended: list[str] = [
        "keywords",
        "subjects",
        "dates",
        "mode",
        "measuredVariable",
        "controlledVariable",
    ]

    class Config:
        @staticmethod
        # def schema_extra(schema: dict[str, Any], model: type['PersonOrOrganization']) -> None:
        def schema_extra(
            schema: dict[str, Any], model: type["Dataset"]
        ) -> None:
            for prop in schema.get("properties", {}).values():
                prop.pop("additional_info", None)
                prop.pop("remark", None)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Inspects the metadata schema. Allows for pdf creation and json output."
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--create-pdf",
        action="store",
        nargs=1,
        metavar="<outputfile>.pdf",
        type=lambda p: Path(p).absolute(),
        help="Creates an PDF of the metadata schema",
    )
    group.add_argument(
        "--create-json",
        action="store",
        nargs=1,
        metavar="<outputfile>.json",
        type=lambda p: Path(p).absolute(),
        help="Outputs the JSON schema to a file.",
    )
    group.add_argument(
        "--view-json",
        action="store_true",
        help='Prints the JSON schema to stdout. View with "jq" or redirect to file.',
    )

    group.add_argument(
        "--created-nestedtext",
        action="store",
        nargs=1,
        metavar="<outputfile>.nt",
        type=lambda p: Path(p).absolute(),
        help="Prints the schema as nestedtext to stdout.",
    )
    group.add_argument(
        "--view-nestedtext",
        action="store_true",
        help="Prints the schema as nestedtext to stdout.",
    )

    args = parser.parse_args()

    if args.create_pdf:
        file_name = args.create_pdf[0].with_suffix(".pdf")

        # https://erdantic.drivendata.org/stable/
        import erdantic as erd

        erd.draw(Dataset, out=file_name)
        print(f'Created "{file_name}"')

    if args.create_json:
        import json

        file_name = args.create_json[0].with_suffix(".json")

        with file_name.open("w", encoding="utf-8") as f:
            json.dump(Dataset.schema(), f, indent=2)
        print(f'Created "{file_name}"')

    if args.view_json:
        print(Dataset.schema_json(indent=2))

    if args.created_nestedtext:
        import nestedtext as nt  # type: ignore[import]

        nt_string = nt.dumps(Dataset.schema())

        file_name = args.created_nestedtext[0].with_suffix(".nt")

        with file_name.open("w", encoding="utf-8") as f:
            f.write(nt_string)

    if args.view_nestedtext:
        import nestedtext as nt  # type: ignore[import]

        print(nt.dumps(Dataset.schema()))

    # PersonOrOrganization.update_forward_refs()
