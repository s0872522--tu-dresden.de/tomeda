Welcome to ToMeDa's documentation!
==================================

ToMeDa is a tool for automatic gathering, organizing and uploading to repository of metadata.


Documentation Contents
----------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
