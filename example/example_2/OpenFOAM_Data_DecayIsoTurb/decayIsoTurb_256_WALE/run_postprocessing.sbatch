#!/bin/bash

#SBATCH -n 1
#SBATCH -c 1
#SBATCH --time=04:00:00
#SBATCH --mem-per-cpu=30G
#SBATCH -p romeo
#SBATCH --hint=nomultithread,memory_bound

set -e
 
module purge
module load OpenFOAM/v2206-foss-2022a

source $FOAM_BASH

echo reconstructPar 
time srun reconstructPar > log.reconstructPar 2>&1

# Delete OpenFOAM decomposed Data
rm -r processor*

echo foamToVTK
time srun foamToVTK -name VTK_decayIsoTurb_256_WALE -no-finite-area -no-lagrangian -no-point-data -no-boundary -noZero > log.foamToVTK 2>&1

# Delete OpenFOAM reconstructed data
find . -maxdepth 1 -type d -regex '\./[0-9]+\.*[0-9]*' -exec rm -r {} \;

pushd VTK*

# Create VTU series file
mv decayIsoTurb_256_WALE.vtm.series decayIsoTurb.vtu.series
sed -i 's|.vtm|.vtu|g' decayIsoTurb.vtu.series

# Remove Multiblock Files
rm *.vtm

# Restructure file hierachy
for dir in decayIsoTurb_*/ ; do
	mv ${dir}/internal.vtu ${dir%/}.vtu
	rm -r ${dir}
done 

popd

# Mark end
touch done

