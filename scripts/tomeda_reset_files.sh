#!/bin/bash

set -euo pipefail

source_file() {
    if [ -f "$1" ]; then
        source "$1"
    else
        echo "File '$1' not found."
        return 1
    fi
}

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null && pwd )"

poetry install || exit 1
source_file "${CWD:?}"/../tomeda.env || exit 1
APP_DIR="${CWD:?}"/../example/example_1/simulation_experiment_folder

rm -frd "${TOMEDA_USER_DIR:?}"/
rm -frd "${TOMEDA_ADMIN_DIR:?}"/
rm -f "${APP_DIR:?}"/metadata.json
rm -f "${APP_DIR:?}"/metadata_for_upload.json
