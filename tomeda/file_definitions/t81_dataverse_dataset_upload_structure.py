from __future__ import annotations

import dataclasses
from dataclasses import dataclass, make_dataclass
from typing import Any, Type

from tomeda.default_exception import TomedaUnexpectedTypeError


def ensure_list(value: Any) -> Any:
    return value if isinstance(value, list) else [value]


def prepend_typeName_infrontof_MetadataElement(
    metadata_element: MetadataElement,
) -> dict[str, MetadataElement]:
    """Create a compound MetadataElement from a given MetadataElement or list of MetadataElements."""
    assert isinstance(metadata_element, MetadataElement)

    return {metadata_element.type_name: metadata_element}


MetadataValue = (
    str
    | list[str]
    | dict[str, "MetadataElement"]
    | list[dict[str, "MetadataElement"]]
)


@dataclass(frozen=True, kw_only=True)
class MetadataElement:
    """Class representing a metadata element."""

    type_name: str  # depends directly on schema
    multiple: bool  # depends directly on schema
    type_class: str  # depends directly on schema
    value: MetadataValue  # depends on typeClass and multiple (indirectly on schema)

    def __post_init__(self):
        object.__setattr__(self, "value", self._set_value())

    def _set_value(self):
        """Set the value attribute based on the typeClass and multiple attributes."""

        value_setting_function = {
            ("primitive", False): self._set_primitive_single,
            ("primitive", True): self._set_primitive_multiple,
            ("compound", False): self._set_compound_single,
            ("compound", True): self._set_compound_multiple,
            (
                "controlledVocabulary",
                False,
            ): self._set_controlled_vocabulary_single,
            (
                "controlledVocabulary",
                True,
            ): self._set_controlled_vocabulary_multiple,
        }.get((self.type_class, self.multiple))

        if not value_setting_function:
            raise ValueError(
                f"Unsupported typeClass and multiple combination: {self.type_class}, {self.multiple}"
            )

        return value_setting_function(self.value)

    @staticmethod
    def _set_primitive_single(value):
        """Set the value attribute for a single primitive typeClass."""
        if isinstance(value, list):
            value = value[0]
        return value

    @staticmethod
    def _set_primitive_multiple(value):
        """Set the value attribute for a multiple primitive typeClass."""
        return ensure_list(value)

    @staticmethod
    def _set_compound_single(
        value: list[MetadataElement],
    ) -> dict[str, MetadataElement]:
        """
        Set the value attribute for a single compound typeClass.

        In this case the result_dict is a single dictionary. containing multiple
        MetadataElements.

        What this does is
        a create a dict with
        {
            "my_name": {
                "name": "my_name",
                "multiple": false|true,
                "type_class": "primitive|controlledVocabulary",
                "value": "my_value"
            },
        }

        The input is a list of MetadataElement's to be converted to a dict containing
        these MetadataElements as dict representation.

        """
        result_dict = {}
        for compound_objects in value:
            result_dict.update(
                prepend_typeName_infrontof_MetadataElement(compound_objects)
            )
        return result_dict

    @staticmethod
    def _set_compound_multiple(
        value: list[list[MetadataElement]],
    ) -> list[dict[str, MetadataElement]]:
        """
        Set the value attribute for a multiple compound typeClass.

        In this case the return type is a list with dictionaries,
        each containing 'one' subcase multiple MetadataElements.

        What this does is to create a dict with
        [
            {
                "my_name": {
                    "name": "my_name",
                    "multiple": false|true,
                    "type_class": "primitive|controlledVocabulary",
                    "value": "my_value"
                },
                "my_name2": {
                    "name": "my_name2",
                    "multiple": false|true,
                    "type_class": "primitive|controlledVocabulary",
                    "value": "my_value"
                },
            },
            {
                "my_name": {
                    "name": "my_name",
                    "multiple": false|true,
                    "type_class": "primitive|controlledVocabulary",
                    "value": "my_value"
                },
                "my_name2": {
                    "name": "my_name2",
                    "multiple": false|true,
                    "type_class": "primitive|controlledVocabulary",
                    "value": "my_value"
                },
            },
        ]

        """
        # self.value is of list[list[MetadataElement]]
        if not isinstance(value[0][0], MetadataElement):
            raise TomedaUnexpectedTypeError(
                value[0][0], "Expected type MetadataElement"
            )

        result_list = []  # this is the 'multiple' part

        for multiple_primitive_objects in value:
            child = MetadataElement._set_compound_single(
                multiple_primitive_objects
            )

            result_list.append(child)

        return result_list

    @staticmethod
    def _set_controlled_vocabulary_single(value):
        return value

    @staticmethod
    def _set_controlled_vocabulary_multiple(value):
        return ensure_list(value)


@dataclass(frozen=True, kw_only=True)
class MetadataSource:
    """Class representing a metadata source like "citation" or "engmeta"."""

    display_name: str
    fields: list[MetadataElement]


def MetadataSource_(
    metadata_sources: list[MetadataSource],
) -> Type[MetadataSource_]:
    """Create a compound MetadataSource from a list of MetadataSources."""
    fields = [
        (element.display_name, MetadataSource) for element in metadata_sources
    ]
    dc = make_dataclass("MetadataSource_", fields, frozen=True)
    return dc(*metadata_sources)


@dataclass(frozen=True, kw_only=True)
class Dataset:
    """Class representing a dataset."""

    metadata_blocks: MetadataSource_
    license: str | dict[str, str] = ""


@dataclass(frozen=True, kw_only=True)
class DatasetEnvelope:
    """Class representing a dataset envelope."""

    dataset_version: Dataset
