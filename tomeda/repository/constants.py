JSONType = (
    dict[str, "JSONType"] | list["JSONType"] | str | int | float | bool | None
)

REPOSITORY_DATA: dict[str, dict[str, dict[str, str]]] = {
    "dataverse": {
        "connector": {
            "module": "tomeda.repository.dataverse_connector",
            "class": "TomedaDataverseConnector",
        },
        "validator": {
            "module": "tomeda.repository.validator",
            "class": "TomedaDataverseValidator",
        },
    },
}

REPOSITORY_SUPPORT = frozenset(sorted(REPOSITORY_DATA.keys()))
