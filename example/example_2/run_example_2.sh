#!/bin/env bash
set -ex
# Set ToMeDa Variables
# Source other ToMeDa Variables
source ../../tomeda.env

# Location of the Pydantic Dataset Definition
export TOMEDA_SCHEMA_FILE=../../data/metadata_schema/dataset_metadata_schema.py
# Name of the base class defined in the file given above
export TOMEDA_SCHEMA_NAME=Dataset

export TOMEDA_USER_DIR=./tomeda/user
export TOMEDA_ADMIN_DIR=./tomeda/admin

export CFGNAME=./config.nt



tomeda_DOFF_00_dump_documentation
tomeda_DOFF_01_dump_template
tomeda_DOFF_02_dump_info_struct
tomeda_DOFF_03_extract_TSV_keys
tomeda_DOFF_04_derive_dataverse_key
tomeda_DOFF_05_derive_dataverse_tsv

LOOP_ROOT=./OpenFOAM_Data_DecayIsoTurb/

for dir in $LOOP_ROOT/*/; do
    echo "$dir"
    export APP_DIR=$dir
    export METADATA_JSON=$dir/metadata.json
    tomeda_USER_01_extract_metadata
    tomeda_USER_02_map_metadata_to_dataverse
done

echo "Done"