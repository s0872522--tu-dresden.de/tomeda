{
    "contact": [
        {
            "name": "Maximilian Sander",
            "affiliation": [
                {
                    "name": "CIDS-ZIH, TU Dresden"
                }
            ]
        }
    ],
    "creator": [
        {
            "name": "Maximilian Sander",
            "affiliation": [
                {
                    "name": "CIDS-ZIH, TU Dresden"
                }
            ]
        }
    ],
    "worked": true,
    "title": [
        {
            "value": "Comparison of different turbulence models at different resolutions in OpenFOAM",
            "lang": "English"
        }
    ],
    "description": [
        {
            "content": "This project compares the turbulence models 'kEqn', 'Smagorinsky' and 'WALE' with the",
            "descriptionType": "Abstract",
            "lang": "English"
        }
    ],
    "keywords": [
        {
            "value": "CFD"
        },
        {
            "value": "Turbulence Models"
        },
        {
            "value": "Grid Convergence"
        }
    ],
    "subjects": {
        "value": "CFD Turbulence Simulation",
        "lang": "English"
    },
    "dates": [
        {
            "value": "2023-04-01",
            "dateType": "Issued"
        },
        {
            "value": "2023-08-28",
            "dateType": "Created"
        }
    ],
    "version": "0.1",
    "mode": "Simulation",
    "measuredVariable": [
        {
            "name": "EnergySpectrum",
            "symbol": "E"
        }
    ],
    "controlledVariable": [
        {
            "name": "Resolution",
            "symbol": "res",
            "value": "64, 128, 256",
            "unit": "m"
        },
        {
            "name": "Turbulence Model",
            "symbol": "turb_model",
            "value": "kEqn, Smagorinsky, WALE",
            "unit": "-"
        }
    ],
    "size": 200000000,
    "storage": {
        "contentLocation": "/longterm-storage/openfoam-turbulence-model_resolution_isodecay_testcase",
        "storageMedium": "Tape"
    },
    "context": {
        "referencePublication": {
            "id": [
                {
                    "id": {
                        "value": "10.1017/S0022112071001599"
                    },
                    "type": "DOI"
                }
            ],
            "title": {
                "value": "Simple Eulerian time correlation of full-and narrow-band velocity signals in grid-generated, 'isotropic' turbulence",
                "lang": "English"
            },
            "author": [
                {
                    "name": "Genevieve Comte-Bellot"
                },
                {
                    "name": "Stanley Corrsin"
                }
            ],
            "year": "1971"
        },
        "relatedResource": {
            "link": [
                "https://www.openfoam.com/documentation/guides/latest/doc/verification-validation-turbulent-decay-homogeneous-isotropic-turbulence.html"
            ]
        }
    },
    "provenance": [
        {
            "type": "Generation",
            "actor": [
                {
                    "name": "Maximilian Sander",
                    "affiliation": [
                        {
                            "name": "CIDS-ZIH, TU Dresden"
                        }
                    ]
                }
            ],
            "tool": [
                {
                    "name": "OpenFOAM-v2206",
                    "softwareVersion": "OpenFOAM-v2206",
                    "operatingSystem": [
                        "Linux"
                    ],
                    "url": {
                        "value": "https://www.openfoam.com/"
                    },
                    "codeRepository": "https://develop.openfoam.com/Development/openfoam/-/tree/master",
                    "referencePublication": {
                        "id": [
                            {
                                "id": {
                                    "value": "10.1063/1.168744"
                                },
                                "type": "DOI"
                            }
                        ],
                        "title": {
                            "value": "A tensorial approach to computational continuum mechanics using object-oriented techniques",
                            "lang": "English"
                        },
                        "author": [
                            {
                                "name": "H. G. Weller"
                            },
                            {
                                "name": "G. Tabor"
                            },
                            {
                                "name": "H. Jasak"
                            },
                            {
                                "name": "C. Fureby"
                            }
                        ],
                        "year": "1998"
                    }
                }
            ],
            "executionCommand": [
                "./run_case.sh"
            ],
            "environment": {
                "name": "OpenFOAM/v2206-foss-2022a",
                "nodes": 1,
                "ppn": 64,
                "cpu": [
                    "AMD Epyc 7702"
                ]
            },
            "system": {
                "component": [
                    {
                        "name": "Resolution",
                        "unit": "(128 128 128);"
                    },
                    {
                        "name": "Turbulence Model",
                        "unit": "LES;"
                    }
                ],
                "grid": {
                    "countCells": 2097152,
                    "countX": 128,
                    "countY": 128,
                    "countZ": 128,
                    "intervalX": 0.56548667765,
                    "intervalY": 0.56548667765,
                    "intervalZ": 0.56548667765,
                    "unit": "m"
                },
                "temporalResolution": {
                    "numberOfTimesteps": 1000,
                    "interval": 0.001,
                    "unit": "s"
                },
                "boundaryCondition": [
                    {
                        "shape": "cyclic",
                        "position": ".*",
                        "component": {
                            "name": "p",
                            "quantity": "uniform 0",
                            "unit": "m^2/s^2"
                        }
                    },
                    {
                        "shape": "cyclic",
                        "position": ".*",
                        "component": {
                            "name": "nut",
                            "quantity": "uniform 0",
                            "unit": "m^2/s"
                        }
                    }
                ]
            }
        },
        {
            "type": "Postprocessing",
            "actor": [
                {
                    "name": "Maximilian Sander",
                    "affiliation": [
                        {
                            "name": "CIDS-ZIH, TU Dresden"
                        }
                    ]
                }
            ],
            "tool": [
                {
                    "name": "OpenFOAM-v2206",
                    "softwareVersion": "OpenFOAM-v2206",
                    "operatingSystem": [
                        "Linux"
                    ],
                    "url": {
                        "value": "https://www.openfoam.com/"
                    },
                    "codeRepository": "https://develop.openfoam.com/Development/openfoam/-/tree/master",
                    "referencePublication": {
                        "id": [
                            {
                                "id": {
                                    "value": "10.1063/1.168744"
                                },
                                "type": "DOI"
                            }
                        ],
                        "title": {
                            "value": "A tensorial approach to computational continuum mechanics using object-oriented techniques",
                            "lang": "English"
                        },
                        "author": [
                            {
                                "name": "H. G. Weller"
                            },
                            {
                                "name": "G. Tabor"
                            },
                            {
                                "name": "H. Jasak"
                            },
                            {
                                "name": "C. Fureby"
                            }
                        ],
                        "year": "1998"
                    }
                }
            ],
            "executionCommand": [
                "./run_case.sh"
            ],
            "environment": {
                "name": "OpenFOAM/v2206-foss-2022a",
                "nodes": 1,
                "ppn": 1,
                "cpu": [
                    "Intel XEON Platinum 8268"
                ]
            },
            "system": {
                "component": [
                    {
                        "name": "Resolution",
                        "unit": "(128 128 128);"
                    },
                    {
                        "name": "Turbulence Model",
                        "unit": "LES;"
                    }
                ],
                "grid": {
                    "countCells": 2097152,
                    "countX": 128,
                    "countY": 128,
                    "countZ": 128,
                    "intervalX": 0.56548667765,
                    "intervalY": 0.56548667765,
                    "intervalZ": 0.56548667765,
                    "unit": "m"
                },
                "temporalResolution": {
                    "numberOfTimesteps": 1000,
                    "interval": 0.001,
                    "unit": "s"
                },
                "boundaryCondition": [
                    {
                        "shape": "cyclic",
                        "position": ".*",
                        "component": {
                            "name": "p",
                            "quantity": "uniform 0",
                            "unit": "m^2/s^2"
                        }
                    },
                    {
                        "shape": "cyclic",
                        "position": ".*",
                        "component": {
                            "name": "nut",
                            "quantity": "uniform 0",
                            "unit": "m^2/s"
                        }
                    }
                ]
            },
            "output": [
                {
                    "link": [
                        "/OpenFOAM_Data_DecayIsoTurb/decayIsoTurb_128_WALE/postProcessing"
                    ]
                }
            ]
        }
    ]
}
