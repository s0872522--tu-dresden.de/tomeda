# Admin-Documentation

The admin has the following responsibilities:

1. Metadata schema definition
   1. Define the metadata schema for the simulation
   2. Create a pre-filled tomeda-config file for the user
   3. Create a json of a schema of the metadata schema

2. Create the conversion to dataverse
   1. Create a mapping between the metadata schema and the dataverse schema
   2. Create a tsv file for the dataverse instance
   3. Upload the tsv file to the dataverse instance

3. Prepare Dataverse for this kind of data
   1. Host Dataverse
   2. Upload the metadata schema to dataverse to be used by the user

## 1. Metadata schema definition

### Define the metadata schema for the simulation

This part has to be well-thought-out and documented.
The schema should be as general as possible to be used by multiple simulations.
It should be as specific as possible to be able to capture all the information needed for the simulation.

The structure has to be defined in `data/metadata_schema/dataset_metadata_schema.py`.
The structure is implemented in Pydantic.

### Create a pre-filled tomeda-config file for the user

This file is a NestedText file which is used by the user to fill the metadata.
The admin should provide a pre-filled file with the most commonly used values
to the user to allow the user to only fill out the differing fields.

The empty file is created by running

`ToDo`

### Create a json of a schema of the metadata schema

This file is used by the admin for multiple purposes, specified in the next section.

The file is created by running

`ToDo`

## 2. Create the conversion to dataverse

### Create a mapping between the metadata schema and the dataverse schema

This creates two files:

1. A json file which is used to map the metadata schema to the dataverse schema
2. A tsv file which is used to upload the dataverse schema to the dataverse instance
3. A json file which is used to create a pydantic schema for the dataverse

### Create a tsv file for the dataverse instance

## 3. Prepare Dataverse for this kind of data

### Host Dataverse

A dataverse instance can easily be hosted on a server. It can simply be installed via
[docker](https://github.com/IQSS/dataverse-docker).

### Upload the tsv file to the dataverse instance

With the tsv file generated in the previous step, the admin can upload the dataverse schema to the dataverse instance.

