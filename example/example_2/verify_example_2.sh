#!/bin/env bash
set -e
# Set ToMeDa Variables

LOOP_ROOT=./OpenFOAM_Data_DecayIsoTurb/

for dir in $LOOP_ROOT/*/; do
    diff $dir/metadata.json ./reference/$dir/metadata.json
    diff $dir/metadata_for_upload.json ./reference/$dir/metadata_for_upload.json
done

echo "Done"