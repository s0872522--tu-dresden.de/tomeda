"""
 This code generates comprehensible schema documentation for the user
 Input is a JSON schema file, output is a PDF file.
"""
import json
from contextlib import suppress
from pathlib import Path
from typing import Any, cast

from reportlab.lib import colors
from reportlab.lib.pagesizes import landscape, letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import (
    KeepTogether,
    Paragraph,
    SimpleDocTemplate,
    Spacer,
    Table,
    TableStyle,
)


def generate_pdf_from_schema(
    schema_path: Path | str, output_path: Path
) -> None:
    if isinstance(schema_path, Path):
        with schema_path.open("r") as file:
            schema = json.load(file)
    else:
        schema = json.loads(schema_path)

    doc_elements = transform_to_table(schema)

    # Create the PDF
    output_path.parent.mkdir(parents=True, exist_ok=True)
    doc = SimpleDocTemplate(str(output_path), pagesize=landscape(letter))  # type: ignore[no-untyped-call]
    doc.build(doc_elements)


def transform_to_table(
    data: dict[str, dict[str, object] | Any],
    definitions: dict[str, Any] | None = None,
    processed_datatypes: set[str] | None = None,
) -> list[Any]:
    if definitions is None:
        definitions = cast(Any, data["definitions"])
    if processed_datatypes is None:
        processed_datatypes = set()

    doc_elements = []
    section_header = data["title"]
    required_fields = cast(list[str], data.get("required", []))
    properties: dict[str, Any] = data.get("properties", {})

    styles = getSampleStyleSheet()  # type: ignore[no-untyped-call]
    styleN = styles["BodyText"]
    styleH = styles["Heading1"]

    def to_paragraph(text: str) -> Any:
        return Paragraph(text.replace("\n", "<br />"), styleN)

    def get_allowed_occurrences(
        element_: str, details_: dict[str, object]
    ) -> str:
        if details_.get("type") == "array":
            return "1-n" if element_ in required_fields else "0-n"
        if details_.get("allOf"):
            return "1" if element_ in required_fields else "0-1"
        return "1" if element_ in required_fields else "0-1"

    def get_data_type(details: dict[str, Any]) -> tuple[str, bool]:
        datatype = details.get("type", "complex")

        if datatype == "array":
            items = details.get("items")
            if items and isinstance(items, dict):
                ref = items.get("$ref")
                if ref:
                    return ref.split("/")[-1], True

        if datatype == "complex":
            all_of = details.get("allOf")
            if all_of and isinstance(all_of, list) and all_of:
                first_item = all_of[0]
                if isinstance(first_item, dict):
                    ref = first_item.get("$ref")
                    if ref:
                        return ref.split("/")[-1], True

        return datatype, False

    # Table headers
    table_data = [
        [
            to_paragraph("Title"),
            to_paragraph("Element Name"),
            to_paragraph("How Often"),
            to_paragraph("Obligation"),
            to_paragraph("Data Type"),
            to_paragraph("Description"),
            to_paragraph("Controlled Vocabulary"),
        ]
    ]

    nested_properties = []

    for element, details in properties.items():
        if not isinstance(details, dict):  # Check details is a dict
            continue
        title = details.get("title", "N/A")
        description = details.get("description", "N/A")
        how_often = get_allowed_occurrences(element, details)
        obligation = "M" if element in required_fields else "O"
        datatype, complicated = get_data_type(details)

        cv = ""  # Controlled Vocabulary
        if dt := definitions.get(datatype):
            if cv_ := dt.get("enum"):
                cv = ", ".join(cv_)

        table_data.append(
            [
                to_paragraph(title),
                to_paragraph(element),
                to_paragraph(how_often),
                to_paragraph(obligation),
                to_paragraph(datatype),
                to_paragraph(description),
                to_paragraph(cv),  # Controlled Vocabulary
            ]
        )

        if complicated and datatype not in processed_datatypes:
            nested_def = definitions.get(datatype)
            if nested_def:
                nested_properties.append(nested_def)
                processed_datatypes.add(datatype)

    if len(table_data) > 1:  # Add Table only if it contains data
        header_paragraph = Paragraph(section_header, styleH)

        table = Table(
            table_data,
            repeatRows=1,
            colWidths=[100, 130, 50, 50, 120, 200, 100],
        )
        style = TableStyle(
            [
                ("BACKGROUND", (0, 0), (-1, 0), colors.gray),
                ("TEXTCOLOR", (0, 0), (-1, 0), colors.whitesmoke),
                ("ALIGN", (0, 0), (-1, -1), "CENTER"),
                ("FONTNAME", (0, 0), (-1, 0), "Helvetica-Bold"),
                ("BOTTOMPADDING", (0, 0), (-1, 0), 12),
                ("BACKGROUND", (0, 1), (-1, -1), colors.beige),
                ("GRID", (0, 0), (-1, -1), 1, colors.black),
                ("VALIGN", (0, 0), (-1, -1), "TOP"),
            ]
        )
        table.setStyle(style)
        together = KeepTogether(
            [header_paragraph, Spacer(1, 12), table, Spacer(1, 12)]
        )
        doc_elements.append(together)

    for prop in nested_properties:
        doc_elements.extend(
            transform_to_table(prop, definitions, processed_datatypes)
        )

    return doc_elements


if __name__ == "__main__":
    # This containes example data
    generate_pdf_from_schema(
        Path("../data/metadata_schema/out.json"),
        Path("../data/metadata_schema/output.pdf"),
    )

# import json
# from reportlab.lib.pagesizes import letter, landscape
# from reportlab.lib import colors
# from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, \
#     Spacer, KeepTogether
# from reportlab.lib.styles import getSampleStyleSheet
##
#
# def main():
#     # Load the JSON schema
#     with open('out.json', 'r') as file:
#         schema = json.load(file)
#
#     definitions = schema["definitions"]
#
#     transform_to_table(schema)
#
#     # Create the PDF
#     doc = SimpleDocTemplate("output.pdf", pagesize=landscape(letter))
#     doc.build(doc_elements)
#
#
# styles = getSampleStyleSheet()
# styleN = styles["BodyText"]
# styleH = styles["Heading1"]
#
# doc_elements = []
#
# processed_datatypes = set()
#
# def to_paragraph(text):
#     return Paragraph(text.replace('\n', '<br />'), styleN)
#
#
# def get_allowed_occurrences(element, details, required_fields):
#     if details.get('type') == 'array':
#         return '1-n' if element in required_fields else '0-n'
#     if details.get('allOf'):
#         return '1' if element in required_fields else '0-1'
#     return '1' if element in required_fields else '0-1'
#
#
# def get_data_type(details) -> (str, bool):
#     # if controlled_vocab := details.get("enum"):
#     #     return ", ".join(controlled_vocab), False
#     datatype = details.get("type", "complex")
#     if datatype == "array":
#         return details.get("items").get("$ref", "").split("/")[-1], True
#     if datatype == "complex":
#         return details.get("allOf")[0].get("$ref", "").split("/")[-1], True
#     return datatype, False
#
#
# def transform_to_table(data):
#     nonlocal definitions
#     section_header = data["title"]
#     required_fields = data.get("required", [])
#     properties = data.get("properties", {})
#
#     # Create a header for the section
#
#     # Create table headers
#     table_data = [[
#         to_paragraph("Title"),
#         to_paragraph("Element Name"),
#         to_paragraph("How Often"),
#         to_paragraph("Obligation"),
#         to_paragraph("Data Type"),
#         to_paragraph("Description"),
#         to_paragraph("Controlled Vocabulary"),
#     ]]
#
#     # This will hold nested properties for breadth-first traversal
#     nested_properties = []
#
#     # Process each property for the current level
#     for element, details in properties.items():
#         title = details.get("title", "N/A")
#         element_name = element
#         description = details.get("description", "N/A")
#         how_often = get_allowed_occurrences(element, details, required_fields)
#         obligation = "M" if element in required_fields else "O"
#         datatype, complicated = get_data_type(details)
#
#         cv = ""  # Empty Controlled Vocabaulary
#         if dt := definitions.get(datatype):
#             if cv_ := dt.get("enum"):
#                 cv = ", ".join(cv_)
#
#         table_data.append(
#             [to_paragraph(title),
#              to_paragraph(element_name),
#              to_paragraph(how_often),
#              to_paragraph(obligation),
#              to_paragraph(datatype),
#              to_paragraph(description),
#              to_paragraph(cv),  # Controlled Vocabulary
#              ])
#
#         if complicated and datatype not in processed_datatypes:
#             tmp = definitions.get(datatype)
#             if tmp:
#                 nested_properties.append(tmp)
#                 processed_datatypes.add(
#                     datatype)  # Mark this datatype as processed
#
#     if len(table_data) > 1:
#         """
#         Add Table only if it contains data
#         """
#         header_paragraph = Paragraph(section_header, styleH)
#
#         # Add table for the current level
#         table = Table(table_data, repeatRows=1,
#                       colWidths=[100, 130, 50, 50, 120, 200, 100])
#         style = TableStyle([
#             ('BACKGROUND', (0, 0), (-1, 0), colors.gray),
#             ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
#             ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
#             ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
#             ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
#             ('BACKGROUND', (0, 1), (-1, -1), colors.beige),
#             ('GRID', (0, 0), (-1, -1), 1, colors.black),
#             ('VALIGN', (0, 0), (-1, -1), 'TOP')
#         ])
#         table.setStyle(style)
#         together = KeepTogether(
#             [header_paragraph, Spacer(1, 12), table, Spacer(1, 12)])
#         doc_elements.append(together)
#
#     # Process nested properties in a breadth-first manner
#     for prop in nested_properties:
#         transform_to_table(prop)
#
#
# if __name__ == "__main__":
#     main()
#
