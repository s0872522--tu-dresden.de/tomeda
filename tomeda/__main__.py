import logging

from .tomeda_logging import TomedaLogging

TomedaLogging(
    console_level=logging.INFO,
    file_level=logging.DEBUG,
)

from .main import main

if __name__ == "__main__":
    main()
