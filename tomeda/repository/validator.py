import json
import logging
from typing import (
    Any,
    TypeVar,
    cast,
)
from urllib.parse import urlparse

from typing_extensions import override

from tomeda.tomeda_logging import TraceLogger

from .constants import REPOSITORY_DATA, JSONType
from .http_handler import TomedaHttpHandler
from .repository_execption import (
    TomedaHttpConnectionError,
    TomedaHttpUnauthorizedError,
    TomedaRepositoryAuthenticationError,
    TomedaRepositoryBadResponseError,
    TomedaRepositoryConnectionError,
    TomedaRepositoryTypeError,
    TomedaRepositoryValueError,
)

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]

T = TypeVar("T")


class TomedaRepositoryValidator:
    def __init__(self, repository_type: str, url: str):
        self.repository_type = repository_type
        self.base_url = url

    def ensure_dict(
        self, data: JSONType, url: str, context: str
    ) -> dict[str, JSONType]:
        if not isinstance(data, dict):
            logger.trace(
                "Error while processing %s: Response from repository "
                "contains data which is not a dictionary.",
                context,
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                f"Error while processing {context}: Response from repository "
                "contains data which is not a dictionary.",
            )
        return cast(dict[str, JSONType], data)

    def ensure_list(
        self, data: JSONType, url: str, context: str
    ) -> list[JSONType]:
        if not isinstance(data, list):
            logger.trace(
                "Error while processing %s: Response from repository "
                "contains data which is not a list.",
                context,
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                f"Error while processing {context}: Response from repository "
                "contains data which is not a list.",
            )
        return cast(list[JSONType], data)

    def ensure_list_of_dicts(
        self, data: JSONType, url: str
    ) -> list[dict[str, JSONType]]:
        if not isinstance(data, list) or not all(
            isinstance(item, dict) for item in data
        ):
            logger.trace(
                "Response from repository contains data which is not a"
                " list with dictionaries."
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from repository contains data which is not a"
                " list with dictionaries.",
            )
        return cast(list[dict[str, JSONType]], data)

    def ensure_list_of_str(self, data: JSONType, url: str) -> list[str]:
        if not isinstance(data, list) or not all(
            isinstance(item, str) for item in data
        ):
            logger.trace(
                "Response from repository contains data which is not a"
                " list with strings."
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from repository contains data which is not a"
                " list with strings.",
            )
        return cast(list[str], data)

    def _handle_response_key_error(self, error: KeyError, context: str) -> None:
        logger.error(
            "Error while parsing %s from %s repository: %s",
            context,
            self.repository_type,
            error,
        )
        raise TomedaRepositoryBadResponseError(
            self.repository_type,
            self.base_url,
            f"Error while parsing {context} from {self.repository_type} "
            f"repository: {error}",
        ) from error

    def validate_and_extract_key(
        self,
        response: dict[str, JSONType],
        key: str,
        expected_type: type[T],
        url: str,
    ) -> T:
        # TODO: Add support for type union (e.g., str | None or JSONType)
        value = response.get(key, None)
        if not isinstance(value, expected_type):
            logger.trace(
                f"Response from Dataverse contains '{key}' which is not a"
                f" {expected_type.__name__}."
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                f"Response from Dataverse contains '{key}' which is not a"
                f" {expected_type.__name__}.",
            )
        return value

    def check_response(self, response: JSONType, url: str) -> None:
        """
        Check if the response contains a valid data structure.

        This is a placeholder method in the base class. Derived classes
        should override this method to implement repository-specific response
        validation. If the response is found to be invalid, an appropriate
        exception should be raised.

        Parameters
        ----------
        response : JSONType
            The JSON response received from the repository.
        url : str
            The URL from which the response was fetched. This is used for
            logging and for creating more informative exception messages.

        Raises
        ------
        TomedaRepositoryBadResponseError
            If the response does not contain valid data. This is just a
            suggested exception type; derived classes might raise different
            or additional exceptions based on the specific validation.

        Returns
        -------
        None
        """
        pass


class TomedaDataverseValidator(TomedaRepositoryValidator):
    @override
    def check_response(self, response: JSONType, url: str) -> None:
        """
        Check if the Dataverse response contains a valid data structure and no
        error message.

        This method validates the structure and content of the response.
        If the response is found to be invalid or contains an error
        status, an appropriate exception (`TomedaRepositoryBadResponseError`)
        is raised.

        Parameters
        ----------
        response : JSONType
            The JSON response received from Dataverse.
        url : str
            The URL from which the response was fetched. This is used
            for logging and for creating more informative exception messages.

        Raises
        ------
        TomedaRepositoryBadResponseError
            If the response from Dataverse does not contain valid data or if an
            error status is detected.

        """
        if not response:
            logger.trace("Response from Dataverse is empty.")
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from Dataverse is empty.",
            )
        if not isinstance(response, dict):
            logger.trace("Response from Dataverse is not a dictionary.")
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from Dataverse is not a dictionary.",
            )
        if "status" not in response:
            logger.trace("Response from Dataverse does not contain status.")
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from Dataverse does not contain status.",
            )
        if response["status"] == "ERROR":
            logger.trace(
                "Response from Dataverse contains error: '%s'",
                response.get("message", "No message"),
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from Dataverse contains error: "
                f"'{response.get('message', 'No message')}'",
            )
        if response["status"] != "OK":
            logger.trace(
                "Response from Dataverse contains unknown status: "
                "'%s', Message: '%s'",
                response["status"],
                response.get("message", "No message"),
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from Dataverse contains unknown status: "
                f"'{response['status']}', Message: "
                f"'{response.get('message', 'No message')}'",
            )
        if response.get("data") is None:
            logger.trace("Response from Dataverse does not contain data.")
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from Dataverse does not contain data.",
            )
        if not isinstance(response["data"], dict | list):
            logger.trace(
                "Response from Dataverse contains data which is not a"
                " dictionary or a list."
            )
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                "Response from Dataverse contains data which is not a"
                " dictionary or a list.",
            )
