"""
Main entry point for the tomeda package.
"""
import importlib
import json
import logging
import os
import sys

import pydantic

from . import (
    t01_gather_metadata,
    t02_upload_to_dataverse,
    t10_extract_dataverse_native_keys,
    t11_create_dataverse_tsv_from_mapping,
    t12_create_dataverse_compatible_json,
    t101_derive_dataverse_key,
)
from .baseclass import TomedaBaseClass
from .create_schema_documentation_pdf import generate_pdf_from_schema
from .params import get_parameters
from .tomeda_logging import TraceLogger

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


def main() -> None:
    param = get_parameters()

    # Create Schema Documentation
    if param.create_schema_documentation:
        logger.info("Creating schema documentation.")
        dataset_schema = load_dataset_class(
            param.schema_module, param.schema_class
        )
        dataset = TomedaBaseClass(dataset_schema, param)
        schema = json.dumps(dataset.root.schema())
        generate_pdf_from_schema(schema, param.output)

    # DOFF_01: Create Gatherer File
    if param.create_gatherer:
        logger.info("Creating gatherer file.")
        dataset_schema = load_dataset_class(
            param.schema_module, param.schema_class
        )
        dataset = TomedaBaseClass(dataset_schema, param)
        dataset.create_gatherer_file_nested_text(output_file=param.output)

    # DOFF_02: Create Dataset Table
    if param.create_dataset_table:
        logger.info("Creating dataset table.")
        dataset_schema = load_dataset_class(
            param.schema_module, param.schema_class
        )
        dataset = TomedaBaseClass(dataset_schema, param)
        dataset.create_info_table(output_file=param.output)

    # DOFF_03: Extract Dataverse Native Keys
    if param.extract_repository_metadata:
        logger.info("Extracting Dataverse native keys.")
        t10_extract_dataverse_native_keys.main(param)

    # DOFF_04: Derive Dataverse Keys
    if param.derive_repository_keys:
        logger.info("Deriving Dataverse keys.")
        t101_derive_dataverse_key.main(param)

    # DOFF_05: Derive Dataverse TSV from Mapping
    if param.derive_repository_tsv_from_mapping:
        logger.info("Deriving Dataverse TSV from mapping.")
        t11_create_dataverse_tsv_from_mapping.main(param)

    # USER_01: Read Gatherer File
    if param.read:
        logger.info("Reading gatherer file.")
        dataset_schema = load_dataset_class(
            param.schema_module, param.schema_class
        )
        dataset = TomedaBaseClass(dataset_schema, param)
        t01_gather_metadata.main(dataset, param)

    # USER_02: Create Dataverse Compatible JSON
    if param.create_repository_compatible_metadata:
        logger.info("Creating data repository compatible metadata.")
        t12_create_dataverse_compatible_json.main(param)

    # USER_03: Upload to Dataverse
    if param.upload:
        logger.info("Uploading to data repository.")
        t02_upload_to_dataverse.main(param)


def load_dataset_class(
    module: str, class_name: str
) -> type[pydantic.BaseModel]:
    """
    Load a pydantic dataset class from a module.

    Parameters
    ----------
    module
        Path to the module containing the dataset class.
    class_name
        Name of the dataset class.

    Returns
    -------
    type[pydantic.BaseModel]
        The dataset class.
    """
    file_dir = os.path.dirname(module)
    sys.path.append(file_dir)
    module_name = os.path.basename(module)
    module_name = os.path.splitext(module_name)[0]
    module_type = importlib.import_module(module_name)
    dataset: type[pydantic.BaseModel] = getattr(module_type, class_name)
    return dataset


if __name__ == "__main__":
    main()
