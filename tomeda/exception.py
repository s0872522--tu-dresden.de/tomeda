"""
Exceptions for Tomeda.
"""


class TomedaException(Exception):
    """Base exception for Tomeda."""

    def __init__(self, *args: object) -> None:
        super().__init__(args)


class TomedaImportError(TomedaException):
    """
    Exception raised when a module cannot be imported.

    This could be due to various reasons, such as the module not existing,
    the module being corrupt, etc.
    """

    def __init__(self, message: str, *args: object) -> None:
        super().__init__(message, args)


class TomedaFactoryError(TomedaException):
    """
    Base exception for all errors related to the Factory process.
    """

    def __init__(self, message: str, *args: object) -> None:
        super().__init__(message, args)


class TomedaFactoryTypeError(TomedaFactoryError):
    """
    Raised when the provided object is not of the expected type.
    """

    def __init__(self, message: str, *args: object) -> None:
        super().__init__(message, args)


class TomedaFactoryValueError(TomedaFactoryError):
    """
    Raised when the provided object does not have the expected value.
    """

    def __init__(self, message: str, *args: object) -> None:
        super().__init__(message, args)


class TomedaFactoryImportError(TomedaFactoryError, TomedaImportError):
    """
    Raised when the module or class cannot be imported.
    """

    def __init__(self, message: str, *args: object) -> None:
        super().__init__(message, args)


class TomedaFactoryInvalidClassError(TomedaFactoryError):
    """
    Raised when the imported class does not extend the expected base class.
    """

    def __init__(self, message: str, *args: object) -> None:
        super().__init__(message, args)


class TomedaFactoryInstantiationError(TomedaFactoryError):
    """
    Raised when there's an error instantiating the repository class.
    """

    def __init__(self, message: str, *args: object) -> None:
        super().__init__(message, args)
