"""
Default Exceptions for Tomeda.
"""

from .exception import TomedaException


class TomedaNotAPathError(TomedaException):
    """Exception for Tomeda if path is not a Path."""

    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


class TomedaUnexpectedTypeError(TomedaException):
    """Exception for Tomeda if a type is unexpected."""

    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


class TomedaValueError(TomedaException):
    """Exception for Tomeda if a value erroneous."""

    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)
