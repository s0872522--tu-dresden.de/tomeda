"""
This script is used to create a mapping table from dataset keys to dataverse
keys, using both dataset metadata and manually matched metadata.

Functions:

1. get_args() -> argparse.Namespace:
    This function parses command-line arguments which include paths to dataset
    keys, manually matched entries, an administration directory, a metadata
    information table, and an output file.

2. create_mapping_table(dataset_metadata: Path, manual_matched_metadata:
    Path | list[Path] = None) -> tuple[list[list[str, str]], list[str], list[str]]:
    This function creates the mapping table using the dataset metadata and
    manually matched metadata.

3. _read_manual_matched_entries(manual_matched_metadata:
    Path | list[Path] = None) -> dict[str, str]:
    This function reads manually matched entries from given files.

4. _create_mapping_table(info_table: dict[str, Any], manual_matched_entries:
    dict[str, str]) -> apping_rt_type:
    This function creates the mapping table based on the information table and
    manually matched entries.

5. camel_case(st: list[str]) -> str:
    This function converts a list of strings to camelCase.

The main driver of this script is the 'main' function which processes command
line arguments, creates the mapping table, writes the mapping table to a JSON
file, and updates the metadata information table file by adding a new field
'dataverse_name' to each entry.
"""
import json
import logging
from pathlib import Path
from typing import Any

import nestedtext as nt  # type: ignore[import]

from .default_exception import TomedaNotAPathError
from .file_handler import TomedaFileHandler
from .params import TomedaParameter
from .tomeda_logging import TraceLogger

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


def main(param: TomedaParameter) -> None:
    logger.info(f"Derive Dataverse Keys.")

    # manually matched metadata -> based on
    # "tomeda_10_extract_dataverse_native_keys.py"
    manually_matched_keys: list[Path] = [param.matched_entries]
    schema_info_table: Path = param.schema_info_table

    # mapping_table: dict[dataverse_key, dataset_key]
    # new_dataset_keys: list[new_dataset_key] (camelCase)
    # split_keys: list[dataset_key] (list of parents)

    # mapping_table_ds_to_dv, new_dataverse_keys, split_keys \

    schema_file_handle = TomedaFileHandler(schema_info_table, overwrite=True)
    schema_info: dict = nt.loads(schema_file_handle.read(raw=True)[0])

    mapping_table_dv_to_ds, new_dataset_keys, _ = create_mapping_table(
        info_table=schema_info, manual_matched_metadata=manually_matched_keys
    )

    mapping_table_file_handle = TomedaFileHandler(
        schema_info_table.parent / Path("complete_mapping.json"),
        overwrite=param.force_overwrite,
    )
    mapping_table_file_handle.write(
        json.dumps(mapping_table_dv_to_ds, indent=4)
    )

    new_dataset_keys_file_handle = TomedaFileHandler(
        schema_info_table.parent / Path("new_dataset_keys.txt"),
        overwrite=param.force_overwrite,
    )
    new_dataset_keys_file_handle.write([e + "\n" for e in new_dataset_keys])

    to_delete = []
    # add properly formatted dataverse name as defined in the
    # schema definition "name"
    for schema_info_entry, schema_info_value in schema_info.items():
        name = schema_info_value["name"]
        # schema_info_value["title"] = "eng_" + schema_info_value["title"]

        if name in mapping_table_dv_to_ds:
            schema_info_value["dataverse_name"] = mapping_table_dv_to_ds[name]
        else:
            # This value is ignored. So delete it:
            to_delete.append(schema_info_entry)

    for entry in to_delete:
        del schema_info[entry]

    schema_file_handle.write(nt.dumps(schema_info))

    logger.info(f"Derive Dataverse Keys. Done.")


def create_mapping_table(
    info_table: dict,
    manual_matched_metadata: Path | list[Path] = None,
) -> tuple[dict[str, str], list[str], list[str]]:
    """
    Create a mapping table using the dataset metadata and manually matched
    metadata.

    Parameters
    ----------
    dataset_metadata : Path
        Path to the dataset metadata.
    manual_matched_metadata : Path or list[Path]
        Path(s) to the manually matched metadata.

    Returns
    -------
    tuple
        Tuple containing the mapping table, new dataverse keys, and split keys.
    """
    # {dataset_key, dataverse_key}
    manual_matched_entries: dict[str, str] = _read_manual_matched_entries(
        manual_matched_metadata
    )

    # mapping_table: list[[dataverse_key, dataset_key]]
    # new_dataset_keys: list[new_dataset_key] (camelCase)
    # split_keys: list[dataset_key] (list of parents)
    mapping_table, new_dataset_keys, split_keys = _create_mapping_table(
        info_table, manual_matched_entries
    )

    mapping_table_ds_to_dv = {e[0]: e[1] for e in mapping_table}

    return mapping_table_ds_to_dv, new_dataset_keys, split_keys


def _read_manual_matched_entries(
    manual_matched_metadata: Path | list[Path] = None,
) -> dict[str, str]:
    """
    Read the manually matched entries from a file.

    Parameters
    ----------
    manual_matched_metadata : Path or list[Path]
        Path(s) to the manually matched metadata.

    Returns
    -------
    dict
        Dictionary with dataset key as key and dataverse key as value.
    """
    if manual_matched_metadata is None:
        manual_matched_metadata = []
    if isinstance(manual_matched_metadata, Path):
        manual_matched_metadata = [manual_matched_metadata]

    manual_matched_entries = {}
    for manual_matched_metadata_file in manual_matched_metadata:
        if not isinstance(manual_matched_metadata_file, Path):
            raise TomedaNotAPathError(
                str(manual_matched_metadata_file),
                "manual_matched_metadata_file is not a Path"
                f"but a {type(manual_matched_metadata_file)}",
            )

        manual_matched_metadata_file_handle = TomedaFileHandler(
            manual_matched_metadata_file
        )
        data = manual_matched_metadata_file_handle.read()
        for line in data:
            if len(line.split(",")) != 2:
                raise ValueError(
                    f"Invalid line in {manual_matched_metadata_file}: {line} : "
                    f"expected format: 'key,value'"
                )
            key, value = line.split(",")
            key = key.strip()
            value = value.strip()
            manual_matched_entries[value] = key

    return manual_matched_entries


mapping_rt_type = tuple[list[list[str]], list[str], list[str]]


def _create_mapping_table(
    info_table: dict[str, Any],
    manual_matched_entries: dict[str, str],
) -> mapping_rt_type:
    """
    Create a mapping table based on the information table and manually matched
    entries.

    Parameters
    ----------
    info_table : dict
        Dictionary containing dataset metadata information.
    manual_matched_entries : dict
        Dictionary of manually matched entries with dataset keys as keys and
        dataverse keys as values.

    Returns
    -------
    tuple
        A tuple containing three lists:
            1. A list of lists where each sublist consists of a dataverse key
                and a dataset key.
            2. A list of new dataverse keys.
            3. A list of split keys.
    """

    mapping_table_ds_to_dv = []
    new_dataverse_keys = []  # These will be used to generate the TSV
    split_keys = []

    # If there is a 'parent' node (a dataset_key with a dot in it),
    # you have to ignore everything that has it as a parent, unless it
    # it manually mapped as well.
    ignore_keys = []

    for dataset_key in info_table.keys():
        if manual_matched_key := manual_matched_entries.get(dataset_key):
            # Manual match overrides automatic 'match'
            dataverse_key = manual_matched_key
            if dataset_key.count(".") == 0:  # this is a parent
                ignore_keys.append(dataset_key)
        else:
            # Derives the new dataverse key from the dataset key
            dataset_key_split = dataset_key.split(".")
            if dataset_key_split[0] in ignore_keys:
                # This is a child of a parent that is manually mapped
                # so ignore it
                continue
            # ToDo: Fix the prefix generation 'Eng_'
            dataverse_key = "Eng_" + string_list_to_camelCase(dataset_key_split)
            new_dataverse_keys.append(dataverse_key)

        mapping_table_ds_to_dv.append([dataset_key, dataverse_key])

        if (level0_key := dataset_key.split(".")[0]) not in split_keys:
            split_keys.append(level0_key)

    return mapping_table_ds_to_dv, new_dataverse_keys, split_keys


def string_list_to_camelCase(st: list[str]) -> str:
    """
    Convert a list of strings to camelCase.
    Args:
        st:

    Returns
    -------
    str
        A camelCase string.

    """
    output = "".join(x[0].upper() + x[1:] for x in st if x.isalnum())
    return output[0].lower() + output[1:]
