from pathlib import Path

import pytest

from tomeda.default_exception import (
    TomedaNotAPathError,
    TomedaUnexpectedTypeError,
    TomedaValueError,
)
from tomeda.params import TomedaParameter, get_parameters


class TestTomedaParameter:
    def test_initialization(self):
        param = TomedaParameter()

        assert param.gatherer_file is None
        assert param.schema_module is None
        assert param.schema_class is None
        assert param.read is False
        assert param.collector_root is None

        assert param.output is None
        assert param.force_overwrite is False
        assert param.dry_run is False

        assert param.create_gatherer is False
        assert param.create_schema_documentation is False
        assert param.create_dataset_table is False
        assert param.check_recommendations is False
        assert param.extract_repository_metadata is False
        assert param.derive_repository_keys is False
        assert param.derive_repository_tsv_from_mapping is False
        assert param.create_repository_compatible_metadata is False
        assert param.tsv_dir == []
        assert param.matched_entries is None
        assert param.schema_info_table is None
        assert param.new_keys == []

        assert param.dataset_metadata is None
        assert param.mapping_table is None

        assert param.upload is False
        assert param.upload_file is None
        assert param.server_url is None
        assert param.target_collection is None
        assert param.query_fields == []
        assert param.api_token is None

    # Tests for _convert_to_bool
    @pytest.mark.parametrize(
        "value, expected",
        [
            (True, True),
            (False, False),
        ],
    )
    @pytest.mark.parametrize("current_value", [None, True, False])
    def test_convert_to_bool_with_bool(self, value, current_value, expected):
        assert (
            TomedaParameter._convert_to_bool(value, current_value) == expected
        )

    @pytest.mark.parametrize(
        "value, expected",
        [
            ("true", True),
            ("True", True),
            ("TRUE", True),
            ("tRue", True),
            (" true", True),
            ("true ", True),
            ("yes", True),
            ("YES", True),
            ("yEs", True),
            ("on", True),
            ("ON", True),
            ("oN", True),
            ("1", True),
            ("false", False),
            ("False", False),
            ("FALSE", False),
            ("fAlSe", False),
            (" false", False),
            ("false ", False),
            ("no", False),
            ("NO", False),
            ("nO", False),
            ("off", False),
            ("OFF", False),
            ("oFf", False),
            ("0", False),
            ("", False),
            (" ", False),
            ("  ", False),
            ("  \t  ", False),
            ("\t", False),
            ("\n", False),
            ("\r", False),
            ("\r\n", False),
        ],
    )
    @pytest.mark.parametrize("current_value", [None, True, False])
    def test_convert_to_bool_with_string(self, value, current_value, expected):
        assert (
            TomedaParameter._convert_to_bool(value, current_value) == expected
        )

    def test_convert_to_bool_with_none(self):
        assert TomedaParameter._convert_to_bool(None, True) is True
        assert TomedaParameter._convert_to_bool(None, False) is False
        assert TomedaParameter._convert_to_bool(None, None) is False

    @pytest.mark.parametrize(
        "value",
        [
            "not a bool",
            "t r u e",
            "f a l s e",
            "true false",
            "yes no",
            "on off",
            "true" * 1000,
            "false" * 1000,
            "Trueish",
            "qwerty 123",
            "123 qwerty",
            " qwerty 123 ",
            "QWERTY",
            "QWERTY 123",
            "123",
            "3.14",
            "[]",
            "{}",
            "[1, 2, 3]",
            "{1: 2, 3: 4}",
            "😊",
            "éàè",
            "こんにちは",  # Guten Tag
        ],
    )
    @pytest.mark.parametrize("current_value", [None, True, False])
    def test_convert_to_bool_with_invalid_string(self, value, current_value):
        with pytest.raises(TomedaValueError):
            TomedaParameter._convert_to_bool(value, current_value)

    @pytest.mark.parametrize(
        "value",
        [
            123,
            3.14,
            [],
            {},
            [1, 2, 3],
            {1: 2, 3: 4},
            Path("path/to/file"),
            TomedaParameter,
            TomedaParameter(),
        ],
    )
    @pytest.mark.parametrize("current_value", [None, True, False])
    def test_convert_to_bool_with_invalid_type(self, value, current_value):
        with pytest.raises(TomedaUnexpectedTypeError):
            # noinspection PyTypeChecker
            TomedaParameter._convert_to_bool(value, current_value)

    # Tests for _convert_to_string
    @pytest.mark.parametrize(
        "value, expected",
        [
            ("string", "string"),
            (" string", "string"),
            ("string ", "string"),
            (" string ", "string"),
            ("", ""),
            (" ", ""),
            ("  ", ""),
            ("  \t  ", ""),
            ("\t", ""),
            ("\n", ""),
            ("\r", ""),
            ("\r\n", ""),
        ],
    )
    @pytest.mark.parametrize("current_value", ["string", "", " ", None])
    def test_convert_to_string_with_string(
        self, value, current_value, expected
    ):
        assert (
            TomedaParameter._convert_to_string(value, current_value) == expected
        )

    @pytest.mark.parametrize(
        "current_value",
        [
            "string",
            "",
            " ",
            "\n",
            "\r",
            None,
        ],
    )
    def test_convert_to_string_with_none(self, current_value):
        assert (
            TomedaParameter._convert_to_string(None, current_value)
            == current_value
        )

    @pytest.mark.parametrize(
        "value",
        [
            True,
            False,
            123,
            3.14,
            [],
            {},
            [1, 2, 3],
            {1: 2, 3: 4},
            Path("path/to/file"),
            TomedaParameter,
            TomedaParameter(),
        ],
    )
    @pytest.mark.parametrize("current_value", ["string", "", " ", None])
    def test_convert_to_string_with_invalid_type(self, value, current_value):
        with pytest.raises(TomedaUnexpectedTypeError):
            TomedaParameter._convert_to_string(value, current_value)

    @pytest.mark.parametrize(
        "value, expected",
        [("😊", "😊"), ("éàè", "éàè"), ("こんにちは", "こんにちは")],  # Guten Tag
    )
    @pytest.mark.parametrize("current_value", ["string", "", " ", None])
    def test_convert_to_string_with_special_characters(
        self, value, current_value, expected
    ):
        assert (
            TomedaParameter._convert_to_string(value, current_value) == expected
        )

    @pytest.mark.parametrize("current_value", ["string", "", " ", None])
    def test_convert_to_string_with_long_string(self, current_value):
        long_string = "a" * 10000
        assert (
            TomedaParameter._convert_to_string(long_string, None) == long_string
        )

    # Tests for _convert_to_path
    @pytest.mark.parametrize(
        "value, expected",
        [
            ("/path/to/file", Path("/path/to/file")),
            ("relative/path", Path("relative/path")),
            ("file", Path("file")),
            ("file.txt", Path("file.txt")),
            ("file.txt.gz", Path("file.txt.gz")),
            ("C:\\path\\to\\file", Path("C:\\path\\to\\file")),
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_path_with_str(self, value, current_value, expected):
        result = TomedaParameter._convert_to_path(value, current_value)
        assert result == expected

    @pytest.mark.parametrize(
        "value",
        [
            Path("path/to/file"),
            Path("relative/path"),
            Path(""),
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_path_with_path(self, value, current_value):
        result = TomedaParameter._convert_to_path(value, current_value)
        assert isinstance(result, Path)
        assert result == value

    @pytest.mark.parametrize(
        "current_value",
        [
            Path("path/to/file"),
            Path("another/path"),
            Path(""),
            None,
        ],
    )
    def test_convert_to_path_with_none(self, current_value):
        assert (
            TomedaParameter._convert_to_path(None, current_value)
            == current_value
        )

    @pytest.mark.parametrize(
        "value",
        [
            123,
            3.14,
            True,
            False,
            [],
            {},
            [1, 2, 3],
            {1: 2, 3: 4},
            TomedaParameter,
            TomedaParameter(),
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_path_with_invalid_types(self, value, current_value):
        with pytest.raises(TomedaUnexpectedTypeError):
            # noinspection PyTypeChecker
            TomedaParameter._convert_to_path(value, current_value)

    @pytest.mark.parametrize(
        "value",
        [
            "<invalid:path>",
            "path|with|invalid|chars",
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_path_with_invalid_path_strings(
        self, value, current_value
    ):
        assert TomedaParameter._convert_to_path(value, current_value) == Path(
            value
        )

    @pytest.mark.parametrize(
        "value", ["", " ", "  ", "  \t  ", "\t", "\n", "\r"]
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_path_with_empty_or_whitespace_strings(
        self, value, current_value
    ):
        with pytest.raises(TomedaNotAPathError):
            TomedaParameter._convert_to_path(value, current_value)

    @pytest.mark.parametrize(
        "path_str, normalized_path",
        [
            ("path/./to/..//file", "path/to/../file"),
            ("/path/./to/..//file", "/path/to/../file"),
            ("C:\\path\\to\\..\\file", "C:\\path\\to\\..\\file"),
        ],
    )
    def test_convert_to_path_with_path_normalization(
        self, path_str, normalized_path
    ):
        result = TomedaParameter._convert_to_path(path_str, None)
        assert result == Path(normalized_path)

    # Tests for _convert_to_list_path
    @pytest.mark.parametrize(
        "value, expected",
        [
            ("path/to/file", [Path("path/to/file")]),
            ("file", [Path("file")]),
            ("file1,file2", [Path("file1"), Path("file2")]),
            ("file1, file2", [Path("file1"), Path("file2")]),
            (
                "/path/to/file1,/path/to/file2",
                [Path("/path/to/file1"), Path("/path/to/file2")],
            ),
            ("file1, /path/to/file2", [Path("file1"), Path("/path/to/file2")]),
            ("/path/to/file1, file2", [Path("/path/to/file1"), Path("file2")]),
            (
                "/path/to a/file1, file2",
                [Path("/path/to a/file1"), Path("file2")],
            ),
            ("file1,,file2", [Path("file1"), Path("file2")]),
            ("file1, ,file2", [Path("file1"), Path("file2")]),
            ("file1, \t\n ,file2", [Path("file1"), Path("file2")]),
            ("", []),
            ("  ", []),
            ("  \t  ", []),
            ("\t", []),
            ("\n", []),
            ("\r", []),
            ("\r\n", []),
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_list_path_with_str(
        self, value, current_value, expected
    ):
        result = TomedaParameter._convert_to_list_path(value, current_value)
        assert result == expected

    @pytest.mark.parametrize(
        "value",
        [
            Path("path/to/file"),
            Path("relative/path"),
            Path(""),
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_list_path_with_path(self, value, current_value):
        result = TomedaParameter._convert_to_list_path(value, current_value)
        assert isinstance(result, list)
        assert result == [value]

    @pytest.mark.parametrize(
        "current_value, expected",
        [
            (None, []),
            ([], []),
            (
                [Path("path/to/file"), Path("file")],
                [Path("path/to/file"), Path("file")],
            ),
        ],
    )
    def test_convert_to_list_path_with_none(self, current_value, expected):
        assert (
            TomedaParameter._convert_to_list_path(None, current_value)
            == expected
        )

    @pytest.mark.parametrize(
        "value, expected",
        [
            (["file1", "file2"], [Path("file1"), Path("file2")]),
            (
                ["file1", "file2", "", "  ", " \t\n "],
                [Path("file1"), Path("file2")],
            ),
            (["file1", "file2", None], [Path("file1"), Path("file2")]),
            ([None, "file1", "file2"], [Path("file1"), Path("file2")]),
            (["file1", Path("file2")], [Path("file1"), Path("file2")]),
            ([Path("file1"), Path("file2")], [Path("file1"), Path("file2")]),
            ([Path("file1"), "file2"], [Path("file1"), Path("file2")]),
            ([Path("file1"), None, "file2"], [Path("file1"), Path("file2")]),
            ([None], []),
            ([], []),
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_list_path_with_list(
        self, value, expected, current_value
    ):
        assert (
            TomedaParameter._convert_to_list_path(value, current_value)
            == expected
        )

    @pytest.mark.parametrize(
        "value",
        [
            123,
            3.14,
            True,
            False,
            {},
            [1, 2, 3],
            {1: 2, 3: 4},
            TomedaParameter,
            TomedaParameter(),
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_list_path_with_invalid_types(
        self, value, current_value
    ):
        with pytest.raises(TomedaUnexpectedTypeError):
            # noinspection PyTypeChecker
            TomedaParameter._convert_to_list_path(value, current_value)

    @pytest.mark.parametrize(
        "value",
        [
            "<invalid:path>",
            "path|with|invalid|chars",
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_list_path_with_invalid_path_strings(
        self, value, current_value
    ):
        assert TomedaParameter._convert_to_list_path(value, current_value) == [
            Path(value)
        ]

    @pytest.mark.parametrize(
        "value",
        [
            "",
            " ",
            "  ",
            "  \t  ",
            "\t",
            "\n",
            "\r",
        ],
    )
    @pytest.mark.parametrize(
        "current_value", [None, [Path("path/to/file"), Path("file")]]
    )
    def test_convert_to_list_path_with_empty_or_whitespace_strings(
        self, value, current_value
    ):
        assert TomedaParameter._convert_to_list_path(value, current_value) == []

    # Tests for _convert_to_list_str
    @pytest.mark.parametrize(
        "value, expected",
        [
            ("string", ["string"]),
            (" string", ["string"]),
            ("string ", ["string"]),
            (" string ", ["string"]),
            ("str1,str2", ["str1", "str2"]),
            ("str1, str2", ["str1", "str2"]),
            (" str1, str2 ", ["str1", "str2"]),
            ("str1, str2, ", ["str1", "str2"]),
            ("str1, str2, \t", ["str1", "str2"]),
            ("a" * 100000, ["a" * 100000]),
            ("a" * 100000 + "," + "b" * 100000, ["a" * 100000, "b" * 100000]),
        ],
        ids=[  # IDs needed to avoid pytest warning about to long test names
            "string",
            "string with leading space",
            "string with trailing space",
            "string with leading and trailing space",
            "string with comma",
            "string with comma and leading space",
            "string with comma and trailing space",
            "string with comma and leading and trailing space",
            "string with comma and tab",
            "long string",
            "long string with comma",
        ],
    )
    @pytest.mark.parametrize("current_value", [None, [], ["str", "str1"]])
    def test_convert_to_list_str_with_string(
        self, value, expected, current_value
    ):
        assert (
            TomedaParameter._convert_to_list_str(value, current_value)
            == expected
        )

    @pytest.mark.parametrize(
        "value, expected",
        [
            ("", []),
            (" ", []),
            ("  ", []),
            ("  \t  ", []),
            ("\t", []),
            ("\n", []),
            ("\r", []),
            ("\r\n", []),
        ],
    )
    @pytest.mark.parametrize("current_value", [None, [], ["str", "str1"]])
    def test_convert_to_list_str_with_empty_string(
        self, value, expected, current_value
    ):
        assert (
            TomedaParameter._convert_to_list_str(value, current_value)
            == expected
        )

    @pytest.mark.parametrize(
        "current_value, expected",
        [
            ([], []),
            (["str", "str1"], ["str", "str1"]),
            (None, []),
        ],
    )
    def test_convert_to_list_str_with_none(self, current_value, expected):
        assert (
            TomedaParameter._convert_to_list_str(None, current_value)
            == expected
        )

    @pytest.mark.parametrize(
        "value, expected",
        [
            (["str1", "str2"], ["str1", "str2"]),
            (["str1", "str2", "", "  ", " \t\n "], ["str1", "str2"]),
            (["str1", "str2", None], ["str1", "str2"]),
            ([None, "str1", "str2"], ["str1", "str2"]),
            (["str1", "str2", "str3"], ["str1", "str2", "str3"]),
            (
                ["str1", "str2", "str3", "str4"],
                ["str1", "str2", "str3", "str4"],
            ),
        ],
    )
    @pytest.mark.parametrize("current_value", [None, [], ["str", "str1"]])
    def test_convert_to_list_str_with_list(
        self, value, expected, current_value
    ):
        assert (
            TomedaParameter._convert_to_list_str(value, current_value)
            == expected
        )

    @pytest.mark.parametrize(
        "value",
        [
            123,
            3.14,
            True,
            False,
            {},
            [1, 2, 3],
            {1: 2, 3: 4},
            TomedaParameter,
            TomedaParameter(),
        ],
    )
    @pytest.mark.parametrize("current_value", [None, [], ["str", "str1"]])
    def test_convert_to_list_str_with_invalid_types(self, value, current_value):
        with pytest.raises(TomedaUnexpectedTypeError):
            # noinspection PyTypeChecker
            TomedaParameter._convert_to_list_str(value, current_value)
