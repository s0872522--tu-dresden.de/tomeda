import logging

from .baseclass import TomedaBaseClass
from .params import TomedaParameter
from .tomeda_logging import TraceLogger

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


def main(dataset: TomedaBaseClass, param: TomedaParameter) -> None:
    dataset.validate_gatherer_file(
        file=param.gatherer_file, collector_root=param.collector_root
    )

    if param.check_recommendations:
        dataset.check_if_recommendations_are_met()

    if param.output:
        dataset.write_json(output_file=param.output)
