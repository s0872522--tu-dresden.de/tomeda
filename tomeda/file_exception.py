"""
FileHandler Exceptions for Tomeda.
"""
from pathlib import Path

from .exception import TomedaException


class TomedaFileError(TomedaException):
    """Base exception for TomedaFileHandler."""

    def __init__(self, path: Path, *args: object) -> None:
        self.path = path
        super().__init__(args)


class TomedaFileValueError(TomedaFileError):
    """Exception for TomedaFileHandler if value is not valid."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileIOError(TomedaFileError):
    """Exception for FileHandler if file or directory IO fails."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileNotReadableError(TomedaFileIOError):
    """Exception for TomedaFileHandler if file is not readable."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileNotWritableError(TomedaFileIOError):
    """Exception for TomedaFileHandler if file is not writable."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFilePermissionError(TomedaFileError):
    """Exception for TomedaFileHandler if file or directory permission fails."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileExistsError(TomedaFileError):
    """Exception for TomedaFileHandler if file exists."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileCreateError(TomedaFileError):
    """Exception for TomedaFileHandler if creating file fails."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileDeleteError(TomedaFileError):
    """Exception for TomedaFileHandler if deleting file fails."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileEncodingError(TomedaFileError):
    """Exception for TomedaFileHandler if encoding file fails."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)


class TomedaFileDirectoryCreateError(TomedaFileError):
    """Exception for TomedaFileHandler if creating directory fails."""

    def __init__(self, path: Path, *args: object) -> None:
        super().__init__(path, args)
