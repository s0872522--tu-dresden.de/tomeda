#!/bin/bash
        
id_prep=$(sbatch                                  --parsable run_preprocessing.sbatch  )
id_comp=$(sbatch --dependency=afterok:${id_prep} --parsable run_processing.sbatch     )
id_post=$(sbatch --dependency=afterok:${id_comp} --parsable run_postprocessing.sbatch )
dtcp -r --dependency=afterok:${id_post}  VTK_decayIsoTurb_256_Smagorinsky /warm_archive/ws/s0872522-archival/OpenFOAM_LES_Turbulence_Data/

