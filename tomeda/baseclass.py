import copy
import inspect
import logging
import re
import subprocess
import sys
from collections.abc import Callable
from pathlib import Path
from subprocess import Popen

import nestedtext as nt  # type: ignore[import]
import pydantic
from pydantic.main import ModelField, ModelMetaclass

from .default_exception import TomedaNotAPathError, TomedaUnexpectedTypeError
from .file_handler import TomedaFileHandler
from .params import TomedaParameter
from .tomeda_logging import TraceLogger

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


class TomedaBaseClass:
    """
    This class is the base class for all tomeda classes. It provides the
    functionality to parse the gatherer file, evaluate the fields and write the
    json file.

    The class is based on the pydantic BaseModel class and uses the pydantic
    ModelMetaclass to create the class dynamically. The class is created from
    the gatherer file and the fields are evaluated. The class is then used to
    write the json file.
    """

    def __init__(
        self,
        dataset_root: type[pydantic.BaseModel],
        param: TomedaParameter,
    ) -> None:
        self.root = dataset_root
        self.root_validated = None
        self.param = param

    def write_json(self, output_file: Path) -> None:
        if self.root_validated is None:
            raise ValueError("No validated gatherer file available")

        if isinstance(output_file, list):
            output_file = output_file[0]

        if not isinstance(output_file, Path):
            raise TomedaNotAPathError(
                output_file,
                f"Expected a Path object, but" f" got {type(output_file)}",
            )

        output_file = (output_file / "metadata").with_suffix(".json")
        file_handler = TomedaFileHandler(Path(output_file))
        data_: str = self.root_validated.json(indent=4, exclude_unset=True)
        file_handler.write(data_)

        logger.info(f"Wrote json to {output_file}")

    def validate_gatherer_file(self, file: Path, collector_root: Path) -> None:
        if isinstance(file, list):
            file = file[0]
        if not isinstance(file, Path):
            raise TomedaNotAPathError(
                file, f"Expected a Path object, but" f" got {type(file)}"
            )

        try:
            logger.debug(f"Starting validation for gatherer file: {file}")
            logger.debug(f"Starting parse operation on file: {file}")

            dict_o = self._parse_gatherer_file(file)

            logger.debug(f"Starting filter operation on file: {file}")

            dict_o = self._filter_gatherer_file(dict_o)

            logger.debug(f"Starting evaluation on file: {file}")

            dict_evaluated = self._evaluate_fields(dict_o, collector_root)

            logger.debug(f"Starting PyDantic parsing on file: {file}")

            self.root_validated = self.root.parse_obj(dict_evaluated)

            logger.info(f"Successfully parsed and validated file: {file}")
        except Exception as e:
            logger.exception(
                f"Exception occurred while validating file {file}: {str(e)}"
            )
            raise

    @staticmethod
    def _parse_gatherer_file(file: Path) -> dict:
        try:
            content = TomedaFileHandler(file).read(raw=True)
        except OSError as e:
            print(e)
            sys.exit(-1)

        try:
            dict_o = nt.loads(content[0])
        except nt.NestedTextError as e:
            e.terminate()
            sys.exit(-1)

        return dict_o

    @staticmethod
    def _filter_gatherer_file(dict_o: dict) -> dict:
        """
        This function filters the gatherer file and removes all the fields that
        are not defined in the gatherer file
        https://stackoverflow.com/a/62159734/20510491
        """

        def strip_empties_from_list(data: list):
            new_data = []
            for v in data:
                if isinstance(v, dict):
                    v = strip_empties_from_dict(v)
                elif isinstance(v, list):
                    v = strip_empties_from_list(v)
                if v not in (None, str(), list(), dict()):
                    new_data.append(v)
            return new_data

        def strip_empties_from_dict(data: dict):
            new_data = {}
            for k, v in data.items():
                if isinstance(v, dict):
                    v = strip_empties_from_dict(v)
                elif isinstance(v, list):
                    v = strip_empties_from_list(v)
                if v not in (None, str(), list(), dict()):
                    new_data[k] = v
            return new_data

        return strip_empties_from_dict(dict_o)

    def _evaluate_fields(self, dict_in: dict, collector_root: Path) -> dict:
        """
        the function iterates through the nested dict and searches for the
        keywords between the "@@" signs and executes them in bash fashion and
        write the return values back in the dict instead of the keyword
        """
        if isinstance(collector_root, list):
            collector_root = collector_root[0]

        def extract_data_from_file(line: str) -> str:
            """
            This function checks whether the line corresponds to this pattern
            "metadataKey, <file>,<keyword>,<delimiter>[,<list-delimiter>,<list-index>]"
            and if so, it extracts the data from the file
            """

            def validate_layout_and_split_(line_: str) -> list[str]:
                """
                This function checks whether the line corresponds to this
                pattern "<file>,<keyword>,<delimiter>[,<list-delimiter>,<list-index>]"
                if not, it raises an error
                """
                line_ = line_.strip()
                if line_.endswith(","):
                    line_ = line_[:-1].strip()

                # Allow parsing of "," albeit being used as delimiter
                line_ = line_.replace('","', "$$$").replace("','", "$$$")
                if not (line_.count(",") == 2 or line_.count(",") == 4):
                    raise ValueError(
                        f"Line does not have the correct number "
                        f"of ',': found {line_.count(',')}, expected '2' or '4'."
                        f"Line: {line_}"
                    )

                split = line_.split(",")
                split_cmd = [s.replace("$$$", ",").strip() for s in split]

                return split_cmd

            def extract_data_from_source_file(
                file_: Path,
                keyword_: str,
                delimiter_: str,
                list_delimiter_: str = None,
                list_index_: int = None,
            ) -> str:
                """
                this function extracts the data from the file
                it searches for the keyword and returns the value after the
                delimiter
                * if the keyword is not found, it returns an empty string
                * if the delimiter is not found, it returns the rest of the line
                * if the keyword and the delimiter are not found, it returns the
                    whole line
                * if the keyword is found multiple times, it returns the first
                    match
                """
                if list_index_ is None:
                    list_index_ = 1  # Default to first element

                if delimiter_ in ['" "', "' '"]:  # Allow parsing of white space
                    delimiter_ = " "

                file_handler = TomedaFileHandler(file_)
                lines = file_handler.read()

                for line_ in lines:
                    if line_.startswith(keyword_) and delimiter_ in line_:
                        number_of_delimiters_in_keyword = keyword_.count(
                            delimiter_
                        )
                        rest_of_line = line_.split(
                            delimiter_, number_of_delimiters_in_keyword + 1
                        )[-1].strip()
                        if list_delimiter_ is None:
                            return rest_of_line

                        return rest_of_line.split(list_delimiter_)[
                            int(list_index_) - 1
                        ].strip()
                raise ValueError(
                    f"Keyword {keyword_} with delimiter {delimiter_=} and"
                    f" optional  list delimiter {list_delimiter_} "
                    f"and index {list_index_} not found in file {file_}"
                )

            source_file, *cmd = validate_layout_and_split_(line)

            source_file = collector_root / Path(source_file)
            source_file = Path(source_file).absolute()

            data = extract_data_from_source_file(source_file, *cmd)

            return data

        def dryrun_shell_process(value: str) -> None:
            print(f"Would execute: {value}")

        def check_command_for_errors(cmd: str) -> str:
            """
            This function checks whether the command contains any of the
            forbidden commands or symbols and raises an error if it does It
            is merely a safety measure to prevent the user from executing
            any commands on the system it is not a security measure
            """
            forbidden_commands = [
                r"\brm\b",
                r"/dev/null",
                r"\bmv\b",
                r"\bcp\b",
                r"\bchown\b",
                r"\bchmod\b",
                r"\bchgrp\b",
                r"\bkill\b",
            ]
            # symbols = [">", ";", "`", "#", "\\"]
            symbols = []

            # Check for forbidden commands
            for fc in forbidden_commands:
                if re.search(fc, cmd):
                    raise ValueError(f"Forbidden command in {cmd}")

            # Check for forbidden symbols
            for symbol in symbols:
                if symbol in cmd:
                    raise ValueError(f"{symbol} found in {cmd}")

            # Check for command substitution
            # if re.search(r"\$\(", cmd) or re.search(r"`", cmd):
            #     raise ValueError(f"Command substitution found in {cmd}")

            # Check for multi-command execution
            # if re.search(r"[\n;&]+", cmd):
            #     raise ValueError(f"Multi-command execution found in {cmd}")

            return cmd

        def execute_shell_process(cmd: str) -> str:
            cmd = check_command_for_errors(cmd)
            logger.debug(f"Execute: {cmd}")
            completed_process = subprocess.run(
                ["/bin/bash", "-c", cmd],
                capture_output=True,
                text=True,
            )

            err_message = completed_process.stderr
            if completed_process.returncode != 0:
                # the process was not successful
                raise ValueError(
                    f"Command {cmd} returned non-zero exit code "
                    f"{completed_process.returncode} with error message: {err_message}"
                )

            return_value: str = completed_process.stdout.strip()

            logger.debug(f"Returned: {return_value}")

            return return_value

        def execute_function_recursely_on_dict(
            dict_o: dict,
            function: Callable[[str], str],
        ) -> None:
            for key, value in dict_o.items():
                if isinstance(value, dict):
                    execute_function_recursely_on_dict(value, function)
                elif isinstance(value, list):
                    for idx, item in enumerate(value):
                        if isinstance(item, dict):
                            execute_function_recursely_on_dict(item, function)
                        elif isinstance(item, str):
                            dict_o.setdefault(key, [])[idx] = function(item)
                elif isinstance(value, str):
                    dict_o[key] = function(value)

        def run_keyword(line: str) -> str:
            """
            This function checks whether the line starts with one of the
            keywords and executes the corresponding function.

            @param line: the line to be checked
            @return: The return value of the function specified in the line
            """

            line = line.replace("TMD_COLL_ROOT", str(collector_root))
            line = line.strip()
            if line.startswith("@bash "):
                cmd = line[5:].strip()
                if self.param.dry_run:
                    dryrun_shell_process(cmd)
                    return cmd
                else:
                    return execute_shell_process(cmd)
            elif line.startswith("@file "):
                cmd = line[5:].strip()
                return extract_data_from_file(cmd)
            elif line.startswith("@literal "):
                content = line[8:].strip()
                return content

            raise NotImplementedError(f"Keyword {line} not implemented")

        dict_out = copy.deepcopy(dict_in)
        execute_function_recursely_on_dict(dict_out, run_keyword)

        return dict_out

    @classmethod
    def _flatten_keys(cls, nested_dict, prefix=None):
        flattened_keys = []

        def flatten_keys(nested_dict_, prefix_=None) -> list:
            flattened_keys_ = []

            if isinstance(nested_dict_, list):
                for idx, value in enumerate(nested_dict_):
                    new_key = prefix_
                    if isinstance(value, (dict, list)):
                        if not value:  # If the value is an empty dict or list
                            flattened_keys_.append(new_key)
                        else:
                            flattened_keys_.extend(flatten_keys(value, new_key))
                    else:
                        flattened_keys_.append(new_key)
            else:
                for key, value in nested_dict_.items():
                    new_key = f"{prefix_}.{key}" if prefix_ else key
                    if isinstance(value, (dict, list)):
                        if not value:  # If the value is an empty dict or list
                            flattened_keys_.append(new_key)
                        else:
                            flattened_keys_.extend(flatten_keys(value, new_key))
                    else:
                        flattened_keys_.append(new_key)

            return flattened_keys_

        keys = flatten_keys(nested_dict)

        return keys

    def get_class_structure(self) -> tuple[dict, dict]:
        """
        Retrieve the structure of the class.

        This method recursively traverses the class structure to extract the
        class attributes, types, and other related information. The returned
        structure includes whether the attribute allows multiple values,
        is required or not, and other extra information.

        Returns: tuple[dict, dict] : A tuple containing two dictionaries
        (Structure / Info).

        The first dictionary represents the class structure with each
        attribute as a key, and its value is either a nested dictionary (for
        complex types) or an empty dictionary (for basic types). The second
        dictionary contains additional information about each attribute,
        including its name, title, description, type, and whether it allows
        multiple values and is required.

        Notes
        -----
        This method makes use of several nested functions:
            * `loop_through_classes_recursively` : Traverses the class
                attributes and constructs the class structure.
            * `is_basic_type` : Checks if a given type is basic (i.e., not
                defined in the current file).
            * `assemble_line` : Assembles a string representation of the current
                attribute path.
            * `gather_info` : Gathers and stores additional information about an
                attribute.

        These helper functions are used to organize the logic of the
        `get_class_structure` method and make it easier to understand.
        """

        def loop_through_classes_recursely(
            cls: ModelMetaclass, dict_o: dict
        ) -> None:
            # get (type_class , attribute_name)
            nonlocal stack, prepend_string, stack_required, info

            types_names_multiple: list[
                tuple[ModelMetaclass, str, bool, bool, dict]
            ] = []

            for field in cls.__fields__:
                model_field: ModelField = cls.__fields__[field]
                name: str = model_field.name
                required: bool = model_field.required
                type_: ModelMetaclass = self.get_type_of(model_field)

                multiple = self.get_multiple_of(model_field)
                extra_info = self.get_extra_info(model_field)
                types_names_multiple.append(
                    (type_, name, multiple, required, extra_info)
                )

            for (
                cls_type,
                cls_name,
                multiple_allowed,
                required,
                extra_info,
            ) in types_names_multiple:
                if (cls_type, cls_name) in stack:
                    continue  # recursion blocker

                stack.append((cls_type, cls_name))
                stack_required.append(required)

                if isinstance(dict_o, list):
                    dict_o = dict_o[0]
                if dict_o.get(cls_name, None) is None:
                    if multiple_allowed is False:
                        dict_o[cls_name] = dict()
                    else:
                        dict_o[cls_name] = [{}]

                def is_basic_type(cls_type_: ModelMetaclass) -> bool:
                    # if type class not in this file defined, then is basic type
                    origin_path = inspect.getfile(self.root)
                    origin_file_name = Path(origin_path).stem

                    # basic_types = ['int','float','str','Email']
                    # for basic_type in basic_types:
                    #     if basic_type in str(cls_type_):
                    #         return True
                    # return False
                    #
                    type_ = cls_type_  # for debugging
                    val = origin_file_name not in str(cls_type_)
                    return val

                def assemble_line() -> str:
                    required_string = ""
                    prepend_string_str = "".join(prepend_string)
                    stack_str = ".".join([val[1] for val in stack])
                    return prepend_string_str + required_string + stack_str

                def gather_info(
                    cls_type_: ModelMetaclass, extra_info_: dict, info_: dict
                ) -> None:
                    type__ = (
                        str(cls_type_)
                        .removeprefix("<class '")
                        .removesuffix("'>")
                    )
                    if "EmailStr" in type__:
                        type__ = "email"

                    controlled_vocabulary = None

                    name_ = ".".join([val[1] for val in stack])
                    if type__.startswith("<enum"):
                        # Get the first member of the enum
                        first_member = next(iter(cls_type_))
                        # Get the underlying data type of the enum value
                        value_type = type(first_member.value)

                        type__ = (
                            str(value_type)
                            .removeprefix("<class '")
                            .removesuffix("'>")
                        )

                        controlled_vocabulary = [
                            member.value for member in cls_type_
                        ]

                    # extract 'read' type from complete qualified name
                    # (e.g. 'dataset.metadata.provenance' -> 'provenance')

                    type__ = type__.split(".")[-1]

                    line_info = {
                        "name": name_,
                        "title": extra_info_.get("title"),
                        "description": extra_info_.get("description"),
                        "allow_multiples": multiple_allowed,
                        "required": required,
                        "type": type__,
                    }

                    if controlled_vocabulary:
                        line_info[
                            "controlledVocabulary"
                        ] = controlled_vocabulary

                    info_[name_] = line_info

                gather_info(cls_type, extra_info, info)

                if is_basic_type(cls_type):
                    line_ = assemble_line()
                    # print(line_)
                    # f.write(line_ + '\n')
                    # name, title, description, allowmultiples, required
                else:
                    # update forward references to allow "wrong" order of
                    # classes in metadate definition file
                    # cls_type.update_forward_refs()
                    loop_through_classes_recursely(cls_type, dict_o[cls_name])

                stack_required.pop(-1)
                stack.pop(-1)

        info = {}
        stack: list[tuple[ModelMetaclass, str]] = []
        stack_required: list[bool] = []
        prepend_string: list[str] = []
        dict_output: dict = {}

        loop_through_classes_recursely(self.root, dict_output)

        return dict_output, info

    def create_flat_structure(self, output_file: Path):
        if isinstance(output_file, list):
            output_file = output_file[0]
        output_file = output_file.with_suffix(".txt")
        class_structure, _ = self.get_class_structure()
        flattened_keys = self._flatten_keys(class_structure)
        file_handler = TomedaFileHandler(
            output_file, overwrite=self.param.force_overwrite
        )
        file_handler.write(flattened_keys)

    def create_gatherer_file_nested_text(self, output_file: Path):
        if isinstance(output_file, list):
            output_file = output_file[0]
        output_file = output_file.with_suffix(".nt.template")
        file_handler = TomedaFileHandler(
            output_file, overwrite=self.param.force_overwrite
        )

        if file_handler.is_existing() and not file_handler.overwrite:
            logger.warning(
                "File %s already exists. Will not overwrite. "
                "Specify '--overwrite' to overwrite the file.",
                output_file,
            )
            return

        schema_file = self.param.schema_module
        schema_root_class = self.param.schema_class

        content = [
            "# This is a 'ToMeDa Collector File' for metadata collection written in NestedText language",
            "# https://nestedtext.org/",
            "#",
            "# This very file is derived from",
            f"# {schema_file}:{schema_root_class}",
            "#",
            "# The Collector file can be specified in different ways:",
            "#    @file | @bash | @literal",
            "#",
            "# 1. File Extraction: Specify values by:",
            "#      a file to read from and its location within the file",
            "#      The list-index is 1 indexed (starts at 1), to specify a comma use ',' or \",\".",
            "#",
            "#      metadataKey: @file <file>,<keyword>,<delimiter>[,<list-delimiter>,<list-index>]",
            "#      e.g. metadataKey: @file /path/to/file,keyword,delimiter",
            "#",
            "# 2. Bash Execution: Specify values by bash commands:",
            "#      Commands after '@bash' are bash commands and will be executed as such",
            "#      The return value of the command will be written to the gatherer file",
            "#      e.g. metadataKey: @bash $USER",
            "#      e.g. metadataKey: @bash cat myfile | grep keyword",
            "#",
            "# 3. Literal Definition: Specify values by literal definition:",
            "#      metadataKey: @literal value",
            "#",
            "",
        ]  # pylint: disable=line-too-long

        class_structure, _ = self.get_class_structure()
        nt_s: list[str] = nt.dumps(class_structure).split("\n")
        content += [line for line in nt_s if "{}" not in line]
        file_handler.write(content)

        logger.info(f"Created empty gatherer file: {output_file}")

    def create_info_table(self, output_file: Path):
        if isinstance(output_file, list):
            output_file = output_file[0]
        structure, info = self.get_class_structure()

        if not isinstance(output_file, Path):
            raise TomedaNotAPathError(
                output_file,
                f"Expected a Path object, but" f" got {type(output_file)}",
            )

        file_handler_info = TomedaFileHandler(
            output_file.with_suffix(".nt"), overwrite=self.param.force_overwrite
        )
        file_handler_info.write(nt.dumps(info))

        required = self.get_required(structure, info)
        file_handler_required = TomedaFileHandler(
            output_file.with_suffix(".required.nt"),
            overwrite=self.param.force_overwrite,
        )
        file_handler_required.write(required)

    @staticmethod
    def get_required(structure, info):
        def traverse_json(json_data: dict | list, callback_: Callable):
            nonlocal stack
            if isinstance(json_data, dict):
                for key, value in json_data.items():
                    stack.append(key)
                    continue_ = callback_(key, value, stack)
                    if continue_:
                        traverse_json(value, callback_)
                    stack.pop(-1)

            elif isinstance(json_data, list):
                for item in json_data:
                    traverse_json(item, callback_)

        stack = []
        required = []

        def callback(key, value, stack_):
            nonlocal required, info
            info_key = ".".join(stack_)
            info_ = info[info_key]
            if info_["required"]:
                if not value:
                    required.append(info_key)
                return True
            else:
                return False

        traverse_json(structure, callback)
        return required

    def check_if_recommendations_are_met(self):
        def check_for_recommendations_recursely(
            current_class: ModelMetaclass,
        ) -> None:
            # get (type_class , attribute_name)
            nonlocal stack, recommender_stack
            types_and_names: list[tuple[ModelMetaclass, str]] = []
            for field in current_class.__fields__:
                model_field = current_class.__fields__[field]
                name = model_field.name
                type_ = self.get_type_of(model_field)
                types_and_names.append((type_, name))

            for cls_type, cls_name in types_and_names:
                if cls_name in stack:
                    continue  # recursion blocker

                stack.append(cls_name)
                origin_path = inspect.getfile(self.root)
                origin_file_name = Path(origin_path).stem

                if (
                    hasattr(current_class, "_recommended")
                    and cls_name in current_class._recommended
                ):
                    # class_name = str(cls).split(".")[-1][:-2]
                    class_name = (
                        str(current_class.__class__).split(".")[-1].rstrip("'>")
                    )
                    msg_ = (
                        f" '{cls_name}' found in "
                        f"{str(self.root_validated.__class__.__name__)}."
                        f"{'.'.join(stack)}  "
                    )
                    recommender_stack.append(msg_)

                if origin_file_name in str(cls_type):
                    check_for_recommendations_recursely(cls_type)
                stack.pop(-1)

        stack: list[str] = []
        recommender_stack: list[str] = []

        if self.root_validated is None:
            raise ValueError("No validated root available - validate first")

        logger.info(f"Checking for recommendations")

        check_for_recommendations_recursely(self.root_validated)

        if len(set(recommender_stack)) > 0:
            logger.warning(
                f"{len(set(recommender_stack))} Recommendations not met!"
            )
            for msg in set(recommender_stack):
                logger.warning(f" * {msg}")
        else:
            logger.info("All recommended fields are set")

    @staticmethod
    def get_type_of(model_field: ModelField) -> ModelMetaclass:
        """
        check if model field is something like a Union and extract "true" data
        types
        """
        if (
            hasattr(model_field, "sub_fields")
            and model_field.sub_fields is not None
        ):
            if not isinstance(model_field.sub_fields[0], ModelField):
                raise TomedaUnexpectedTypeError(
                    model_field.sub_fields[0],
                    "First element of list 'sub_fields' is not of type"
                    f" 'ModelField', but of type"
                    f"{type(model_field.sub_fields[0])}",
                )

            type_: ModelMetaclass = model_field.sub_fields[0].type_

            # validate that the type of the subfields is the same type as type_
            for sub_field in model_field.sub_fields:
                if not (sub_field.type_ == type_):
                    raise TomedaUnexpectedTypeError(
                        sub_field.type_,
                        f"Value {model_field.name} has two different types: "
                        f"{type_} and  {sub_field.type_}: Not Supported",
                    )
        else:
            type_: ModelMetaclass = model_field.type_

        return type_

    @staticmethod
    def get_multiple_of(model_field: ModelField) -> bool:
        """
        check if model field is something like a Union and extract "true" data
        types
        """
        if (
            hasattr(model_field, "sub_fields")
            and model_field.sub_fields is not None
        ):
            if not isinstance(model_field.sub_fields, list):
                raise TomedaUnexpectedTypeError(
                    model_field.sub_fields,
                    "Value 'sub_fields' is not of type 'list', but of type"
                    f" {type(model_field.sub_fields)}",
                )

            multiple: bool = True
        else:
            multiple: bool = False

        return multiple

    @staticmethod
    def get_extra_info(model_field: ModelField) -> dict:
        extra_info_keys = ["title", "description"]

        extra_info = model_field.field_info.extra.copy() or {}

        extra_info.update(
            {
                key: getattr(model_field.field_info, key)
                for key in extra_info_keys
                if getattr(model_field.field_info, key)
            }
        )

        return extra_info


def get_42():
    return 42
