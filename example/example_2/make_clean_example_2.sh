#!/bin/env bash
set -e

export TOMEDA_USER_DIR=./tomeda/user
export TOMEDA_ADMIN_DIR=./tomeda/admin

export CFGNAME=./config.nt

LOOP_ROOT=./OpenFOAM_Data_DecayIsoTurb/

for dir in $LOOP_ROOT/*/; do
    echo "$dir"
    export APP_DIR=$dir
    rm -rf $TOMEDA_USER_DIR
    rm -rf $TOMEDA_ADMIN_DIR
    rm -rf $APP_DIR/metadata.json
    rm -rf $APP_DIR/metadata_for_upload.json
done



