import logging
from typing import Any, cast

from typing_extensions import override

from tomeda.tomeda_logging import TraceLogger

from .constants import JSONType
from .models import (
    RepositoryDataCollection,
    RepositoryDataCollectionContent,
    RepositoryDataDataset,
    RepositoryDataDatasetVersion,
    RepositoryDataMetadataBlock,
    RepositoryDataMetadataBlockFieldDefinition,
    RepositoryDataMetadataBlockFieldValue,
    RepositoryDataServer,
    RepositoryDataUser,
)
from .repository_connector import TomedaRepositoryConnector
from .repository_execption import (
    TomedaHttpConnectionError,
    TomedaHttpException,
    TomedaHttpUnauthorizedError,
    TomedaRepositoryAuthenticationError,
    TomedaRepositoryBadResponseError,
    TomedaRepositoryConnectionError,
    TomedaRepositoryError,
    TomedaRepositoryNotFoundError,
    TomedaRepositoryTypeError,
    TomedaRepositoryValueError,
)
from .validator import TomedaRepositoryValidator

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


class TomedaDataverseConnector(TomedaRepositoryConnector):
    API_VERSION = "v1"

    def __init__(
        self,
        repository_type: str,
        validator: TomedaRepositoryValidator,
        url: str,
        api_token: str,
    ):
        super().__init__(repository_type, validator, url.rstrip("/"), api_token)

    @override
    def _set_base_url_api(self) -> str:
        """
        Constructs the base API URL for the Dataverse repository.

        The base API URL is formed by appending '/api/' and the API version
        to the base URL of the repository. The method also validates the
        constructed URL before returning it.

        Overrides the abstract method in the superclass.

        Returns
        -------
        str
            The base API URL for the Dataverse repository.

        Raises
        ------
        TomedaRepositoryTypeError
            If the base URL is not a string.
        TomedaRepositoryValueError
            If the constructed URL does not refer to the repository or if the
            URL is not a valid Unicode string.
        """
        url = f"{self.base_url}/api/{self.API_VERSION}"
        self._validate_url(url)
        return url

    @override
    def _get_auth_headers(self) -> dict[str, str]:
        """
        Returns authentication headers for a request for
        TomedaDataverseConnector.

        In this implementation, the method returns a dictionary with the
        Dataverse authentication header "X-Dataverse-key" and the API token as
        value.

        Returns
        -------
        dict[str, str]
            A dictionary with the 'X-Dataverse-key' header for
            TomedaDataverseConnector.
        """
        return {"X-Dataverse-key": self.api_token}

    @override
    def _fetch_api_data(self, url: str, context: str) -> JSONType:
        # "Return Type Contravariance" used
        raw_response: JSONType = self._get_json_from_url(url, context)
        self.validator.check_response(raw_response, url)
        response = cast(dict[str, JSONType], raw_response)
        data = response["data"]
        return data

    @override
    def get_server_info(self) -> RepositoryDataServer:
        url_version = f"{self.base_url_api}/info/version"
        raw_response_version: JSONType = self._get_json_from_url(
            url_version, "get_server_info_version"
        )
        self.validator.check_response(raw_response_version, url_version)
        raw_response_version_cast = cast(
            dict[str, JSONType], raw_response_version
        )
        response_version = cast(
            dict[str, JSONType], raw_response_version_cast["data"]
        )

        url_server = f"{self.base_url_api}/info/server"
        raw_response_server: JSONType = self._get_json_from_url(
            url_server, "get_server_info_server"
        )
        self.validator.check_response(raw_response_server, url_server)
        raw_response_server_cast = cast(
            dict[str, JSONType], raw_response_server
        )
        response_server = cast(
            dict[str, JSONType], raw_response_server_cast["data"]
        )

        return RepositoryDataServer(
            server=self.validator.validate_and_extract_key(
                response_server, "message", str, url_server
            ),
            # server=raw_response_server,
            version=self.validator.validate_and_extract_key(
                response_version, "version", str, url_version
            ),
            build=self.validator.validate_and_extract_key(
                response_version, "build", str, url_version
            ),
            api_version=self.API_VERSION,
        )

    @override
    def get_user_info(self) -> RepositoryDataUser:
        url = f"{self.base_url_api}/users/:me"
        raw_response: JSONType = self._get_json_from_url(url, "get_user_info")
        self.validator.check_response(raw_response, url)
        raw_response_cast = cast(dict[str, JSONType], raw_response)
        response = cast(dict[str, JSONType], raw_response_cast["data"])

        return RepositoryDataUser(
            id_=self.validator.validate_and_extract_key(
                response, "id", int, url
            ),
            username=self.validator.validate_and_extract_key(
                response, "persistentUserId", str, url
            ),
            firstname=self.validator.validate_and_extract_key(
                response, "firstName", str, url
            ),
            lastname=self.validator.validate_and_extract_key(
                response, "lastName", str, url
            ),
            display_name=self.validator.validate_and_extract_key(
                response, "displayName", str, url
            ),
            email=self.validator.validate_and_extract_key(
                response, "email", str, url
            ),
            superuser=self.validator.validate_and_extract_key(
                response, "superuser", bool, url
            ),
            deactivated=self.validator.validate_and_extract_key(
                response, "deactivated", bool, url
            ),
            create_date=self.validator.validate_and_extract_key(
                response, "createdTime", str, url
            ),
            raw_data=response,
        )

    @override
    def get_metadata_blocks(self) -> list[RepositoryDataMetadataBlock]:
        url = f"{self.base_url_api}/metadatablocks"
        response = self._fetch_api_data(url, "get_metadata_blocks")

        contents: list[RepositoryDataMetadataBlock] = []
        items = self.validator.ensure_list_of_dicts(response, url)
        for item in items:
            contents.append(
                RepositoryDataMetadataBlock(
                    id_=self.validator.validate_and_extract_key(
                        item, "id", int, url
                    ),
                    name=self.validator.validate_and_extract_key(
                        item, "name", str, url
                    ),
                    display_name=self.validator.validate_and_extract_key(
                        item, "displayName", str, url
                    ),
                    field_definition=None,
                    field_value=None,
                    raw_data=response,
                )
            )
        return contents

    @override
    def get_metadata_block_definition(
        self, block_id: int | str
    ) -> list[RepositoryDataMetadataBlockFieldDefinition]:
        url = f"{self.base_url_api}/metadatablocks/{block_id}"
        response = self._fetch_api_data(url, "get_metadata_blocks")
        fields = self.validator.ensure_dict(
            self.validator.ensure_dict(response, url, "")["fields"],
            url,
            "",
        )

        contents: list[RepositoryDataMetadataBlockFieldDefinition] = []
        for item in fields.values():
            item_cast = self.validator.ensure_dict(item, url, "")
            contents.append(
                RepositoryDataMetadataBlockFieldDefinition(
                    name=self.validator.validate_and_extract_key(
                        item_cast, "name", str, url
                    ),
                    display_name=self.validator.validate_and_extract_key(
                        item_cast, "displayName", str, url
                    ),
                    title=self.validator.validate_and_extract_key(
                        item_cast, "title", str, url
                    ),
                    type_=self.validator.validate_and_extract_key(
                        item_cast, "type", str, url
                    ),
                    description=self.validator.validate_and_extract_key(
                        item_cast, "description", str, url
                    ),
                    format_=self.validator.validate_and_extract_key(
                        item_cast, "displayFormat", str, url
                    ),
                    is_controlled_vocabulary=self.validator.validate_and_extract_key(
                        item_cast, "isControlledVocabulary", bool, url
                    ),
                    multiple=self.validator.validate_and_extract_key(
                        item_cast, "multiple", bool, url
                    ),
                    watermark=self.validator.validate_and_extract_key(
                        item_cast, "watermark", str, url
                    ),
                    raw_data=response,
                )
            )
        return contents

    @override
    def get_collection(self, collection_id: str) -> RepositoryDataCollection:
        url = f"{self.base_url_api}/dataverses/{collection_id}"
        raw_response: JSONType = self._get_json_from_url(url, "get_collection")
        self.validator.check_response(raw_response, url)
        raw_response_cast = cast(dict[str, JSONType], raw_response)
        response = cast(dict[str, JSONType], raw_response_cast["data"])

        return RepositoryDataCollection(
            id=self.validator.validate_and_extract_key(
                response, "id", int, url
            ),
            id_alias=self.validator.validate_and_extract_key(
                response, "alias", str, url
            ),
            name=self.validator.validate_and_extract_key(
                response, "name", str, url
            ),
            creation_date=self.validator.validate_and_extract_key(
                response, "creationDate", str, url
            ),
            contact=[
                item["contactEmail"]
                for item in self.validator.validate_and_extract_key(
                    response, "dataverseContacts", list, url
                )
            ],
            category=self.validator.validate_and_extract_key(
                response, "dataverseType", str, url
            ),
            raw_data=response,
        )

    @override
    def get_collection_contents(
        self, collection_id: str
    ) -> list[RepositoryDataCollectionContent]:
        url = f"{self.base_url_api}/dataverses/{collection_id}/contents"
        raw_response: JSONType = self._get_json_from_url(
            url, "get_collection_contents"
        )
        self.validator.check_response(raw_response, url)
        response = cast(dict[str, JSONType], raw_response)["data"]

        contents: list[RepositoryDataCollectionContent] = []
        items = self.validator.ensure_list_of_dicts(response, url)
        for item in items:
            contents.append(
                RepositoryDataCollectionContent(
                    id_=self.validator.validate_and_extract_key(
                        item, "id", int, url
                    ),
                    id_alias=self.validator.validate_and_extract_key(
                        item, "identifier", str, url
                    ),
                    persistent_id=(
                        "doi:"
                        + self.validator.validate_and_extract_key(
                            item, "authority", str, url
                        )
                        + "/"
                        + self.validator.validate_and_extract_key(
                            item, "identifier", str, url
                        )
                    ),
                    type_=self.validator.validate_and_extract_key(
                        item, "type", str, url
                    ),
                    publisher=self.validator.validate_and_extract_key(
                        item, "publisher", str, url
                    ),
                    publication_date=self.validator.validate_and_extract_key(
                        item, "publicationDate", str, url
                    ),
                    authority=self.validator.validate_and_extract_key(
                        item, "authority", str, url
                    ),
                    persistent_url=self.validator.validate_and_extract_key(
                        item, "persistentUrl", str, url
                    ),
                    raw_data=item,
                )
            )
        return contents

    @override
    def get_collection_metadata_blocks(
        self, collection_id: str
    ) -> list[RepositoryDataMetadataBlock]:
        url = f"{self.base_url_api}/dataverses/{collection_id}/metadatablocks"
        response = self._fetch_api_data(url, "get_collection_metadata_blocks")

        contents: list[RepositoryDataMetadataBlock] = []
        items = self.validator.ensure_list_of_dicts(response, url)
        for item in items:
            contents.append(
                RepositoryDataMetadataBlock(
                    id_=self.validator.validate_and_extract_key(
                        item, "id", int, url
                    ),
                    name=self.validator.validate_and_extract_key(
                        item, "name", str, url
                    ),
                    display_name=self.validator.validate_and_extract_key(
                        item, "displayName", str, url
                    ),
                    field_definition=None,
                    field_value=None,
                    raw_data=response,
                )
            )
        return contents

    def get_dataset(
        self, dataset_id: int, dataset_name: str
    ) -> RepositoryDataDataset:
        # TODO: use id or name
        url = (
            f"{self.base_url_api}/datasets/:persistentId/?"
            f"persistentId={dataset_name}"
        )
        response = self._fetch_api_data(url, "get_collection_metadata_blocks")

        dataset_raw = self.validator.ensure_dict(response, url, "")
        version_raw = self.validator.ensure_dict(
            self.validator.validate_and_extract_key(
                dataset_raw, "latestVersion", dict, url
            ),
            url,
            "latestVersion",
        )
        version_data = self._create_dataset_version(version_raw, url)

        return RepositoryDataDataset(
            id_=self.validator.validate_and_extract_key(
                dataset_raw, "id", int, url
            ),
            id_alias=self.validator.validate_and_extract_key(
                dataset_raw, "identifier", str, url
            ),
            persistent_id=self.validator.validate_and_extract_key(
                dataset_raw, "persistentUrl", str, url
            ),
            latest_version=version_data,
            publisher=self.validator.validate_and_extract_key(
                dataset_raw, "publisher", str, url
            ),
            authority=self.validator.validate_and_extract_key(
                dataset_raw, "authority", str, url
            ),
            persistent_url=self.validator.validate_and_extract_key(
                dataset_raw, "persistentUrl", str, url
            ),
            publication_date=self.validator.validate_and_extract_key(
                dataset_raw, "publicationDate", str, url
            ),
            raw_data=response,
        )

    def _create_dataset_version(
        self, version_json: dict[str, JSONType], url: str
    ) -> RepositoryDataDatasetVersion:
        version_checked = self.validator.ensure_dict(version_json, url, "")
        metadata_blocks_raw = self.validator.ensure_dict(
            self.validator.validate_and_extract_key(
                version_checked, "metadataBlocks", dict, url
            ),
            url,
            "metadataBlocks",
        )
        metadata_blocks_data = self._create_dataset_metablock(
            metadata_blocks_raw, url
        )
        files = self.validator.ensure_list_of_str(
            self.validator.validate_and_extract_key(
                version_checked, "files", list, url
            ),
            url,
        )
        version_data = RepositoryDataDatasetVersion(
            id_=self.validator.validate_and_extract_key(
                version_checked, "id", int, url
            ),
            version=str(
                self.validator.validate_and_extract_key(
                    version_checked, "versionNumber", int, url
                )
            )
            + "."
            + str(
                self.validator.validate_and_extract_key(
                    version_checked, "versionMinorNumber", int, url
                )
            ),
            version_state=self.validator.validate_and_extract_key(
                version_checked, "versionState", str, url
            ),
            metadata_block=metadata_blocks_data,
            files=files,
            create_date=self.validator.validate_and_extract_key(
                version_checked, "createTime", str, url
            ),
            release_date=self.validator.validate_and_extract_key(
                version_checked, "releaseTime", str, url
            ),
            publication_date=self.validator.validate_and_extract_key(
                version_checked, "publicationDate", str, url
            ),
            citation_date=self.validator.validate_and_extract_key(
                version_checked, "citationDate", str, url
            ),
            last_update_date=self.validator.validate_and_extract_key(
                version_checked, "lastUpdateTime", str, url
            ),
            raw_data=version_checked,
        )
        return version_data

    def _create_dataset_metablock(
        self, metadata_blocks_json: dict[str, JSONType], url: str
    ) -> dict[str, RepositoryDataMetadataBlock]:
        metadata_blocks_checked = self.validator.ensure_dict(
            metadata_blocks_json, url, "metadata_blocks"
        )
        metadata_blocks_data: dict[str, RepositoryDataMetadataBlock] = {}
        for key, block_data_raw in metadata_blocks_checked.items():
            block_data = self.validator.ensure_dict(
                block_data_raw, url, "block_data" + key
            )

            field_values_raw = self.validator.ensure_list_of_dicts(
                self.validator.validate_and_extract_key(
                    block_data, "fields", list, url
                ),
                url,
            )
            field_values_data = [
                self._create_dataset_field_value(field_data, url)
                for field_data in field_values_raw
            ]

            metadata_blocks_data |= {
                key: RepositoryDataMetadataBlock(
                    id_=None,
                    name=key,  # TODO: Check string
                    display_name=self.validator.validate_and_extract_key(
                        block_data, "displayName", str, url
                    ),
                    field_definition=None,
                    field_value=field_values_data,
                    raw_data=block_data,
                )
            }
        return metadata_blocks_data

    def _create_dataset_field_value(
        self, field_data: JSONType, url: str
    ) -> RepositoryDataMetadataBlockFieldValue:
        field_data_checked = self.validator.ensure_dict(field_data, url, "")
        # TODO: Fix validate_and_extract_key for multiple types

        # value field
        raw_value = field_data_checked.get("value", None)
        value: str | list[str | RepositoryDataMetadataBlockFieldValue]
        if isinstance(raw_value, str):
            value = raw_value
        elif isinstance(raw_value, list):
            # TODO: Check recursion depth
            value = []
            raw_value_cast = self.validator.ensure_list(raw_value, url, "value")
            if all(isinstance(item, str) for item in raw_value_cast):
                value = self.validator.ensure_list_of_str(
                    raw_value_cast, url
                )  # type: ignore[assignment]
            elif all(isinstance(item, dict) for item in raw_value_cast):
                for list_item in raw_value_cast:
                    list_item_cast = self.validator.ensure_dict(
                        list_item, url, ""
                    )
                    for dict_item in list_item_cast.values():
                        value.append(
                            self._create_dataset_field_value(dict_item, url)
                        )
        else:
            logger.trace("Value is not a string or list: %s", raw_value)
            raise TomedaRepositoryBadResponseError(
                self.repository_type,
                url,
                f"Value is not a string or list: {raw_value}",
            )

        field_instance = RepositoryDataMetadataBlockFieldValue(
            value=value,
            title=self.validator.validate_and_extract_key(
                field_data_checked, "typeName", str, url
            ),
            type_=self.validator.validate_and_extract_key(
                field_data_checked, "typeClass", str, url
            ),
            multiple=self.validator.validate_and_extract_key(
                field_data_checked, "multiple", bool, url
            ),
            raw_data=field_data_checked,
        )
        return field_instance
