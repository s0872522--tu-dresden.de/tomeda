"""
Exceptions for the HTTP error handling and repository error handling.
"""

from typing import Any, Union

from ..exception import TomedaException


class TomedaHttpException(TomedaException):
    """
    Base exception for all HTTP-related errors.

    Attributes
    ----------
    status_code : int
        The HTTP status code that was returned.
    url : str
        The URL that the request was made to.
    """

    def __init__(
        self, status_code: int, url: str, message: str, *args: object
    ) -> None:
        self.status_code = status_code
        self.url = url
        super().__init__(message, args)


class TomedaHttpConnectionError(TomedaHttpException):
    """
    Exception raised when a connection to the server could not be established.

    This could be due to various reasons, such as the server not responding,
    the server refusing the connection, network problems, etc.
    """

    def __init__(
        self, status_code: int, url: str, message: str, *args: object
    ) -> None:
        super().__init__(status_code, url, message, args)


class TomedaHttpTimeoutError(TomedaHttpConnectionError):
    """
    Exception raised when a request to the server times out.

    This could be due to the server not responding in time, network congestion,
    or other network-related issues that cause the request to take too long.
    """

    def __init__(
        self, status_code: int, url: str, message: str, *args: object
    ) -> None:
        super().__init__(status_code, url, message, args)


class TomedaHttpUnauthorizedError(TomedaHttpException):
    """
    Exception raised when a request is unauthorized.

    This usually means that the server responded with a 401 Unauthorized status
    code, often due to missing or incorrect authentication credentials.
    """

    def __init__(
        self, status_code: int, url: str, message: str, *args: object
    ) -> None:
        super().__init__(status_code, url, message, args)


class TomedaRepositoryError(TomedaException):
    """
    Base exception for all repository-related errors.

    Attributes
    ----------
    repository_type : str
        The repository type that was used e.g. dataverse.
    url : str
        The base URL of the repository.
    """

    def __init__(
        self, repository_type: str, url: str, message: str, *args: object
    ) -> None:
        self.repository_type = repository_type
        self.url = url
        super().__init__(message, args)


class TomedaRepositoryConnectionError(TomedaRepositoryError):
    """
    Exception raised when a connection to the repository could not be
    established.

    This could be due to various reasons, such as the server not responding,
    the server refusing the connection, network problems, etc.
    """

    def __init__(
        self, repository_type: str, url: str, message: str, *args: object
    ) -> None:
        super().__init__(repository_type, url, message, args)


class TomedaRepositoryAuthenticationError(TomedaRepositoryError):
    """
    Exception raised when a request is unauthorized.

    This usually means that the server responded with a 401 Unauthorized status
    code, often due to missing or incorrect authentication credentials.
    """

    def __init__(
        self, repository_type: str, url: str, message: str, *args: object
    ) -> None:
        super().__init__(repository_type, url, message, args)


class TomedaRepositoryBadResponseError(TomedaRepositoryError):
    """
    Exception raised when a response from the repository is not as expected.
    """

    def __init__(
        self, repository_type: str, url: str, message: str, *args: object
    ) -> None:
        super().__init__(repository_type, url, message, args)


class TomedaRepositoryNotFoundError(TomedaRepositoryError):
    """
    Exception raised when a resource is not found.
    """

    def __init__(
        self, repository_type: str, url: str, message: str, *args: object
    ) -> None:
        super().__init__(repository_type, url, message, args)


class TomedaRepositoryValueError(TomedaRepositoryError):
    """
    Exception raised when a value is not as expected.
    """

    def __init__(
        self, repository_type: str, url: str, message: str, *args: object
    ) -> None:
        super().__init__(repository_type, url, message, args)


class TomedaRepositoryTypeError(TomedaRepositoryError):
    """
    Exception raised when a type is not as expected.
    """

    def __init__(
        self, repository_type: str, url: str, message: str, *args: object
    ) -> None:
        super().__init__(repository_type, url, message, args)


class HTTPResponseError(Exception):
    """Custom error that is raised if HTTP Return is not 200 ('Ok')"""

    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


class QueryDictNotValidError(Exception):
    """Custom error that is rised if the Query dict is not valid"""

    def __init__(
        self, value: Union[dict[str, str], dict[str, str]], message: str
    ) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


class DatasetExistsWarning:
    """
    Custom Warning that is raised if the dataset already exists in dataverse
    """

    def __init__(self, value: dict[str, Any], message: str) -> None:
        self.value = value
        self.message = message
        print(self.message)

        for i, elem in enumerate(self.value["data"]["items"], start=1):
            # print(f"> Search for: {i:3}. {elem['global_id'].split('/')[-1]}")
            print(f"> Search for: {i:3}. {elem['global_id']}")
