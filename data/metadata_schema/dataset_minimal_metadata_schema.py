from __future__ import annotations

import re
from datetime import date, datetime
from enum import Enum
from pathlib import Path
from typing import (
    Any,
    Iterable,
    List,
    Type,
)

import pydantic
from pydantic import (
    BaseModel,
    ByteSize,
    EmailStr,
    Field,
    PositiveInt,
    validator,
)

DTC_TitleType = Enum(
    "DTC_TitleType",
    ["AlternativeTitle", "Subtitle", "TranslatedTitle", "Other"],
    type=str,
)

"""
The Description Type complex type is used to record information about the
description type of the object. Its reference can be found:
https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#9a-descriptiontype
"""
DTC_DescriptionType = Enum(
    "DTC_DescriptionType",
    [
        "Abstract",
        "Methods",
        "SeriesInformation",
        "TableOfContents",
        "TechnicalInfo",
        "Other",
    ],
    type=str,
)

"""
Contributor Type
See 
https://support.datacite.org/docs/datacite-metadata-schema-v44-recommended-and-optional-properties#7a-contributortype
"""
DTC_ContributorType = Enum(
    "DTC_ContributorType",
    [
        "ContactPerson",
        "DataCollector",
        "DataCurator",
        "DataManager",
        "Distributor",
        "Editor",
        "HostingInformation",
        "Producer",
        "ProjectLeader",
        "ProjectManager",
        "ProjectMember",
        "RegistrationAgency",
        "RegistrationAuthority",
        "RelatedPerson",
        "Researcher",
        "ResearchGroup",
        "RightsHolder",
        "Sponsor",
        "Supervisor",
        "WorkPackageLeader",
        "Other",
    ],
    type=str,
)

Language = Enum(
    "Language",
    [
        "English",
        "German",
        "French",
        "Spanish",
        "Italian",
        "Russian",
        "Chinese",
        "Japanese",
        "Korean",
        "Arabic",
        "Portuguese",
        "Other",
    ],
    type=str,
)

PID_Type = Enum(
    "PID_Type",
    ["DOI", "URN", "EPIC", "HANDLE", "ORCID", "GND", "OTHER"],
    type=str,
)


class TitleType(BaseModel):
    value: DTC_TitleType = Field(
        title="Title Type",
        description="Type of the related item title. Use this subproperty to add a"
        "subtitle, translation, or alternate title to the main title."
        "The titleType subproperty is used when more than a"
        "single title is provided. Unless otherwise indicated"
        "by titleType, a title is considered to be the main"
        "title.",
    )


class Title(BaseModel):
    value: str = Field(title="Title", description="Title of the resource")
    titleType: None | TitleType = Field(
        title="Title Type",
        description="Type of the title, only specify if not main title",
    )
    lang: None | Language = Field(
        title="Language",
        description="Language of the title",
    )


class Project(BaseModel):
    name: str = Field(title="Name", description="Project name. Free text.")
    level: None | int = Field(
        title="Level",
        description="Project level, can be used to specify subprojects and"
        " subsubprojects. 0 is the highest level.",
    )


class URI(BaseModel):
    value: str = Field(
        title="URI",
        description="Uniform Resource Identifier, a superset of "
        "URL (UniformResource Locator) and URN "
        "(Uniform Resource Name)",
    )


class PID(BaseModel):
    """
    Persistent Identifier.
    Identifier of a resource that is persistent and unique.
    """

    id: URI = Field(title="ID", description="Identifier")
    type: PID_Type = Field(
        title="Type",
        description="Type of the identifier (Controlled Vocabulary)",
    )
    scheme: None | URI = Field(
        title="Scheme", description="Scheme of the identifier"
    )


class Description(BaseModel):
    """
    Description of the simulation.
    """

    content: str = Field(title="Description")
    descriptionType: None | DTC_DescriptionType = Field(
        title="Description Type", description="Type of the description"
    )
    lang: None | Language = Field(title="Language of the Description")


class Keyword(BaseModel):
    value: str = Field(title="Keyword", description="Keyword")
    vocabulary: None | str = Field(
        title="Vocabulary", description="Vocabulary of the Keyword used"
    )
    vocabularyURL: None | str = Field(
        title="Vocabulary URL", description="URL of the Vocabulary used"
    )


class PersonOrOrganization(BaseModel):
    """
    Datatype for a Person or Organization.
    """

    name: str = Field(
        title="Name",
        description="Name of the person or organization",
    )
    givenName: None | str = Field(
        title="First Name",
        description="The first name of a Person",
    )
    familyName: None | str = Field(
        title="Surname",
        description="The family name of the person",
    )
    address: None | str = Field(
        title="Address", description="The address of the person of organization"
    )
    affiliation: None | PersonOrOrganization = Field(
        title="Affiliation", description="The affiliation of the person"
    )
    email: None | list[EmailStr] = Field(
        title="Email", description="The email of the person"
    )
    id: None | list[PID] = Field(
        title="ID",
        description="Give ID if applicable",
    )
    role: None | DTC_ContributorType = Field(
        title="Role",
        description="Datacite Contributor Type",
    )


class Dataset(BaseModel):
    """
    A PyDantic model representing a Dataset.
    """

    # NEW
    creator: list[PersonOrOrganization] = Field(title="Producer/Author")
    worked: None | bool = Field(
        title="Indication of Success",
        description="Flag indicating whether the processing or operation on"
        " the dataset was successful.",
    )
    workedNote: None | str = Field(title="Explanation of (missing) Success")
    description: None | list[Description] = Field(
        title="Description (With attribute Description Type)"
    )
    title: list[Title] = Field(
        title="Title", description="Title of the Dataset"
    )
    keywords: None | list[Keyword] = Field(title="Keywords")
    project: None | list[Project] = Field(
        title="Project", description="Project with Attribute 'Level'"
    )

    _recommended: list[str] = [
        "keywords",
        "subjects",
        "dates",
        "mode",
        "measuredVariable",
        "controlledVariable",
    ]

    class Config:
        @staticmethod
        # def schema_extra(schema: dict[str, Any], model: type['PersonOrOrganization']) -> None:
        def schema_extra(  # type: ignore[misc]
            schema: dict[str, Any], model: type["Dataset"]
        ) -> None:
            for prop in schema.get("properties", {}).values():
                prop.pop("additional_info", None)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Inspects the metadata schema. Allows for pdf creation and json output."
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--create-pdf",
        action="store",
        nargs=1,
        metavar="<outputfile>.pdf",
        type=lambda p: Path(p).absolute(),
        help="Creates an PDF of the metadata schema",
    )
    group.add_argument(
        "--create-json",
        action="store",
        nargs=1,
        metavar="<outputfile>.json",
        type=lambda p: Path(p).absolute(),
        help="Outputs the JSON schema to a file.",
    )
    group.add_argument(
        "--view-json",
        action="store_true",
        help='Prints the JSON schema to stdout. View with "jq" or redirect to file.',
    )

    group.add_argument(
        "--created-nestedtext",
        action="store",
        nargs=1,
        metavar="<outputfile>.nt",
        type=lambda p: Path(p).absolute(),
        help="Prints the schema as nestedtext to stdout.",
    )
    group.add_argument(
        "--view-nestedtext",
        action="store_true",
        help="Prints the schema as nestedtext to stdout.",
    )

    args = parser.parse_args()

    if args.create_pdf:
        file_name = args.create_pdf[0].with_suffix(".pdf")

        # https://erdantic.drivendata.org/stable/
        import erdantic as erd

        erd.draw(Dataset, out=file_name)
        print(f'Created "{file_name}"')

    if args.create_json:
        import json

        file_name = args.create_json[0].with_suffix(".json")

        with file_name.open("w", encoding="utf-8") as f:
            json.dump(Dataset.schema(), f, indent=2)
        print(f'Created "{file_name}"')

    if args.view_json:
        print(Dataset.schema_json(indent=2))

    if args.created_nestedtext:
        import nestedtext as nt

        nt_string = nt.dumps(Dataset.schema_json(indent=2))

        file_name = args.created_nestedtext[0].with_suffix(".nt")

        with file_name.open("w", encoding="utf-8") as f:
            f.write(nt_string)

    if args.view_nestedtext:
        import nestedtext as nt

        print(nt.dumps(Dataset.schema_json(indent=2)))

    # PersonOrOrganization.update_forward_refs()
