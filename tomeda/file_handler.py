"""
This module provides a comprehensive solution for file handling in `ToMeDa`.
The main class, `TomedaFileHandler`, offers a set of methods for reading,
writing, and managing files.

The `read` and `write` methods are the central functions of this module:

- **`read() -> list[str]`**:
  The `read` method, by default, reads the content of a file as a list of
  strings, with each line of the file being a separate string in the list.
  Leading and trailing whitespaces are removed and empty lines are skipped.
  For special use cases, it offers the flexibility to read the entire file
  content as a single string or to disable the removal of whitespaces.

- **`write() -> None`**:
  The `write` method writes content to a file. When a string or a list of
  strings is provided, the strings are written to the file as they are. If no
  line breaks are present, Line Feeds (`\\n`) are appended to the strings. If
  a list is empty or None, the file is created but not written to.

These two methods are central to the functionality of the `TomedaFileHandler`
class and enable reading and writing of files with a high degree of detail
and control. For further details on these and other methods in this class,
refer to the respective docstrings in the codebase.


Possible optimizations:

- **File locking**: If ToMeDa processes many records in parallel or in threads, it
    can cause problems with file accesses. File locking can be used to prevent
    this. Library `fcntl` for unix and msvcrt for windows can be used,
    `filelock` is a wrapper for both.
- **File System Check**: Some file systems have no or only partial permission
    concept. This could be taken into account for file permission checks.
- **Chunked File Reading**: If ToMeDa processes large files, it can be useful to
    read the file in chunks. This can be done with the `read` method of the
    file object.
- **File Encoding**: If ToMeDa processes files with different encodings, it can be
    useful to use the 'chardet' library to detect the encoding of the file.

"""
import logging
import os
import time
from pathlib import Path

from .file_exception import (
    TomedaFileCreateError,
    TomedaFileDirectoryCreateError,
    TomedaFileEncodingError,
    TomedaFileError,
    TomedaFileExistsError,
    TomedaFileNotReadableError,
    TomedaFileNotWritableError,
    TomedaFilePermissionError,
    TomedaFileValueError,
)
from .tomeda_logging import TraceLogger

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


class TomedaFileHandler:
    """
    File Handler takes care of reading, writing, creating and more of files.
    """

    MAX_WRITE_ATTEMPTS = 5
    WRITE_ATTEMPT_DELAY = 0.1

    def __init__(
        self,
        file_path: Path,
        encoding: str = "utf8",
        overwrite: bool = False,
        dryrun: bool = False,
    ) -> None:
        self.file_path = file_path
        self.encoding = encoding
        self.overwrite = overwrite
        self.dryrun = dryrun

    def is_readable(self) -> bool:
        """
        Checks if a file is readable.

        Returns
        -------
        bool
            True if file is readable, False otherwise or not exists.

        """
        result = os.access(self.file_path, os.R_OK)
        if not result:
            logger.trace(
                "File '%s' is not readable (%s.is_readable)",
                self.file_path,
                self.__class__.__name__,
            )
        return result

    def is_writable(self) -> bool:
        """
        Checks if a file is writable.

        Returns
        -------
        bool
            True if file is writable, False otherwise or not exists.

        """
        result = os.access(self.file_path, os.W_OK)
        if not result:
            logger.trace(
                "File '%s' is not writable or not exist. (%s.is_writeable)",
                self.file_path,
                self.__class__.__name__,
            )
        return result

    def is_existing(self) -> bool:
        """
        Checks if a file exists.

        Returns
        -------
        bool
            True if file exists, False otherwise.

        """
        result = self.file_path.exists()
        if not result:
            logger.trace(
                "File '%s' does not exist. (%s.is_existing)",
                self.file_path,
                self.__class__.__name__,
            )
        return result

    def _check_overwrite(self) -> None:
        """
        Checks if file is overwritable or does not exist.

        Raises
        ------
        TomedaFileExistsError
            If file exists and overwrite is False.

        """
        result = self.is_existing() and self.overwrite or not self.is_existing()
        if not result:
            logger.trace(
                "File '%s' exists and overwrite is False. "
                "(%s._check_overwrite)",
                self.file_path,
                self.__class__.__name__,
            )
            raise TomedaFileExistsError(
                self.file_path,
                f"File exists and overwrite is False: {self.file_path}",
            )

    def _check_writable(self) -> None:
        """
        Checks if a file is writable and raises an exception if not.

        Raises
        ------
        TomedaFileNotWritableError
            If file is not writable.

        """
        result = self.is_writable() or not self.is_existing()
        if not result:
            logger.trace(
                "File '%s' is not writable. (%s._check_writable)",
                self.file_path,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileNotWritableError(
                self.file_path,
                f"File is not writable: {self.file_path}",
            )

    def read(self, raw: bool = False, strip: bool = True) -> list[str]:
        """
        Reads the content of a file and returns it as a list of strings. Line
        endings are set to unix line endings (`\\n`), if line endings are not
        removed anyway.

        * If `raw` is True, the content is returned as a list with a single
            string. `strip` is ignored in this case. Line endings are not
            removed, but set unix line endings (`\\n`).
        * If `raw` is False, the content is returned as a list of
            strings, each string represents a line of the file.
        * If `strip` is True, each string is removed line ending characters and
            leading and trailing whitespaces. Empty lines are removed.

        Parameters
        ----------
        raw: bool
            If `True`, the content is returned as a single string.
        strip: bool
            If `True`, each line is stripped from leading and trailing
            whitespaces.

        Returns
        -------
        list[str]
            The content of the file as a list of strings.

        Raises
        ------
        TomedaFileNotReadableError
            If the file is not readable.
        TomedaFilePermissionError
            If the file is not readable due to permission error.
        TomedaFileEncodingError
            If the file encoding is not valid.

        """
        if not self.is_readable():
            logger.trace(
                "File '%s' is not readable. (%s.read)",
                self.file_path,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileNotReadableError(
                self.file_path,
                f"File is not readable: {self.file_path}",
            )

        try:
            if raw:
                with self.file_path.open(
                    mode="r", encoding=self.encoding, newline="\n"
                ) as file_:
                    return [file_.read()]

            else:
                with self.file_path.open(
                    encoding=self.encoding, newline=""
                ) as file_:
                    file_contents = file_.read().splitlines()
                if strip:
                    result = [line.strip() for line in file_contents]
                    result = [line for line in result if line]
                else:
                    result = file_contents

                return result
        except PermissionError as error:
            logger.trace(
                "File '%s' is not readable due to permission error. (%s.read)",
                self.file_path,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFilePermissionError(
                self.file_path,
                f"File is not readable due to permission error: "
                f"{self.file_path}",
            ) from error
        except UnicodeDecodeError as error:
            logger.trace(
                "File '%s' is not readable due to encoding error. (%s.read)",
                self.file_path,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileEncodingError(
                self.file_path,
                f"File is not readable due to encoding error: {self.file_path}",
            ) from error
        except IOError as error:
            logger.trace(
                "File '%s' is not readable due to IO error. (%s.read)",
                self.file_path,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileNotReadableError(
                self.file_path,
                f"File is not readable due to IO error: {self.file_path}",
            ) from error

    def write(self, content: str | list[str] | None) -> None:
        """
        Writes content to a file.

        The content can be a string or a list of strings. If a list is given,
        each list item is written as a line. If a string is given, it is
        written as is without any line ending characters or other modifications.
        If the content is None or empty list, the file is created empty. If the
        file exists and overwrite is `True`, the file is overwritten/replaced.
        If the file exists and overwrite is `False`, an exception is raised.
        If the file does not exist, it is created with parent directories if
        needed.

        Parameters
        ----------
        content: str | list[str]
            The content to write to the file.

        Raises
        ------
        TomedaFileValueError
            If the content is not a string or a list of strings.
        TomedaFileNotWritableError
            If the file is not writable.
        TomedaFileExistsError
            If the file exists and overwrite is False.
        TomedaFileCreateError
            If the file could not be created.
        TomedaDirectoryCreateError
            If the parent directory could not be created.
        TomedaFileEncodingError
            If the encoding is not valid for the file.
        TomedaFilePermissionError
            If the file is not writable due to permission error.
        TomedaFileError
            If the file could not be written due to an unknown error.

        """
        if self.dryrun:
            logger.info("Dryrun: Not writing to '%s'.", self.file_path)
            return

        self._check_overwrite()
        self._check_writable()

        text = self._convert_line_to_text(content)
        self.create_file()

        for attempt in range(self.MAX_WRITE_ATTEMPTS):
            try:
                self.file_path.write_text(
                    text,
                    encoding=self.encoding,
                    errors="strict",
                    newline="\n",
                )
                logger.debug(
                    "Wrote to file '%s'. (%s.write)",
                    self.file_path,
                    self.__class__.__name__,
                )
                break
            except Exception as error:
                if not self._handle_write_error(error, attempt):
                    raise TomedaFileError(
                        self.file_path,
                        f"Could not write to file (Unknown Error): "
                        f"{self.file_path}",
                    ) from error

    def _handle_write_error(self, error: Exception, attempt: int) -> bool:
        """
        Handles write errors.

        If the error is a permission error or a IO error and the number of
        attempts is less than the maximum number of attempts to write,
        the write attempt is retried. If the number of attempts is equal to the
        maximum number of attempts to write (meaning that the last attempt
        failed), the error is raised.
        If the error is a value error, the error is raised.

        Parameters
        ----------
        error: Exception
            The error to handle.
        attempt: int
            The number of the write attempt.

        Returns
        -------
        bool
            `True` if the write attempt should be retried, `False` otherwise.

        Raises
        ------
        TomedaFileEncodingError
            If the encoding is not valid for the file.
        TomedaFilePermissionError
            If the file is not writable due to permission error.
        TomedaFileNotWritableError
            If the file is not writable.

        """
        if isinstance(error, ValueError):
            logger.trace(
                "Encoding '%s' is not valid for file '%s'. "
                "(%s._handle_write_error)",
                self.encoding,
                self.file_path,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileEncodingError(
                self.file_path, "Encoding is not valid for file."
            ) from error
        elif isinstance(error, PermissionError):
            if attempt < self.MAX_WRITE_ATTEMPTS - 1:
                time.sleep(self.WRITE_ATTEMPT_DELAY)
                logger.debug(
                    "Write failed to the file '%s' due to permission "
                    "error. Retrying attempt %d... (%s._handle_write_error)",
                    self.file_path,
                    attempt + 1,
                    self.__class__.__name__,
                )
                return True
            logger.trace(
                "File '%s' is not writable due to permission error.",
                self.file_path,
                exc_info=True,
            )
            raise TomedaFileNotWritableError(
                self.file_path,
                f"File '{self.file_path}' is not writable due to "
                f"permission error on attempt {attempt + 1}.",
            ) from error
        elif isinstance(error, IOError):
            if attempt < self.MAX_WRITE_ATTEMPTS - 1:
                time.sleep(self.WRITE_ATTEMPT_DELAY)
                logger.debug(
                    "Write failed to the file '%s'. Retrying attempt %d... "
                    "(%s._handle_write_error)",
                    self.file_path,
                    attempt + 1,
                    self.__class__.__name__,
                )
                return True
            if self.overwrite:
                logger.trace(
                    "The file '%s' could not be overwritten. "
                    "(%s._handle_write_error)",
                    self.file_path,
                    self.__class__.__name__,
                    exc_info=True,
                )
            else:
                logger.trace(
                    "Write failed to the file '%s'. (%s._handle_write_error)",
                    self.file_path,
                    self.__class__.__name__,
                    exc_info=True,
                )
            raise TomedaFileNotWritableError(
                self.file_path,
                f"IO error occurred when writing to the file "
                f"'{self.file_path}' on attempt {attempt + 1}.",
            ) from error
        else:
            logger.trace(
                "Write failed to the file '%s' for unknown reason. (%s._handle_write_error)",
                self.file_path,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileNotWritableError(
                self.file_path,
                f"Unknown error occurred when writing to the file "
                f"'{self.file_path}' on attempt {attempt + 1}.",
            ) from error

    def create_file(self) -> None:
        """
        Creates a file.

        If the file already exists, nothing is done. if parent directories
        do not exist, they are created.

        Raises
        ------
        TomedaFileCreateError
            If the file could not be created.
        TomedaFileDirectoryCreateError
            If the parent directory could not be created.
        TomedaFilePermissionError
            If the file or parent directory could not be created due to
            permission.

        """
        if self.is_existing():
            logger.trace(
                "File '%s' exists. (%s.create_file)",
                self.file_path,
                self.__class__.__name__,
            )
        else:
            self.create_parent_dir()
            try:
                self.file_path.touch()
            except PermissionError as error:
                logger.trace(
                    "File '%s' could not be created due to permission. "
                    "(%s.create_file)",
                    self.file_path,
                    self.__class__.__name__,
                    exc_info=True,
                )
                raise TomedaFilePermissionError(
                    self.file_path,
                    f"File could not be created due to permission: "
                    f"{self.file_path}",
                ) from error
            except IOError as error:
                logger.trace(
                    "File '%s' could not be created. (%s.create_file)",
                    self.file_path,
                    self.__class__.__name__,
                    exc_info=True,
                )
                raise TomedaFileCreateError(
                    self.file_path,
                    f"File could not be created: {self.file_path}",
                ) from error

    def create_parent_dir(self) -> None:
        """
        Creates the parent directory of a file.

        Raises
        ------
        TomedaFileDirectoryCreateError
            If the parent directory could not be created.
        TomedaFilePermissionError
            If the parent directory could not be created due to permission.

        """
        if self.file_path.parent.exists():
            logger.trace(
                "Parent directory '%s' exists. (%s.create_parent_dir)",
                self.file_path.parent,
                self.__class__.__name__,
            )
            return

        try:
            self.file_path.parent.mkdir(parents=True, exist_ok=True)
            logger.debug(
                "Parent directory '%s' created.", self.file_path.parent
            )
        except PermissionError as error:
            logger.trace(
                "Parent directory '%s' could not be created. "
                "(%s.create_parent_dir)",
                self.file_path.parent,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFilePermissionError(
                self.file_path.parent,
                f"Parent directory could not be created: "
                f"{self.file_path.parent}",
            ) from error
        except IOError as error:
            logger.trace(
                "Parent directory '%s' could not be created. "
                "(%s.create_parent_dir)",
                self.file_path.parent,
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileDirectoryCreateError(
                self.file_path.parent,
                f"Parent directory could not be created: "
                f"{self.file_path.parent}",
            ) from error

    def _convert_line_to_text(self, content: list[str] | str | None) -> str:
        """
        Converts a list of strings to a string with line breaks.

        Parameters
        ----------
        content: list[str] | str | None
            The list of strings to convert. A single string is returned as is.
            If `None` or empty list, an empty string is returned.

        Returns
        -------
        str
            The converted string with line breaks.

        Raises
        ------
        TomedaFileValueError
            If the content is not a list of strings or None.

        """
        content_checked: list[str]
        if content is None:
            content_checked = []
        elif isinstance(content, str):
            content_checked = [content]
        else:
            content_checked = content

        if not isinstance(content_checked, list):
            logger.trace(
                "Content must be a list of strings or None. Given type: '%s' "
                "(%s._convert_line_to_text)",
                type(content_checked),
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileValueError(
                self.file_path,
                f"Content must be a list of strings or None: Given type: "
                f"'{type(content_checked)}'",
            )

        try:
            text: str = (
                "\n".join(line.rstrip("\n\r") for line in content_checked)
                + "\n"
            )
        except (AttributeError, TypeError) as error:
            logger.trace(
                "Content must be a list of strings or None. Given type: '%s' "
                "(%s._convert_line_to_text)",
                type(content_checked),
                self.__class__.__name__,
                exc_info=True,
            )
            raise TomedaFileValueError(
                self.file_path,
                f"Content must be a list of strings or None: Given type: "
                f"'{type(content_checked)}'",
            ) from error
        logger.trace(
            "Converted content to text. (%s._convert_line_to_text)",
            self.__class__.__name__,
        )
        return text
