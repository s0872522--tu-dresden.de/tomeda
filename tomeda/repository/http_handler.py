"""
This module provides a wrapper for making HTTP requests using the `requests`
library. It includes error handling for common HTTP error status codes.

"""

import logging
from typing import Any

import requests

from tomeda.tomeda_logging import TraceLogger

from .repository_execption import (
    TomedaHttpException,
    TomedaHttpTimeoutError,
    TomedaHttpUnauthorizedError,
)

logger: TraceLogger = logging.getLogger(__name__)  # type: ignore[assignment]


class TomedaHttpHandler:
    """
    A class used to make HTTP requests with error handling.

    Attributes
    ----------
    REQUEST_ATTEMPTS : int
        The maximum number of attempts to make for a single HTTP request in
        case of a connection timeout.
    timeout : float | tuple[float, float]
        The timeout duration in seconds for the HTTP requests. This can be
        a single float, in which case the same timeout applies to both the
        connection and the data transfer, or a tuple of two floats,
        specifying separate timeouts for the connection and data transfer.

    """

    REQUEST_ATTEMPTS = 3

    def __init__(self, timeout: float | tuple[float, float] = (10, 10)) -> None:
        """
        Constructs a new `TomedaHttpHandler` object.

        Parameters
        ----------
        timeout : float | tuple[float, float], optional
            The timeout duration in seconds for the HTTP requests. This can be
            a single float, in which case the same timeout applies to both the
            connection and the data transfer, or a tuple of two floats,
            specifying separate timeouts for the connection and data transfer.

        """
        self.timeout = timeout

    def _request(
        self,
        method: str,
        url: str,
        parameter: dict[str, str] | None,
        header: dict[str, str] | None,
        data: Any,
        json: Any,
        files: Any,
    ) -> requests.Response:
        """
        Makes a request to a specified URL with a specified HTTP method.

        This method will attempt to send the request up to `REQUEST_ATTEMPTS`
        times in case of a connection timeout.

        Parameters
        ----------
        method : str
            The HTTP method to use for the request.
        url : str
            The URL to send the request to.
        parameter : dict[str, str] | None
            A dictionary of URL parameters to append to the URL.
        header : dict[str, str] | None
            A dictionary of HTTP headers to send with the request.
        data : Any
            The data to send in the body of the request. This may be a
            dictionary, a list of tuples, bytes, or a file-like object. If a
            dictionary or list of tuples is provided, it will be form-encoded.
        json : Any
            The JSON-serializable Python object to send in the body of the
            request. If provided, it will be serialized to JSON and the
            Content-Type header of the request will be set to
            'application/json'.
        files : Any
            The files to send in the body of the request. This should be a
            dictionary, where each key-value pair represents a field name and a
            file-like object (including a file object or a BytesIO object).

        Returns
        -------
        requests.Response
            The response from the server.

        Raises
        ------
        TomedaHttpUnauthorizedError
            If the server returns a 401 Unauthorized status code.
        TomedaHttpException
            If the server returns a status code that indicates an error
            (>= 400).
        TomedaHttpTimeoutError
            If the request times out after `REQUEST_ATTEMPTS` attempts.

        """
        attempt = 1
        while attempt <= self.REQUEST_ATTEMPTS:
            try:
                response = requests.request(
                    method,
                    url,
                    params=parameter,
                    headers=header,
                    data=data,
                    json=json,
                    files=files,
                    timeout=self.timeout,
                )
                break
            except requests.exceptions.Timeout as error:
                logger.warning(
                    "HTTP request timeout on attempt %s of %s: %s",
                    attempt,
                    self.REQUEST_ATTEMPTS,
                    error,
                )
                if attempt >= self.REQUEST_ATTEMPTS:
                    logger.error(
                        "A HTTP connection timeout has occurred: %s", error
                    )
                    raise TomedaHttpTimeoutError(
                        408,
                        url,
                        "A HTTP connection timeout has occurred: %s",
                        error,
                    ) from error
                attempt += 1

        logger.trace(
            "Received HTTP response with status code %s", response.status_code
        )

        if response.status_code == 401:
            logger.trace(
                "An HTTP authentication error has occurred: %s", response.text
            )
            raise TomedaHttpUnauthorizedError(
                response.status_code,
                response.url,
                "An HTTP authentication error has occurred: %s",
                response.text,
            )
        if response.status_code >= 400:
            logger.trace("An HTTP error has occurred: %s", response.text)
            raise TomedaHttpException(
                response.status_code, response.url, response.text
            )
        return response

    def get(
        self,
        url: str,
        parameter: dict[str, str] | None = None,
        header: dict[str, str] | None = None,
    ) -> requests.Response:
        """
        Makes a GET request to a specified URL.

        Parameters
        ----------
        url : str
            The URL to send the GET request to.
        parameter : dict, optional
            A dictionary of URL parameters to append to the URL.
        header : dict, optional
            A dictionary of HTTP headers to send with the request.

        Returns
        -------
        requests.Response
            The response from the server.

        Raises
        ------
        TomedaHttpUnauthorizedError
            If the server returns a 401 Unauthorized status code.
        TomedaHttpException
            If the server returns a status code that indicates an error
            (>= 400).
        TomedaHttpTimeoutError
            If the request times out after `REQUEST_ATTEMPTS` attempts.

        """
        logger.trace("GET with url '%s'", url)
        return self._request(
            "GET",
            url,
            parameter=parameter,
            header=header,
            data=None,
            json=None,
            files=None,
        )

    def post(
        self,
        url: str,
        parameter: dict[str, str] | None = None,
        header: dict[str, str] | None = None,
        data: Any = None,
        json: Any = None,
        files: Any = None,
    ) -> requests.Response:
        """
        Makes a POST request to a specified URL.

        Parameters
        ----------
        url : str
            The URL to send the GET request to.
        parameter : dict, optional
            A dictionary of URL parameters to append to the URL.
        header : dict, optional
            A dictionary of HTTP headers to send with the request.
        data : Any, optional
            The data to send in the body of the request. This may be a
            dictionary, a list of tuples, bytes, or a file-like object.
            If a dictionary or list of tuples is provided, it will be
            form-encoded.
        json : Any, optional
            The JSON-serializable Python object to send in the body of the
            request. If provided, it will be serialized to JSON and the
            Content-Type header of the request will be set to
            'application/json'.
        files : Any, optional
            The files to send in the body of the request. This should be
            a dictionary, where each key-value pair represents a field
            name and a file-like object (including a file object or a
            BytesIO object).

        Returns
        -------
        requests.Response
            The response from the server.

        Raises
        ------
        TomedaHttpUnauthorizedError
            If the server returns a 401 Unauthorized status code.
        TomedaHttpException
            If the server returns a status code that indicates an error
            (>= 400).
        TomedaHttpTimeoutError
            If the request times out after `REQUEST_ATTEMPTS` attempts.

        """
        logger.trace("POST with url '%s'", url)
        return self._request(
            "POST",
            url,
            parameter=parameter,
            header=header,
            data=data,
            json=json,
            files=files,
        )

    def put(
        self,
        url: str,
        parameter: dict[str, str] | None = None,
        header: dict[str, str] | None = None,
        data: Any = None,
        json: Any = None,
        files: Any = None,
    ) -> requests.Response:
        """
        Makes a PUT request to a specified URL.

        Parameters
        ----------
        url : str
            The URL to send the GET request to.
        parameter : dict, optional
            A dictionary of URL parameters to append to the URL.
        header : dict, optional
            A dictionary of HTTP headers to send with the request.
        data : Any, optional
            The data to send in the body of the request. This may be a
            dictionary, a list of tuples, bytes, or a file-like object.
            If a dictionary or list of tuples is provided, it will be
            form-encoded.
        json : Any, optional
            The JSON-serializable Python object to send in the body of the
            request. If provided, it will be serialized to JSON and the
            Content-Type header of the request will be set to
            'application/json'.
        files : Any, optional
            The files to send in the body of the request. This should be
            a dictionary, where each key-value pair represents a field
            name and a file-like object (including a file object or a
            BytesIO object).

        Returns
        -------
        requests.Response
            The response from the server.

        Raises
        ------
        TomedaHttpUnauthorizedError
            If the server returns a 401 Unauthorized status code.
        TomedaHttpException
            If the server returns a status code that indicates an error
            (>= 400).
        TomedaHttpTimeoutError
            If the request times out after `REQUEST_ATTEMPTS` attempts.

        """
        logger.trace("PUT with url '%s'", url)
        return self._request(
            "PUT",
            url,
            parameter=parameter,
            header=header,
            data=data,
            json=json,
            files=files,
        )
